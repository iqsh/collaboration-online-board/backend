// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {
    development:{
        client: 'postgresql',
        connection: {
            host: process.env.BACKEND_DB_HOST,
            port: process.env.BACKEND_DB_PORT,
            user: process.env.BACKEND_DB_USER,
            password: process.env.BACKEND_DB_PASSWORD,
            database: process.env.BACKEND_DB_DATABASE
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },
    production: {
        client: 'postgresql',
        connection: {
            host: process.env.BACKEND_DB_HOST,
            port: process.env.BACKEND_DB_PORT,
            user: process.env.BACKEND_DB_USER,
            password: process.env.BACKEND_DB_PASSWORD,
            database: process.env.BACKEND_DB_DATABASE
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    }

};
