-- Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
--
-- SPDX-License-Identifier: AGPL-3.0-or-later

BEGIN;

--Tables--
CREATE TABLE registration (
    mail text,
    expire text,
    code text NOT NULL,
    dnr character varying,
    hashcode character varying,
    type character varying
);
CREATE TABLE freigabe (
    pane_hash character varying,
    ende character varying,
    prev_hash character varying,
    likes boolean,
    comments boolean
);
CREATE TABLE admin_panel(
	mail varchar primary key,
	dashboard varchar,
	password varchar,
	dnr varchar DEFAULT ''
);
CREATE TABLE password_reset(
	hashcode varchar PRIMARY KEY,
	mail varchar,
	boardname varchar,
	expire varchar,
	code varchar,
	password_pane varchar,
	password_admin varchar
);

CREATE TABLE passwort_zuruecksetzen (
    hashcode character varying ,
	mail character varying,
    abgelaufen text,
    code text PRIMARY KEY
);

CREATE TABLE freeformembedded(
	hashcode varchar,
	parent_id varchar,
	id varchar,
	body varchar,
	url varchar,
	type varchar,
	likes integer,
	shape varchar,
	color varchar,
	info varchar,
	time varchar DEFAULT current_timestamp,
	PRIMARY KEY(hashcode, parent_id, id)
);
CREATE TABLE freeformobject(
	hashcode varchar,
	id varchar,
	title varchar,
	"left" varchar,
	top varchar,
	comment_length integer,
	shape varchar,
	color varchar,
	embedded_length integer,
	body varchar,
	info varchar,
	height varchar,
	width varchar,
	time varchar DEFAULT current_timestamp,
	PRIMARY KEY(hashcode, id)
);
CREATE TABLE freeforms(
	hashcode varchar,
	password_pane varchar,
	password_admin varchar,
	arrow varchar,
	line_style varchar,
	line_color varchar,
	line_size integer,
	like_symbol varchar,
	background_color varchar,
	shape varchar,
	shape_color varchar,
	comment_color varchar,
	circle BOOLEAN,
	embeddeds BOOLEAN,
	line_text BOOLEAN,
	likes BOOLEAN,
	font_size varchar,
	comments BOOLEAN,
	count integer,
	unlocked varchar,
	headline varchar,
	subheadline varchar,
	background_image varchar,
	public_key varchar,
	private_key varchar,
	iv varchar,
	mail varchar,
	dnr varchar,
	time varchar DEFAULT current_timestamp,
	no_pass_hash varchar,
	PRIMARY KEY(hashcode)
);
CREATE TABLE schedulecomments(
	hashcode varchar,
	parent_id varchar,
	id varchar,
	message varchar,
	likes integer,
	shape varchar,
	color varchar,
	body varchar,
	info varchar,
	type varchar,
	time varchar DEFAULT current_timestamp,
	PRIMARY KEY(hashcode, parent_id, id)
);
CREATE TABLE scheduleposts(
	hashcode varchar,
	parent_id varchar,
	id varchar,
	message varchar,
	body varchar,
	type varchar,
	likes integer,
	shape varchar,
	color varchar,
	info varchar,
	comment_length integer,
	order_id varchar,
	time varchar DEFAULT current_timestamp,
	PRIMARY KEY(hashcode, parent_id, id)
);
CREATE TABLE scheduleobjects(
	hashcode varchar,
	id varchar,
	title varchar,
	comments integer,
	color varchar,
	shape varchar,
	order_id varchar,
	time varchar DEFAULT current_timestamp,
	PRIMARY KEY(hashcode, id)
);
CREATE TABLE schedules(
	hashcode varchar,
	password_pane varchar,
	password_admin varchar,
	like_symbol varchar,
	background_color varchar,
	shape varchar,
	shape_color varchar,
	comment_color varchar,
	column_width varchar,
	embeddeds BOOLEAN,
	one_column BOOLEAN,
	likes BOOLEAN,
	font_size varchar,
	comments BOOLEAN,
	count integer,
	unlocked varchar,
	headline varchar,
	subheadline varchar,
	background_image varchar,
	public_key varchar,
	private_key varchar,
	iv varchar,
	mail varchar,
	dnr varchar,
	time varchar DEFAULT current_timestamp,
	no_pass_hash varchar,
	PRIMARY KEY(hashcode)
);

CREATE TABLE lines (
	id varchar, 
	hashcode varchar,
	start varchar,
	"end" varchar,
	text varchar,
	size varchar,
	dash boolean,
	color varchar,
	PRIMARY KEY (id, hashcode)
);
CREATE TABLE dashboard (
    "timestamp" character varying NOT NULL,
    cpu character varying,
    memory character varying,
    heap character varying,
    response_time character varying,
    requests_per_second character varying,
    user_count integer,
    pane_count integer
);


CREATE TABLE session_store (
	session_id varchar,
	mail varchar,
	expire varchar
);



--Views--

CREATE VIEW view_dashboard AS
 SELECT dashboard."timestamp", dashboard.cpu, dashboard.memory, dashboard.heap, dashboard.response_time, dashboard.requests_per_second, dashboard.user_count, dashboard.pane_count
   FROM dashboard  ORDER BY dashboard."timestamp";

CREATE OR REPLACE VIEW view_freeforms AS 
	SELECT hashcode, password_pane,password_admin,arrow,line_style,line_color,line_size,like_symbol,background_color,shape, shape_color, comment_color, circle,embeddeds,line_text,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash FROM freeforms f;

CREATE OR REPLACE VIEW view_adminpanel AS
 SELECT admin_panel.mail,
    admin_panel.dashboard, admin_panel.password, admin_panel.dnr
   FROM admin_panel;
     
CREATE OR REPLACE VIEW view_freeformobject AS 
	SELECT id, hashcode as freeform_id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width FROM freeformobject;

CREATE OR REPLACE VIEW view_freeformembedded AS
	SELECT id, parent_id, hashcode as freeform_id, body, url, type, shape, color, likes, info FROM freeformembedded;

CREATE OR REPLACE VIEW view_line AS SELECT id, hashcode as freeform_id, start, "end", text as line_text, size, dash, color FROM lines;

CREATE OR REPLACE VIEW view_scheduleobjects AS 
	SELECT id, hashcode as schedule_id, title, comments, color,shape, order_id FROM scheduleobjects;

CREATE OR REPLACE VIEW view_schedulecomments AS
    SELECT id, parent_id, hashcode as schedule_id, message, likes, shape, color, body, info, type FROM schedulecomments;

CREATE OR REPLACE VIEW view_scheduleposts AS
    SELECT id, parent_id, hashcode as schedule_id, message, likes, type, body, color, shape, comment_length, info, order_id FROM scheduleposts;

CREATE OR REPLACE VIEW view_schedules AS
	SELECT hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash FROM schedules s;

CREATE OR REPLACE VIEW view_password_reset AS
	SELECT t.hashcode, t.mail, t.boardname, pr.abgelaufen as expire, pr.code,password_admin, password_pane FROM (SELECT hashcode, mail, 'Schedule' as boardname, password_admin, password_pane from schedules
	UNION
	SELECT hashcode, mail, 'Freeform' as boardname, password_admin, password_pane from freeforms) t
	LEFT  JOIN passwort_zuruecksetzen pr ON t.hashcode = pr.hashcode;

CREATE OR REPLACE VIEW view_share AS
	SELECT f.pane_hash, f.ende, f.prev_hash, f.likes, f.comments,
	CASE WHEN pane_hash IN (SELECT hashcode FROM schedules) AND ((SELECT one_column from schedules where hashcode=pane_hash) = 'true') THEN 'Stream' WHEN pane_hash IN (SELECT hashcode FROM schedules) THEN 'Schedule' ELSE 'Freeform' END as typname
	FROM freigabe f;

CREATE OR REPLACE VIEW view_all_boards AS
	SELECT t.hash_pane, a.dashboard as hash_dash, t.type, t.mail, t.headline, t.subheadline, t.no_pass_hash, t.background_image, t.background_color, t.last_update FROM (SELECT * FROM (SELECT s.hashcode as hash_pane, CASE WHEN (one_column IS TRUE) THEN 'Stream' ELSE 'Schedule' END as type, s.mail,s.headline, s.subheadline, s.no_pass_hash, s.background_image, s.background_color, CASE WHEN (sp.last_update IS NULL) THEN s.time else sp.last_update end as last_update FROM schedules s LEFT JOIN (SELECT max(spost.time) as last_update, spost.hashcode FROM scheduleposts spost GROUP BY spost.hashcode) sp ON s.hashcode=sp.hashcode) as s
     UNION (SELECT f.hashcode as hash_pane, 'Freeform' as type, f.mail,f.headline, f.subheadline, f.no_pass_hash, f.background_image, f.background_color, CASE WHEN (fo.last_update IS NULL) THEN f.time else fo.last_update end as last_update FROM freeforms f LEFT JOIN (SELECT max(fobj.time) as last_update, fobj.hashcode FROM freeformobject fobj GROUP BY fobj.hashcode) fo ON f.hashcode=fo.hashcode) ) as t
         LEFT JOIN admin_panel a on t.mail = a.mail;
		 
CREATE OR REPLACE VIEW view_account_password_reset AS
	SELECT pr.mail, pr.abgelaufen as expire, pr.code, ap.password
	FROM passwort_zuruecksetzen pr LEFT JOIN admin_panel ap ON pr.mail = ap.mail WHERE pr.mail IS NOT NULL ;

		 
CREATE OR REPLACE VIEW view_freeform_size AS 
 SELECT f.hashcode,
    f.no_pass_hash,
    max(replace(fo.top::text, 'px'::text, ''::text)::double precision + COALESCE(replace(fo.height::text, 'px'::text, ''::text)::double precision, 400::double precision)) AS max_height,
    max(replace(fo."left"::text, 'px'::text, ''::text)::double precision + COALESCE(replace(fo.width::text, 'px'::text, ''::text)::double precision, 400::double precision)) AS max_width
   FROM freeformobject fo
     JOIN freeforms f ON fo.hashcode::text = f.hashcode::text
  GROUP BY f.hashcode, f.no_pass_hash;
  
  
  CREATE OR REPLACE VIEW pilot_oberflaechen AS
SELECT t.anzahl_oberflaechen,
	(
		SELECT count(*) AS count
		FROM (
				SELECT view_freeforms.mail
				FROM view_freeforms
				UNION
				SELECT view_schedules.mail
				FROM view_schedules
			) a
	) AS mails,
	(
		SELECT count(
				DISTINCT regexp_replace(t_1.dnr::text, '^\s+'::text, ''::text)
			) AS count
		FROM (
				SELECT view_schedules.dnr
				FROM view_schedules
				UNION
				SELECT view_freeforms.dnr
				FROM view_freeforms
			) t_1
	) AS schulen,
	(
		SELECT max(t_1.user_count) AS max
		FROM (
				SELECT dashboard.user_count
				FROM dashboard
			) t_1
	) AS max_user_overall,
	(
		SELECT max(dashboard.user_count) AS max
		FROM dashboard
		WHERE dashboard."timestamp"::date >= CURRENT_DATE
			AND dashboard."timestamp"::date < (CURRENT_DATE + '1 day'::interval)
	) AS max_user_day
FROM (
		SELECT count(*) AS anzahl_oberflaechen
		FROM (
				SELECT view_schedules.hashcode
				FROM view_schedules
				UNION
				SELECT view_freeforms.hashcode
				FROM view_freeforms
			) b
	) t;
  
  --Triggerfunctions--
	
   
CREATE OR REPLACE FUNCTION insert_update_delete_freeformobject()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
        INSERT INTO freeformobject (hashcode, id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width) VALUES (new.freeform_id, new.id, new.title, new.left, new.top, new.comment_length, new.shape, new.color, new.embedded_length, new.body, new.info, new.height, new.width);
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformobject';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM view_freeformembedded WHERE freeform_id = old.freeform_id AND parent_id = old.id;
	DELETE FROM view_line WHERE freeform_id = old.freeform_id and start = old.id;
	DELETE FROM view_line WHERE freeform_id = old.freeform_id and "end" = old.id;
    DELETE FROM freeformobject where hashcode = old.freeform_id AND id = old.id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION insert_update_delete_freeformembedded()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
        INSERT INTO freeformembedded (hashcode, parent_id, id, body, url, type, likes, shape, color, info) VALUES (new.freeform_id, new.parent_id, new.id, new.body, new.url, new.type, new.likes, new.shape, new.color, new.info);
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformembedded';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM freeformembedded where hashcode = old.freeform_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION insert_update_delete_line()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
        INSERT INTO lines (id, hashcode, start, "end", text, size, dash, color) VALUES (new.id, new.freeform_id, new.start, new."end", new.line_text, new.size, new.dash, new.color);
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM lines WHERE hashcode = old.freeform_id and id = old.id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION insert_update_delete_freeforms()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
        INSERT INTO freeforms(hashcode, password_pane, password_admin, arrow, line_style, line_color, line_size, like_symbol, background_color, shape, shape_color, comment_color, circle, embeddeds, line_text, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash) VALUES
	    (new.hashcode, new.password_pane, new.password_admin, new.arrow, new.line_style, new.line_color, new.line_size, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.circle, new.embeddeds, new.line_text, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash);
	ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE freeforms SET password_pane=new.password_pane, password_admin = new.password_admin, arrow = new.arrow, line_style = new.line_style, line_color = new.line_color, line_size = new.line_size, like_symbol = new.like_symbol, background_color = new.background_color,
                             shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, circle = new.circle, embeddeds = new.embeddeds, line_text = new.line_text, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash = new.no_pass_hash
	    WHERE hashcode= old.hashcode;
    ELSEIF (TG_OP = 'DELETE') THEN
	DELETE FROM view_password_reset where hashcode = old.hashcode;
	DELETE FROM view_share where pane_hash = old.hashcode;
    DELETE FROM view_freeformembedded WHERE freeform_id = old.hashcode;
    DELETE FROM view_freeformobject where freeform_id = old.hashcode;
    DELETE FROM view_line WHERE freeform_id = old.hashcode;
    DELETE FROM freeforms where hashcode = old.hashcode;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION insert_update_delete_scheduleobject()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
        INSERT INTO scheduleobjects (hashcode, id, title, comments, color, shape, order_id) VALUES
	    (new.schedule_id, new.id, new.title, new.comments, new.color, new.shape, new.order_id);
     ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleobject';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM view_scheduleposts where parent_id = old.id AND schedule_id = old.schedule_id;
    DELETE FROM scheduleobjects where hashcode = old.schedule_id AND id = old.id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION insert_update_delete_schedulecomments()
  RETURNS trigger AS
$$
DECLARE

BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
        INSERT INTO schedulecomments (id, parent_id, hashcode, message, likes, shape, color, info, body, type) VALUES (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.shape, new.color, new.info, new.body, new.type) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für schedulecomments';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM schedulecomments where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;

END;
$$
LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION insert_update_delete_scheduleposts()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
        INSERT INTO scheduleposts (id, parent_id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id) VALUES
                                (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleposts';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM schedulecomments where hashcode = old.schedule_id AND parent_id = id;
    DELETE FROM scheduleposts where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION insert_update_delete_schedules()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
        INSERT INTO schedules(hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash) VALUES
	    (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.column_width, new.embeddeds, new.one_column, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash);
	ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE schedules SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                             shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, one_column = new.one_column, embeddeds = new.embeddeds, column_width = new.column_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash
	    WHERE hashcode= old.hashcode;
    ELSEIF (TG_OP = 'DELETE') THEN
	DELETE FROM view_password_reset where hashcode = old.hashcode;
	DELETE FROM view_share where pane_hash = old.hashcode;
    DELETE FROM view_scheduleposts WHERE schedule_id = old.hashcode;
    DELETE FROM view_schedulecomments WHERE schedule_id = old.hashcode;
    DELETE FROM view_scheduleobjects where schedule_id = old.hashcode;
    DELETE FROM schedules where hashcode = old.hashcode;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION public.insert_update_delete_password_reset() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO passwort_zuruecksetzen (hashcode,abgelaufen,code) VALUES (new.hashcode, new.expire, new.code);
        ELSEIF(tg_op='UPDATE') THEN
           IF (old.boardname = 'Schedule') THEN
               UPDATE schedules SET password_admin = new.password_admin, password_pane = new.password_pane WHERE hashcode=old.hashcode;
           ELSEIF (old.boardname = 'Freeform') THEN
               UPDATE freeforms SET password_admin = new.password_admin, password_pane = new.password_pane WHERE hashcode=old.hashcode;
           end if;
        ELSEIF(tg_op='DELETE') THEN
			DELETE FROM passwort_zuruecksetzen WHERE hashcode = old.hashcode AND abgelaufen = old.expire;
       END IF;
    RETURN NEW;
END;
$$;


CREATE OR REPLACE FUNCTION insert_update_delete_share() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
        IF (TG_OP = 'INSERT') THEN
			DELETE FROM freigabe where pane_hash  = new.pane_hash;
			INSERT INTO freigabe (pane_hash ,ende, prev_hash, likes, comments) VALUES (new.pane_hash, new.ende, new.prev_hash, new.likes, new.comments);
        ELSEIF(tg_op='UPDATE') THEN
           RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF(tg_op='DELETE') THEN
			DELETE FROM freigabe where pane_hash  = old.pane_hash;
       END IF;
    RETURN NEW;
END;
$$;



CREATE OR REPLACE FUNCTION insert_update_delete_adminpanel() RETURNS trigger AS $$
DECLARE
BEGIN
        IF (TG_OP = 'INSERT') THEN
            IF ((SELECT count(*) FROM admin_panel a WHERE a.mail = new.mail)<1)
			THEN INSERT INTO admin_panel (mail, dashboard, password, dnr) VALUES (new.mail, new.dashboard, new.password, new.dnr);
			ELSE 
			UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
			END IF;
		ELSEIF(tg_op='UPDATE') THEN
		UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
        ELSEIF(tg_op='DELETE') THEN
			DELETE FROM view_account_password_reset where mail = old.mail;
			DELETE FROM view_freeforms where mail = old.mail;
			DELETE FROM view_schedules where mail = old.mail;
			DELETE FROM admin_panel a WHERE a.mail = old.mail;
       END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION insert_update_delete_account_password_reset() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO passwort_zuruecksetzen (mail,abgelaufen,code) VALUES (new.mail, new.expire, new.code);
        ELSEIF(tg_op='UPDATE') THEN
			IF (NEW.password IS NOT NULL) THEN
               UPDATE admin_panel SET password = new.password WHERE mail = old.mail;
           	END IF;
        ELSEIF(tg_op='DELETE') THEN
			DELETE FROM passwort_zuruecksetzen WHERE code = old.code;
       END IF;
    RETURN NEW;

END;
$$;
	
--Trigger--

CREATE TRIGGER view_scheduleposts_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_scheduleposts
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_scheduleposts();
	
CREATE TRIGGER view_schedulecomments_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_schedulecomments
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_schedulecomments();
	
CREATE TRIGGER view_scheduleobject_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_scheduleobjects
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_scheduleobject();
	
CREATE TRIGGER view_freeforms_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeforms
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeforms();

CREATE TRIGGER view_line_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_line
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_line();
	
CREATE TRIGGER view_freeformembedded_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeformembedded
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeformembedded();

CREATE TRIGGER view_freeformobject_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeformobject
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeformobject();
	
CREATE TRIGGER account_passwort_zuruecksetzen_trigger
    INSTEAD OF INSERT OR DELETE OR UPDATE ON view_account_password_reset
    FOR EACH ROW EXECUTE FUNCTION insert_update_delete_account_password_reset();
	
CREATE TRIGGER admin_panel_trigger
    INSTEAD OF INSERT OR DELETE OR UPDATE ON view_adminpanel
    FOR EACH ROW EXECUTE FUNCTION insert_update_delete_adminpanel();
   
CREATE TRIGGER view_schedules_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_schedules
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_schedules();
	
CREATE TRIGGER freigabe_trigger 
	INSTEAD OF INSERT OR DELETE OR UPDATE ON view_share 
	FOR EACH ROW EXECUTE FUNCTION insert_update_delete_share();

CREATE TRIGGER passwort_zuruecksetzen_trigger
    INSTEAD OF INSERT OR DELETE OR UPDATE ON view_password_reset
    FOR EACH ROW EXECUTE FUNCTION insert_update_delete_password_reset();
	

END;
