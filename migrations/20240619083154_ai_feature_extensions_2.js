// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    columnExists = await knex.schema.hasColumn('ai_user', 'token_quota');
    if (!columnExists) {
        await knex.schema.table('ai_user', function (table) {
            table.integer('token_quota').defaultTo(100000);
        });
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {    
    await knex.schema.alterTable('ai_user', function (table) {
        table.dropColumn('token_quota');
    });
};