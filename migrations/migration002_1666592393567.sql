-- Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
ALTER TABLE freeforms ADD font varchar DEFAULT 'default';
ALTER TABLE schedules ADD font varchar DEFAULT 'default';


CREATE OR REPLACE VIEW view_schedules AS
	SELECT hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, add_position, font FROM schedules s;
	
CREATE OR REPLACE VIEW view_freeforms AS 
	SELECT hashcode, password_pane,password_admin,arrow,line_style,line_color,line_size,like_symbol,background_color,shape, shape_color, comment_color, circle,embeddeds,line_text,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, font FROM freeforms f;


	
CREATE OR REPLACE FUNCTION insert_update_delete_schedules()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
        INSERT INTO schedules(hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash, add_position,font) VALUES
	    (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.column_width, new.embeddeds, new.one_column, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.add_position, new.font);
	ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE schedules SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                             shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, one_column = new.one_column, embeddeds = new.embeddeds, column_width = new.column_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, add_position = new.add_position, font = new.font
	    WHERE hashcode= old.hashcode;
    ELSEIF (TG_OP = 'DELETE') THEN
	DELETE FROM view_password_reset where hashcode = old.hashcode;
	DELETE FROM view_share where pane_hash = old.hashcode;
    DELETE FROM view_scheduleposts WHERE schedule_id = old.hashcode;
    DELETE FROM view_schedulecomments WHERE schedule_id = old.hashcode;
    DELETE FROM view_scheduleobjects where schedule_id = old.hashcode;
    DELETE FROM schedules where hashcode = old.hashcode;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION insert_update_delete_freeforms()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
        INSERT INTO freeforms(hashcode, password_pane, password_admin, arrow, line_style, line_color, line_size, like_symbol, background_color, shape, shape_color, comment_color, circle, embeddeds, line_text, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, font) VALUES
	    (new.hashcode, new.password_pane, new.password_admin, new.arrow, new.line_style, new.line_color, new.line_size, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.circle, new.embeddeds, new.line_text, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font);
	ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE freeforms SET password_pane=new.password_pane, password_admin = new.password_admin, arrow = new.arrow, line_style = new.line_style, line_color = new.line_color, line_size = new.line_size, like_symbol = new.like_symbol, background_color = new.background_color,
                             shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, circle = new.circle, embeddeds = new.embeddeds, line_text = new.line_text, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash = new.no_pass_hash, font = new.font
	    WHERE hashcode= old.hashcode;
    ELSEIF (TG_OP = 'DELETE') THEN
	DELETE FROM view_password_reset where hashcode = old.hashcode;
	DELETE FROM view_share where pane_hash = old.hashcode;
    DELETE FROM view_freeformembedded WHERE freeform_id = old.hashcode;
    DELETE FROM view_freeformobject where freeform_id = old.hashcode;
    DELETE FROM view_line WHERE freeform_id = old.hashcode;
    DELETE FROM freeforms where hashcode = old.hashcode;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';
