// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    columnExists = await knex.schema.hasColumn('ai_user', 'tokens_per_model');
    if (!columnExists) {
        await knex.schema.table('ai_user', function (table) {
            table.jsonb('tokens_per_model');
        });
    }
    
    columnExists = await knex.schema.hasColumn('ai_chat_message_token_usage', 'model');
    if (!columnExists) {
        await knex.schema.table('ai_chat_message_token_usage', function (table) {
            table.string('model');
        });
    }

    tableExists = await knex.schema.hasTable('ai_user_backup');
    if (!tableExists) {
        await knex.schema.createTable('ai_user_backup', function (table) {
            table.string('mail').notNullable();
            table.string('token_used').notNullable();
            table.string('token_quota').notNullable();
            table.string('time').notNullable();
            table.jsonb('tokens_per_model');
            table.primary(['mail', 'time']);
        })
    }

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION activate_ai_mail(email varchar, max_tokens int)
        RETURNS varchar AS $BODY$
        DECLARE
        BEGIN
            IF ((SELECT count(*) FROM admin_panel where mail= email)>0) THEN
                UPDATE admin_panel SET ai=true where mail = email;
                IF ((SELECT count(*) FROM ai_user where mail= email) <1) THEN
                    INSERT INTO ai_user (mail, token_quota) VALUES (email, max_tokens);
                    RETURN 'Neu hinzugefügt: ' || email;
                END IF;
                RETURN 'Nur Update: ' || email;
            END IF;	
            RETURN 'Fehler mit: ' || email;
        END;
        $BODY$
        LANGUAGE plpgsql;`)
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {    
    await knex.schema.alterTable('ai_user', function (table) {
        table.dropColumn('tokens_per_model');
    });
    await knex.schema.alterTable('ai_chat_message_token_usage', function (table) {
        table.dropColumn('model');
    });
    await knex.schema.dropTableIfExists('ai_user_backup');

    await knex.raw(`DROP FUNCTION activate_ai_mail(email varchar,max_tokens int);`)
};