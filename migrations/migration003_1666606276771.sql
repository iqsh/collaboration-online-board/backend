-- Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
ALTER TABLE freeformembedded ADD COLUMN visible boolean DEFAULT TRUE;
ALTER TABLE freeformobject ADD COLUMN visible boolean DEFAULT TRUE;
ALTER TABLE scheduleobjects ADD COLUMN visible boolean DEFAULT TRUE;
ALTER TABLE scheduleposts ADD COLUMN visible boolean DEFAULT TRUE;
ALTER TABLE schedulecomments ADD COLUMN visible boolean DEFAULT TRUE;



CREATE OR REPLACE VIEW view_freeformobject AS 
	SELECT id, hashcode as freeform_id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width, visible FROM freeformobject;

CREATE OR REPLACE VIEW view_freeformembedded AS
	SELECT id, parent_id, hashcode as freeform_id, body, url, type, shape, color, likes, info, visible FROM freeformembedded;

CREATE OR REPLACE VIEW view_scheduleobjects AS 
	SELECT id, hashcode as schedule_id, title, comments, color,shape, order_id, visible FROM scheduleobjects order by order_id::integer;;

CREATE OR REPLACE VIEW view_schedulecomments AS
    SELECT id, parent_id, hashcode as schedule_id, message, likes, shape, color, body, info, type, visible FROM schedulecomments;

CREATE OR REPLACE VIEW view_scheduleposts AS
    SELECT id, parent_id, hashcode as schedule_id, message, likes, type, body, color, shape, comment_length, info, order_id, visible FROM scheduleposts order by order_id::integer;;
	
	
	

   
CREATE OR REPLACE FUNCTION insert_update_delete_freeformobject()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
		DELETE FROM freeformobject where hashcode=new.freeform_id and id = new.id;
        INSERT INTO freeformobject (hashcode, id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width, visible) VALUES (new.freeform_id, new.id, new.title, new.left, new.top, new.comment_length, new.shape, new.color, new.embedded_length, new.body, new.info, new.height, new.width, new.visible);
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformobject';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM view_freeformembedded WHERE freeform_id = old.freeform_id AND parent_id = old.id;
	DELETE FROM view_line WHERE freeform_id = old.freeform_id and start = old.id;
	DELETE FROM view_line WHERE freeform_id = old.freeform_id and "end" = old.id;
    DELETE FROM freeformobject where hashcode = old.freeform_id AND id = old.id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION insert_update_delete_freeformembedded()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
		DELETE FROM freeformembedded where hashcode=new.freeform_id and id = new.id;
        INSERT INTO freeformembedded (hashcode, parent_id, id, body, url, type, likes, shape, color, info, visible) VALUES (new.freeform_id, new.parent_id, new.id, new.body, new.url, new.type, new.likes, new.shape, new.color, new.info, new.visible);
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformembedded';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM freeformembedded where hashcode = old.freeform_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';
	
	


CREATE OR REPLACE FUNCTION insert_update_delete_scheduleobject()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
		DELETE FROM scheduleobjects where hashcode=new.schedule_id and id = new.id;
        INSERT INTO scheduleobjects (hashcode, id, title, comments, color, shape, order_id, visible) VALUES
	    (new.schedule_id, new.id, new.title, new.comments, new.color, new.shape, new.order_id, new.visible);
     ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleobject';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM view_scheduleposts where parent_id = old.id AND schedule_id = old.schedule_id;
    DELETE FROM scheduleobjects where hashcode = old.schedule_id AND id = old.id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION insert_update_delete_schedulecomments()
  RETURNS trigger AS
$$
DECLARE

BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
		DELETE FROM schedulecomments WHERE hashcode=new.schedule_id and id = new.id;
        INSERT INTO schedulecomments (id, parent_id, hashcode, message, likes, shape, color, info, body, type, visible) VALUES (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.shape, new.color, new.info, new.body, new.type, new.visible) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für schedulecomments';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM schedulecomments where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;

END;
$$
LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION insert_update_delete_scheduleposts()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
		DELETE FROM scheduleposts where hashcode=new.schedule_id and id = new.id;
        INSERT INTO scheduleposts (id, parent_id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, visible) VALUES
                                (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleposts';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM schedulecomments where hashcode = old.schedule_id AND parent_id = id;
    DELETE FROM scheduleposts where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';
