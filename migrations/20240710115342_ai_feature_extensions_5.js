// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    columnExists = await knex.schema.hasColumn('ai_person', 'description');
    if (columnExists) {
        await knex.schema.alterTable('ai_person', function (table) {
            table.text('description').alter();
        });
    }
    columnExists = await knex.schema.hasColumn('ai_person', 'system_prompt');
    if (columnExists) {
        await knex.schema.alterTable('ai_person', function (table) {
            table.text('system_prompt').alter();
        });
    }
    columnExists = await knex.schema.hasColumn('ai_chat', 'description');
    if (columnExists) {
        await knex.schema.alterTable('ai_chat', function (table) {
            table.text('description').alter();
        });
    }
    columnExists = await knex.schema.hasColumn('ai_chat', 'system_prompt');
    if (columnExists) {
        await knex.schema.alterTable('ai_chat', function (table) {
            table.text('system_prompt').alter();
        });
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {    
    await knex.schema.alterTable('ai_person', function (table) {
        table.string('description').alter();
    });
    await knex.schema.alterTable('ai_person', function (table) {
        table.string('system_prompt').alter();
    });
    await knex.schema.alterTable('ai_chat', function (table) {
        table.string('description').alter();
    });
    await knex.schema.alterTable('ai_chat', function (table) {
        table.string('system_prompt').alter();
    });
};