-- Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
ALTER TABLE schedules ADD COLUMN folder varchar  DEFAULT '';
ALTER TABLE freeforms ADD COLUMN folder varchar  DEFAULT '';
ALTER TABLE timelines ADD COLUMN folder varchar DEFAULT '';



CREATE TABLE folder (
id varchar PRIMARY KEY,
name varchar,
color varchar,
parent varchar,
"user" varchar
);



CREATE OR REPLACE VIEW view_folder AS
    SELECT id, name,color,parent, "user" FROM folder;
	

CREATE OR REPLACE FUNCTION insert_update_delete_folder()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		IF ((SELECT COUNT(*) FROM folder where id = new.id) <1) THEN
			INSERT INTO folder(id,name,color,parent, "user") VALUES (new.id, new.name, new.color, new.parent, new.user);
		END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		IF ((SELECT COUNT(*) FROM folder where id = new.id) >0) THEN
			UPDATE folder SET name = new.name, color = new.color, parent = new.parent where id = old.id;
		END IF;
    ELSEIF (TG_OP = 'DELETE') THEN
		IF ((SELECT COUNT(*) FROM folder where id = old.id) >0) THEN
			UPDATE schedules SET folder = '' where folder = old.id;
			UPDATE freeforms SET folder = '' where folder = old.id;
			UPDATE timelines SET folder = '' where folder = old.id;
			DELETE FROM folder where id = old.id;
		END IF;
    END IF;
  RETURN NEW;

END;
$$
LANGUAGE 'plpgsql';


CREATE TRIGGER view_folder_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_folder
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_folder();
	


CREATE OR REPLACE VIEW view_folder_pane AS
	SELECT * FROM (
		SELECT folder as id, hashcode FROM schedules where folder IS NOT NULL and folder !=''
		UNION
		SELECT folder as id, hashcode FROM freeforms  where folder IS NOT NULL and folder !=''
		UNION
		SELECT folder as id, hashcode FROM timelines  where folder IS NOT NULL and folder !=''
	) inner_t
	

CREATE OR REPLACE FUNCTION insert_update_delete_folder_pane()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
		IF ((SELECT COUNT(*) FROM schedules where hashcode = new.hashcode) <1) THEN
			IF ((SELECT COUNT(*) FROM freeforms where hashcode = new.hashcode) <1) THEN
				UPDATE timelines SET folder = new.id where hashcode=new.hashcode;
			ELSE
				UPDATE freeforms SET folder = new.id where hashcode=new.hashcode;
			END IF;
		ELSE
			UPDATE schedules SET folder = new.id where hashcode=new.hashcode;
		END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		IF ((SELECT COUNT(*) FROM schedules where hashcode = new.hashcode) <1) THEN
			IF ((SELECT COUNT(*) FROM freeforms where hashcode = new.hashcode) <1) THEN
				UPDATE timelines SET folder = new.id where hashcode=new.hashcode;
			ELSE
				UPDATE freeforms SET folder = new.id where hashcode=new.hashcode;
			END IF;
		ELSE
			UPDATE schedules SET folder = new.id where hashcode=new.hashcode;
		END IF;
    ELSEIF (TG_OP = 'DELETE') THEN
		IF ((SELECT COUNT(*) FROM schedules where hashcode = old.hashcode) <1) THEN
			IF ((SELECT COUNT(*) FROM freeforms where hashcode = old.hashcode) <1) THEN
				DELETE FROM timelines where hashcode=old.hashcode;
			ELSE
				DELETE FROM freeforms where hashcode=old.hashcode;
			END IF;
		ELSE
			DELETE FROM schedules where hashcode=old.hashcode;
		END IF;
		DELETE FROM folder where id = old.id;
    END IF;
  RETURN NEW;

END;
$$
LANGUAGE 'plpgsql';


CREATE TRIGGER view_folder_pane_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_folder_pane
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_folder_pane();
	


CREATE OR REPLACE VIEW view_all_boards AS
	SELECT t.hash_pane, a.dashboard as hash_dash, t.type, t.mail, t.headline, t.subheadline, t.no_pass_hash, t.background_image, t.background_color, t.last_update, t.folder FROM (SELECT * FROM (SELECT s.hashcode as hash_pane, s.folder, CASE WHEN (one_column IS TRUE) THEN 'Stream' ELSE 'Schedule' END as type, s.mail,s.headline, s.subheadline, s.no_pass_hash, s.background_image, s.background_color, CASE WHEN (sp.last_update IS NULL) THEN s.time else sp.last_update end as last_update FROM schedules s LEFT JOIN (SELECT max(spost.time) as last_update, spost.hashcode FROM scheduleposts spost GROUP BY spost.hashcode) sp ON s.hashcode=sp.hashcode) as s
     UNION SELECT f.hashcode as hash_pane, f.folder, 'Freeform' as type, f.mail,f.headline, f.subheadline, f.no_pass_hash, f.background_image, f.background_color, CASE WHEN (fo.last_update IS NULL) THEN f.time else fo.last_update end as last_update FROM freeforms f LEFT JOIN (SELECT max(fobj.time) as last_update, fobj.hashcode FROM freeformobject fobj GROUP BY fobj.hashcode) fo ON f.hashcode=fo.hashcode
	 UNION SELECT * FROM (SELECT ti.hashcode as hash_pane, ti.folder,'Timeline' as type, ti.mail,ti.headline, ti.subheadline, ti.no_pass_hash, ti.background_image, ti.background_color, CASE WHEN (tp.last_update IS NULL) THEN ti.time else tp.last_update end as last_update FROM timelines ti LEFT JOIN (SELECT max(tpost.time) as last_update, tpost.hashcode FROM timelineposts tpost GROUP BY tpost.hashcode) tp ON ti.hashcode=tp.hashcode) as ti) as t
         LEFT JOIN admin_panel a on t.mail = a.mail;
		 