// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function(knex) {
    let columnExists = await knex.schema.hasColumn('schedules','authorize');
    if(!columnExists){
        await knex.schema.table('schedules', function(table) {
            table.boolean('authorize').defaultTo(false);
        });
    }
    columnExists = await knex.schema.hasColumn('freeforms','authorize');
    if(!columnExists){
        await knex.schema.table('freeforms', function(table) {
            table.boolean('authorize').defaultTo(false);
        });
    }
    columnExists = await knex.schema.hasColumn('timelines','authorize');
    if(!columnExists){
        await knex.schema.table('timelines', function(table) {
            table.boolean('authorize').defaultTo(false);
        });
    }

    columnExists = await knex.schema.hasColumn('scheduleposts','authorized');
    if(!columnExists){
        await knex.schema.table('scheduleposts', function(table) {
            table.boolean('authorized').defaultTo(true);
        });
    }
    columnExists = await knex.schema.hasColumn('freeformobject','authorized');
    if(!columnExists){
        await knex.schema.table('freeformobject', function(table) {
            table.boolean('authorized').defaultTo(true);
        });
    }
    columnExists = await knex.schema.hasColumn('timelineposts','authorized');
    if(!columnExists){
        await knex.schema.table('timelineposts', function(table) {
            table.boolean('authorized').defaultTo(true);
        });
    }

    await knex.schema.createViewOrReplace('view_freeforms', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'arrow', 'line_style', 'line_color', 'line_size', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'circle', 'embeddeds', 'line_text', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'font', 'authorize').from('freeforms'));
    });

    await knex.schema.createViewOrReplace('view_schedules', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'column_width', 'embeddeds', 'one_column', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'add_position', 'font', 'authorize').from('schedules as s'));
    });
    
    await knex.schema.createViewOrReplace('view_timelines', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'element_width', 'embeddeds', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'font', 'line_color', 'line_size', 'authorize').from('timelines as s'));
    });

    await knex.schema.createViewOrReplace('view_freeformobject', function(view) {
        view.as(knex.select('id', 'hashcode as freeform_id', 'title', "left", 'top', 'comment_length', 'shape', 'color', 'embedded_length', 'body', 'info', 'height', 'width', 'visible', 'authorized').from('freeformobject'));
    });

    await knex.schema.createViewOrReplace('view_scheduleposts', function(view) {
        view.as(knex.select('id', 'parent_id', 'hashcode as schedule_id', 'message', 'likes', 'type', 'body', 'color', 'shape', 'comment_length', 'info', 'order_id', 'visible', 'authorized').from('scheduleposts').orderByRaw('order_id::integer'));
    });

    await knex.schema.createViewOrReplace('view_timelineposts', function(view) {
        view.as(knex.select('id', 'hashcode as timeline_id', 'message', 'likes', 'type', 'body', 'color', 'shape', 'comment_length', 'info', 'order_id', 'sichtbar as visible', 'authorized').from('timelineposts'));
    });


    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_schedules()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO schedules(hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash, add_position, authorize) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.column_width, new.embeddeds, new.one_column, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.add_position, new.authorize);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE schedules SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, one_column = new.one_column, embeddeds = new.embeddeds, column_width = new.column_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, add_position = new.add_position,authorize=new.authorize
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites where hashcode=old.hashcode;
        DELETE FROM view_scheduleposts WHERE schedule_id = old.hashcode;
        DELETE FROM view_schedulecomments WHERE schedule_id = old.hashcode;
        DELETE FROM view_scheduleobjects where schedule_id = old.hashcode;
        DELETE FROM schedules where hashcode = old.hashcode;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);
    
    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_freeforms()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO freeforms(hashcode, password_pane, password_admin, arrow, line_style, line_color, line_size, like_symbol, background_color, shape, shape_color, comment_color, circle, embeddeds, line_text, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, font,authorize) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.arrow, new.line_style, new.line_color, new.line_size, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.circle, new.embeddeds, new.line_text, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font, new.authorize);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE freeforms SET password_pane=new.password_pane, password_admin = new.password_admin, arrow = new.arrow, line_style = new.line_style, line_color = new.line_color, line_size = new.line_size, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, circle = new.circle, embeddeds = new.embeddeds, line_text = new.line_text, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash = new.no_pass_hash, font = new.font, authorize=new.authorize
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites where hashcode=old.hashcode;
        DELETE FROM view_freeformembedded WHERE freeform_id = old.hashcode;
        DELETE FROM view_freeformobject where freeform_id = old.hashcode;
        DELETE FROM view_line WHERE freeform_id = old.hashcode;
        DELETE FROM freeforms where hashcode = old.hashcode;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelines()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO timelines (hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash,font,authorize) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.element_width, new.embeddeds,new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font, new.authorize);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE timelines SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, embeddeds = new.embeddeds, element_width = new.element_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, font = new.font, authorize=new.authorize
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites WHERE hashcode = old.hashcode;
        DELETE FROM view_timelineposts WHERE timeline_id = old.hashcode;
        DELETE FROM view_timelinecomments WHERE timeline_id = old.hashcode;
        DELETE FROM timelines where hashcode = old.hashcode;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_freeformobject()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
            DELETE FROM freeformobject where hashcode=new.freeform_id and id = new.id;
            INSERT INTO freeformobject (hashcode, id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width, visible,authorized) VALUES (new.freeform_id, new.id, new.title, new.left, new.top, new.comment_length, new.shape, new.color, new.embedded_length, new.body, new.info, new.height, new.width, new.visible, new.authorized);
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformobject';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
                RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_freeformembedded WHERE freeform_id = old.freeform_id AND parent_id = old.id;
        DELETE FROM view_line WHERE freeform_id = old.freeform_id and start = old.id;
        DELETE FROM view_line WHERE freeform_id = old.freeform_id and "end" = old.id;
        DELETE FROM freeformobject where hashcode = old.freeform_id AND id = old.id;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_scheduleposts()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
            DELETE FROM scheduleposts where hashcode=new.schedule_id and id = new.id;
            INSERT INTO scheduleposts (id, parent_id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, visible,authorized) VALUES
                                    (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible,new.authorized) ;
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleposts';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM schedulecomments where hashcode = old.schedule_id AND parent_id = id;
        DELETE FROM scheduleposts where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelineposts()
  RETURNS trigger AS
      $$
      DECLARE
      BEGIN
      IF (TG_OP = 'INSERT') THEN
      IF ((SELECT COUNT(*) FROM view_timelines where hashcode = new.timeline_id) > 0) THEN
          DELETE FROM timelineposts where hashcode=new.timeline_id and id = new.id;
          INSERT INTO timelineposts (id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, sichtbar,authorized) VALUES
                                  (new.id, new.timeline_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible, new.authorized) ;
      ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für timelineposts';
      END IF;
      ELSEIF (TG_OP = 'UPDATE') THEN
          RAISE EXCEPTION 'Update nicht erlaubt!';
      ELSEIF (TG_OP = 'DELETE') THEN
      DELETE FROM timelinecomments where hashcode = old.timeline_id;
      DELETE FROM timelineposts where hashcode = old.timeline_id AND id = old.id;
      END IF;
      RETURN NEW;
      END;
      $$
    LANGUAGE 'plpgsql';
  `);

};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function(knex) {
    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_schedules()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO schedules(hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash, add_position) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.column_width, new.embeddeds, new.one_column, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.add_position);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE schedules SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, one_column = new.one_column, embeddeds = new.embeddeds, column_width = new.column_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, add_position = new.add_position
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites where hashcode=old.hashcode;
        DELETE FROM view_scheduleposts WHERE schedule_id = old.hashcode;
        DELETE FROM view_schedulecomments WHERE schedule_id = old.hashcode;
        DELETE FROM view_scheduleobjects where schedule_id = old.hashcode;
        DELETE FROM schedules where hashcode = old.hashcode;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);
    
    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_freeforms()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO freeforms(hashcode, password_pane, password_admin, arrow, line_style, line_color, line_size, like_symbol, background_color, shape, shape_color, comment_color, circle, embeddeds, line_text, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, font) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.arrow, new.line_style, new.line_color, new.line_size, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.circle, new.embeddeds, new.line_text, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE freeforms SET password_pane=new.password_pane, password_admin = new.password_admin, arrow = new.arrow, line_style = new.line_style, line_color = new.line_color, line_size = new.line_size, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, circle = new.circle, embeddeds = new.embeddeds, line_text = new.line_text, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash = new.no_pass_hash, font = new.font
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites where hashcode=old.hashcode;
        DELETE FROM view_freeformembedded WHERE freeform_id = old.hashcode;
        DELETE FROM view_freeformobject where freeform_id = old.hashcode;
        DELETE FROM view_line WHERE freeform_id = old.hashcode;
        DELETE FROM freeforms where hashcode = old.hashcode;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelines()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO timelines (hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash,font) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.element_width, new.embeddeds,new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE timelines SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, embeddeds = new.embeddeds, element_width = new.element_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, font = new.font
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites WHERE hashcode = old.hashcode;
        DELETE FROM view_timelineposts WHERE timeline_id = old.hashcode;
        DELETE FROM view_timelinecomments WHERE timeline_id = old.hashcode;
        DELETE FROM timelines where hashcode = old.hashcode;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelineposts()
    RETURNS trigger AS
        $$
        DECLARE
        BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_timelines where hashcode = new.timeline_id) > 0) THEN
            DELETE FROM timelineposts where hashcode=new.timeline_id and id = new.id;
            INSERT INTO timelineposts (id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, sichtbar) VALUES
                                    (new.id, new.timeline_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible) ;
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für timelineposts';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM timelinecomments where hashcode = old.timeline_id;
        DELETE FROM timelineposts where hashcode = old.timeline_id AND id = old.id;
        END IF;
        RETURN NEW;
        END;
        $$
    LANGUAGE 'plpgsql';
    `);

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_scheduleposts()
    RETURNS trigger AS
        $$
        DECLARE
        BEGIN
            IF (TG_OP = 'INSERT') THEN
            IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
                DELETE FROM scheduleposts where hashcode=new.schedule_id and id = new.id;
                INSERT INTO scheduleposts (id, parent_id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, visible) VALUES
                                        (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible) ;
            ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleposts';
            END IF;
            ELSEIF (TG_OP = 'UPDATE') THEN
                RAISE EXCEPTION 'Update nicht erlaubt!';
            ELSEIF (TG_OP = 'DELETE') THEN
            DELETE FROM schedulecomments where hashcode = old.schedule_id AND parent_id = id;
            DELETE FROM scheduleposts where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
            END IF;
        RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';
    `);

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_freeformobject()
    RETURNS trigger AS
        $$
        DECLARE
        BEGIN
            IF (TG_OP = 'INSERT') THEN
            IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
                DELETE FROM freeformobject where hashcode=new.freeform_id and id = new.id;
                INSERT INTO freeformobject (hashcode, id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width, visible) VALUES (new.freeform_id, new.id, new.title, new.left, new.top, new.comment_length, new.shape, new.color, new.embedded_length, new.body, new.info, new.height, new.width, new.visible);
            ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformobject';
            END IF;
            ELSEIF (TG_OP = 'UPDATE') THEN
                    RAISE EXCEPTION 'Update nicht erlaubt!';
            ELSEIF (TG_OP = 'DELETE') THEN
            DELETE FROM view_freeformembedded WHERE freeform_id = old.freeform_id AND parent_id = old.id;
            DELETE FROM view_line WHERE freeform_id = old.freeform_id and start = old.id;
            DELETE FROM view_line WHERE freeform_id = old.freeform_id and "end" = old.id;
            DELETE FROM freeformobject where hashcode = old.freeform_id AND id = old.id;
            END IF;
            RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';
    `);


    await knex.schema.raw(`DROP VIEW IF EXISTS view_scheduleposts`);
    await knex.schema.raw(`DROP VIEW IF EXISTS view_freeformobject`);
    await knex.schema.raw(`DROP VIEW IF EXISTS view_timelineposts`);
    await knex.schema.raw(`DROP VIEW IF EXISTS view_schedules`);
    await knex.schema.raw(`DROP VIEW IF EXISTS view_freeforms`);
    await knex.schema.raw(`DROP VIEW IF EXISTS view_timelines`);

    await knex.schema.createViewOrReplace('view_timelineposts', function(view) {
        view.as(knex.select('id', 'hashcode as timeline_id', 'message', 'likes', 'type', 'body', 'color', 'shape', 'comment_length', 'info', 'order_id', 'sichtbar as visible').from('timelineposts'));
    });

    await knex.schema.createViewOrReplace('view_scheduleposts', function(view) {
        view.as(knex.select('id', 'parent_id', 'hashcode as schedule_id', 'message', 'likes', 'type', 'body', 'color', 'shape', 'comment_length', 'info', 'order_id', 'visible').from('scheduleposts').orderByRaw('order_id::integer'));
    });

    await knex.schema.createViewOrReplace('view_freeformobject', function(view) {
        view.as(knex.select('id', 'hashcode as freeform_id', 'title', "left", 'top', 'comment_length', 'shape', 'color', 'embedded_length', 'body', 'info', 'height', 'width', 'visible').from('freeformobject'));
    });
    
    await knex.schema.createViewOrReplace('view_freeforms', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'arrow', 'line_style', 'line_color', 'line_size', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'circle', 'embeddeds', 'line_text', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'font').from('freeforms'));
    });

    await knex.schema.createViewOrReplace('view_schedules', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'column_width', 'embeddeds', 'one_column', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'add_position', 'font').from('schedules as s'));
    });

    await knex.schema.createViewOrReplace('view_timelines', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'element_width', 'embeddeds', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'font', 'line_color', 'line_size').from('timelines as s'));
    });

    await knex.schema.raw('DROP TRIGGER IF EXISTS view_scheduleposts_trigger ON view_scheduleposts');
    await knex.schema.raw(`CREATE TRIGGER view_scheduleposts_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_scheduleposts
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_scheduleposts();
    `);

    await knex.schema.raw('DROP TRIGGER IF EXISTS view_freeforms_trigger ON view_freeforms');
    await knex.schema.raw(`CREATE TRIGGER view_freeforms_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeforms
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeforms();
    `);
    
    await knex.schema.raw('DROP TRIGGER IF EXISTS view_freeformobject_trigger ON view_freeformobject');
    await knex.schema.raw(`CREATE TRIGGER view_freeformobject_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeformobject
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeformobject();
    `);
        
    await knex.schema.raw('DROP TRIGGER IF EXISTS view_schedules_trigger ON view_schedules');
    await knex.schema.raw(`CREATE TRIGGER view_schedules_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_schedules
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_schedules();
    `);
    
    await knex.schema.raw('DROP TRIGGER IF EXISTS view_timelines_trigger ON view_timelines');
    await knex.schema.raw(`CREATE TRIGGER view_timelines_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelines
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelines();
    `);

    await knex.schema.raw('DROP TRIGGER IF EXISTS view_timelinesposts_trigger ON view_timelineposts');
    await knex.schema.raw(`CREATE TRIGGER view_timelinesposts_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelineposts
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelineposts();
    `);

    await knex.schema.alterTable('scheduleposts', function(table) {
        table.dropColumn('authorized');
    });
    await knex.schema.alterTable('freeformobject', function(table) {
        table.dropColumn('authorized');
    });
    await knex.schema.alterTable('timelineposts', function(table) {
        table.dropColumn('authorized');
    });

    await knex.schema.alterTable('schedules', function(table) {
        table.dropColumn('authorize');
    });
    await knex.schema.alterTable('freeforms', function(table) {
        table.dropColumn('authorize');
    });
    await knex.schema.alterTable('timelines', function(table) {
        table.dropColumn('authorize');
    });
};
