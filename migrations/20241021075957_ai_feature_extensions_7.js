// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function(knex) {
    columnExists = await knex.schema.hasColumn('ai_chat_message_session', 'timestamp');
    if (!columnExists) {
        await knex.schema.table('ai_chat_message_session', function (table) {
            table.dateTime('timestamp').defaultTo(knex.fn.now());
        });
    }

    await knex.schema.raw(`CREATE OR REPLACE FUNCTION delete_old_sessions()
       RETURNS VOID AS $$
        BEGIN
            DELETE FROM ai_chat_message_session AS m
            USING ai_chat AS c
            WHERE m.chat_uuid = c.uuid
            AND (
                (c.type = 'image' AND m.timestamp < NOW() - INTERVAL '60 minutes')
                OR
                ((c.type = 'chat' or c.type = 'person') AND m.timestamp < NOW() - INTERVAL '5 minutes')
                );
        END;
        $$ LANGUAGE plpgsql;`)
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function(knex) {
    await knex.schema.alterTable('ai_chat_message_session', function (table) {
        table.dropColumn('timestamp');
    });

    await knex.schema.raw(`DROP FUNCTION delete_old_sessions();`)
};
