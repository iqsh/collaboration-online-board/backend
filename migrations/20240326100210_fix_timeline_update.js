// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelines()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO timelines (hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash,font, line_color, line_size, authorize) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.element_width, new.embeddeds,new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font, new.line_color, new.line_size, new.authorize);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE timelines SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, embeddeds = new.embeddeds, element_width = new.element_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, font = new.font, authorize = new.authorize,line_color = new.line_color, line_size = new.line_size
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites WHERE hashcode = old.hashcode;
        DELETE FROM view_timelineposts WHERE timeline_id = old.hashcode;
        DELETE FROM view_timelinecomments WHERE timeline_id = old.hashcode;
        DELETE FROM timelines where hashcode = old.hashcode;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelines()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO timelines (hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash,font, authorize) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.element_width, new.embeddeds,new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font, new.authorize);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE timelines SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, embeddeds = new.embeddeds, element_width = new.element_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, font = new.font, autohorize = new.authorize
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM favourites WHERE hashcode = old.hashcode;
        DELETE FROM view_timelineposts WHERE timeline_id = old.hashcode;
        DELETE FROM view_timelinecomments WHERE timeline_id = old.hashcode;
        DELETE FROM timelines where hashcode = old.hashcode;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);
};