// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function(knex) {
    let tableExists = await knex.schema.hasTable('registration');
    if(!tableExists){
        await knex.schema.createTable('registration', function(table) {
            table.text('mail');
            table.text('expire');
            table.text('code').notNullable();
            table.string('dnr');
            table.string('hashcode');
            table.string('type');
        });
    };

    tableExists = await knex.schema.hasTable('freigabe');
    if(!tableExists){
        await knex.schema.createTable('freigabe', function(table) {
            table.string('pane_hash');
            table.string('ende');
            table.string('prev_hash');
            table.boolean('likes');
            table.boolean('comments');
          });
    };

    tableExists = await knex.schema.hasTable('admin_panel');
    if(!tableExists){
        await knex.schema.createTable('admin_panel', function(table) {
            table.string('mail').primary();
            table.string('dashboard');
            table.string('password');
            table.string('dnr').defaultTo('');
          });
    };

    tableExists = await knex.schema.hasTable('passwort_zuruecksetzen');
    if(!tableExists){
        await knex.schema.createTable('passwort_zuruecksetzen', function(table) {
            table.string('hashcode');
            table.string('mail');
            table.string('abgelaufen');
            table.string('code').primary();
          });
    };

    tableExists = await knex.schema.hasTable('freeformembedded');
    if(!tableExists){
        await knex.schema.createTable('freeformembedded', function(table) {
            table.string('hashcode');
            table.string('parent_id');
            table.string('id');
            table.string('body');
            table.string('url');
            table.string('type');
            table.integer('likes');
            table.string('shape');
            table.string('color');
            table.string('info');
            table.string('time').defaultTo(knex.fn.now());
            table.primary(['hashcode', 'parent_id', 'id']);
            table.boolean('visible').defaultTo(true);
          });
    };

    tableExists = await knex.schema.hasTable('freeformobject');
    if(!tableExists){
        await knex.schema.createTable('freeformobject', function(table) {
            table.string('hashcode');
            table.string('id');
            table.string('title');
            table.string('left');
            table.string('top');
            table.integer('comment_length');
            table.string('shape');
            table.string('color');
            table.integer('embedded_length');
            table.string('body');
            table.string('info');
            table.string('height');
            table.string('width');
            table.string('time').defaultTo(knex.fn.now());
            table.primary(['hashcode', 'id']);
            table.boolean('visible').defaultTo(true);
          });
    };

    tableExists = await knex.schema.hasTable('freeforms');
    if(!tableExists){
        await knex.schema.createTable('freeforms', function(table) {
            table.string('hashcode').primary();
            table.string('password_pane');
            table.string('password_admin');
            table.string('arrow');
            table.string('line_style');
            table.string('line_color');
            table.integer('line_size');
            table.string('like_symbol');
            table.string('background_color');
            table.string('shape');
            table.string('shape_color');
            table.string('comment_color');
            table.boolean('circle');
            table.boolean('embeddeds');
            table.boolean('line_text');
            table.boolean('likes');
            table.string('font_size');
            table.boolean('comments');
            table.integer('count');
            table.string('unlocked');
            table.string('headline');
            table.string('subheadline');
            table.string('background_image');
            table.string('public_key');
            table.string('private_key');
            table.string('iv');
            table.string('mail');
            table.string('dnr');
            table.string('time').defaultTo(knex.fn.now());
            table.string('no_pass_hash');
            table.string('font').defaultTo('default');
          });
    };

    tableExists = await knex.schema.hasTable('schedulecomments');
    if(!tableExists){
        await knex.schema.createTable('schedulecomments', function(table) {
            table.string('hashcode');
            table.string('parent_id');
            table.string('id');
            table.string('message');
            table.integer('likes');
            table.string('shape');
            table.string('color');
            table.string('body');
            table.string('info');
            table.string('type');
            table.string('time').defaultTo(knex.fn.now());
            table.primary(['hashcode', 'parent_id', 'id']);
            table.boolean('visible').defaultTo(true);
          });
    };

    tableExists = await knex.schema.hasTable('scheduleposts');
    if(!tableExists){
        await knex.schema.createTable('scheduleposts', function(table) {
            table.string('hashcode');
            table.string('parent_id');
            table.string('id');
            table.string('message');
            table.string('body');
            table.string('type');
            table.integer('likes');
            table.string('shape');
            table.string('color');
            table.string('info');
            table.integer('comment_length');
            table.string('order_id');
            table.string('time').defaultTo(knex.fn.now());
            table.primary(['hashcode', 'parent_id', 'id']);
            table.boolean('visible').defaultTo(true);
          });
    };

    tableExists = await knex.schema.hasTable('scheduleobjects');
    if(!tableExists){
        await knex.schema.createTable('scheduleobjects', function(table) {
            table.string('hashcode');
            table.string('id');
            table.string('title');
            table.integer('comments');
            table.string('color');
            table.string('shape');
            table.string('order_id');
            table.string('time').defaultTo(knex.fn.now());
            table.primary(['hashcode', 'id']);
            table.boolean('visible').defaultTo(true);
          });
    };

    tableExists = await knex.schema.hasTable('schedules');
    if(!tableExists){
        await knex.schema.createTable('schedules', function(table) {
            table.string('hashcode').primary();
            table.string('password_pane');
            table.string('password_admin');
            table.string('like_symbol');
            table.string('background_color');
            table.string('shape');
            table.string('shape_color');
            table.string('comment_color');
            table.string('column_width');
            table.boolean('embeddeds');
            table.boolean('one_column');
            table.boolean('likes');
            table.string('font_size');
            table.boolean('comments');
            table.integer('count');
            table.string('unlocked');
            table.string('headline');
            table.string('subheadline');
            table.string('background_image');
            table.string('public_key');
            table.string('private_key');
            table.string('iv');
            table.string('mail');
            table.string('dnr');
            table.string('time').defaultTo(knex.fn.now());
            table.string('no_pass_hash');
            table.string('add_position').defaultTo('bottom');
            table.string('font').defaultTo('default');
          });
    };

    tableExists = await knex.schema.hasTable('lines');
    if(!tableExists){
        await knex.schema.createTable('lines', function(table) {
            table.string('id');
            table.string('hashcode');
            table.string('start');
            table.string('end');
            table.string('text');
            table.string('size');
            table.boolean('dash');
            table.string('color');
            table.primary(['id', 'hashcode']);
          });
    };

    tableExists = await knex.schema.hasTable('dashboard');
    if(!tableExists){
        await knex.schema.createTable('dashboard', function(table) {
            table.string('timestamp').notNullable();
            table.string('cpu');
            table.string('memory');
            table.string('heap');
            table.string('response_time');
            table.string('requests_per_second');
            table.integer('user_count');
            table.integer('pane_count');
            table.primary('timestamp');
          });
    };

    tableExists = await knex.schema.hasTable('session_store');
    if(!tableExists){
        await knex.schema.createTable('session_store', function(table) {
            table.string('session_id');
            table.string('mail');
            table.string('expire');
            table.primary('session_id');
          });
    };

    tableExists = await knex.schema.hasTable('timelines');
    if(!tableExists){
        await knex.schema.createTable('timelines', function(table) {
            table.string('hashcode').primary();
            table.string('password_pane');
            table.string('password_admin');
            table.string('like_symbol');
            table.string('background_color');
            table.string('shape');
            table.string('shape_color');
            table.string('comment_color');
            table.string('element_width');
            table.boolean('embeddeds');
            table.boolean('likes');
            table.string('font_size');
            table.boolean('comments');
            table.integer('count');
            table.string('unlocked');
            table.string('headline');
            table.string('subheadline');
            table.string('background_image');
            table.string('public_key');
            table.string('private_key');
            table.string('iv');
            table.string('mail');
            table.string('dnr');
            table.string('no_pass_hash');
            table.string('time').defaultTo(knex.fn.now());
            table.string('font').defaultTo('default');
            table.string('line_color').defaultTo('#666666');
            table.integer('line_size').defaultTo(2);
          });
    };

    tableExists = await knex.schema.hasTable('timelinecomments');
    if(!tableExists){
        await knex.schema.createTable('timelinecomments', function(table) {
            table.string('hashcode');
            table.string('parent_id');
            table.string('id');
            table.string('message');
            table.integer('likes');
            table.string('shape');
            table.string('color');
            table.string('body');
            table.string('info');
            table.string('type');
            table.string('time').defaultTo(knex.fn.now());
            table.boolean('sichtbar').defaultTo(true);
            table.primary(['hashcode', 'parent_id', 'id']);
          });
    };


    tableExists = await knex.schema.hasTable('timelineposts');
    if(!tableExists){
        await knex.schema.createTable('timelineposts', function(table) {
            table.string('hashcode');
            table.string('id');
            table.string('message');
            table.string('body');
            table.string('type');
            table.integer('likes');
            table.string('shape');
            table.string('color');
            table.string('info');
            table.integer('comment_length');
            table.string('order_id');
            table.string('time').defaultTo(knex.fn.now());
            table.boolean('sichtbar').defaultTo(true);
            table.primary(['hashcode', 'id']);
          });
    };

    await knex.schema.createViewOrReplace('view_dashboard', function(view) {
        view.as(knex.select('timestamp', 'cpu', 'memory', 'heap', 'response_time', 'requests_per_second', 'user_count', 'pane_count')
          .from('dashboard')
          .orderBy('timestamp'));
    });
    
    await knex.schema.createViewOrReplace('view_freeforms', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'arrow', 'line_style', 'line_color', 'line_size', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'circle', 'embeddeds', 'line_text', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'font').from('freeforms'));
    });
    
    await knex.schema.createViewOrReplace('view_adminpanel', function(view) {
        view.as(knex.select('mail', 'dashboard', 'password', 'dnr')
          .from('admin_panel'));
    });

    await knex.schema.createViewOrReplace('view_line', function(view) {
        view.as(knex
            .select('id', 'hashcode as freeform_id', 'start', 'end', 'text as line_text', 'size', 'dash', 'color')
            .from('lines')
        );
    });

    
    await knex.schema.createViewOrReplace('view_schedules', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'column_width', 'embeddeds', 'one_column', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'add_position', 'font').from('schedules as s'));
    });

    
    await knex.schema.createViewOrReplace('view_account_password_reset', function(view) {
        view.as(knex
          .select('pr.mail', { expire: 'pr.abgelaufen' }, 'pr.code', 'ap.password')
          .from('passwort_zuruecksetzen as pr')
          .leftJoin('admin_panel as ap', 'pr.mail', 'ap.mail')
          .whereNotNull('pr.mail')
        );
    });

    await knex.schema.createViewOrReplace('view_freeform_size', function(view) {
        view.as(knex
          .select('f.hashcode', 'f.no_pass_hash', knex.raw(`max(replace(fo.top::text, 'px'::text, ''::text)::double precision + COALESCE(replace(fo.height::text, 'px'::text, ''::text)::double precision, 400::double precision)) as max_height`), knex.raw(`max(replace(fo."left"::text, 'px'::text, ''::text)::double precision + COALESCE(replace(fo.width::text, 'px'::text, ''::text)::double precision, 400::double precision)) as max_width`))
          .from('freeformobject as fo')
          .join('freeforms as f', knex.raw('fo.hashcode::text = f.hashcode::text'))
          .groupBy('f.hashcode', 'f.no_pass_hash')
        );
      });


    await knex.schema.createViewOrReplace('view_freeformobject', function(view) {
        view.as(knex.select('id', 'hashcode as freeform_id', 'title', "left", 'top', 'comment_length', 'shape', 'color', 'embedded_length', 'body', 'info', 'height', 'width', 'visible').from('freeformobject'));
    });
    
    await knex.schema.createViewOrReplace('view_freeformembedded', function(view) {
        view.as(knex.select('id', 'parent_id', 'hashcode as freeform_id', 'body', 'url', 'type', 'shape', 'color', 'likes', 'info', 'visible').from('freeformembedded'));
    });
    
    await knex.schema.createViewOrReplace('view_scheduleobjects', function(view) {
        view.as(knex.select('id', 'hashcode as schedule_id', 'title', 'comments', 'color', 'shape', 'order_id', 'visible').from('scheduleobjects').orderByRaw('order_id::integer'));
    });
    
    await knex.schema.createViewOrReplace('view_schedulecomments', function(view) {
        view.as(knex.select('id', 'parent_id', 'hashcode as schedule_id', 'message', 'likes', 'shape', 'color', 'body', 'info', 'type', 'visible').from('schedulecomments'));
    });
    
    await knex.schema.createViewOrReplace('view_scheduleposts', function(view) {
        view.as(knex.select('id', 'parent_id', 'hashcode as schedule_id', 'message', 'likes', 'type', 'body', 'color', 'shape', 'comment_length', 'info', 'order_id', 'visible').from('scheduleposts').orderByRaw('order_id::integer'));
    });

    await knex.schema.createViewOrReplace('view_timelines', function(view) {
        view.as(knex.select('hashcode', 'password_pane', 'password_admin', 'like_symbol', 'background_color', 'shape', 'shape_color', 'comment_color', 'element_width', 'embeddeds', 'likes', 'font_size', 'comments', 'count', 'unlocked', 'headline', 'subheadline', 'background_image', 'public_key', 'private_key', 'iv', 'mail', 'dnr', 'no_pass_hash', 'font', 'line_color', 'line_size').from('timelines as s'));
    });

    await knex.schema.createViewOrReplace('view_timelinecomments', function(view) {
        view.as(knex.select('id', 'parent_id', 'hashcode as timeline_id', 'message', 'likes', 'shape', 'color', 'body', 'info', 'type', 'sichtbar as visible').from('timelinecomments'));
    });
    
    await knex.schema.createViewOrReplace('view_timelineposts', function(view) {
        view.as(knex.select('id', 'hashcode as timeline_id', 'message', 'likes', 'type', 'body', 'color', 'shape', 'comment_length', 'info', 'order_id', 'sichtbar as visible').from('timelineposts'));
    });

    await knex.schema.createViewOrReplace('view_password_reset', function(view) {
        view.as(knex.select(['t.hashcode', 't.mail', 't.boardname', 'pr.abgelaufen as expire', 'pr.code', 'password_admin', 'password_pane'])
        .from(function() {
          this.select(['hashcode', 'mail', knex.raw("'Schedule' as boardname"), 'password_admin', 'password_pane'])
            .from('schedules')
            .union(function() {
              this.select(['hashcode', 'mail', knex.raw("'Freeform' as boardname"), 'password_admin', 'password_pane'])
                .from('freeforms')
            })
            .union(function() {
              this.select(['hashcode', 'mail', knex.raw("'Timeline' as boardname"), 'password_admin', 'password_pane'])
                .from('timelines')
            })
            .as('t');
        })
        .leftJoin('passwort_zuruecksetzen as pr', 't.hashcode', '=', 'pr.hashcode')
        );
    });

    await knex.schema.createViewOrReplace('view_share', function(view) {
        view.as(knex.select('f.pane_hash', 'f.ende', 'f.prev_hash', 'f.likes', 'f.comments')
        .select(knex.raw(`CASE 
          WHEN pane_hash IN (SELECT hashcode FROM schedules) AND ((SELECT one_column from schedules where hashcode=pane_hash) = 'true') THEN 'Stream' 
          WHEN pane_hash IN (SELECT hashcode FROM schedules) THEN 'Schedule' 
          WHEN pane_hash IN (SELECT hashcode FROM timelines) THEN 'Timeline' 
          ELSE 'Freeform' 
          END as typname`
        ))
        .from('freigabe as f'));
    });

    await knex.schema.raw(`DROP VIEW IF EXISTS view_all_boards`);
    await knex.schema.raw(`CREATE OR REPLACE VIEW view_all_boards AS
        SELECT t.hash_pane, a.dashboard as hash_dash, t.type, t.mail, t.headline, t.subheadline, t.no_pass_hash, t.background_image, t.background_color, t.last_update FROM (SELECT * FROM (SELECT s.hashcode as hash_pane, CASE WHEN (one_column IS TRUE) THEN 'Stream' ELSE 'Schedule' END as type, s.mail,s.headline, s.subheadline, s.no_pass_hash, s.background_image, s.background_color, CASE WHEN (sp.last_update IS NULL) THEN s.time else sp.last_update end as last_update FROM schedules s LEFT JOIN (SELECT max(spost.time) as last_update, spost.hashcode FROM scheduleposts spost GROUP BY spost.hashcode) sp ON s.hashcode=sp.hashcode) as s
        UNION SELECT f.hashcode as hash_pane, 'Freeform' as type, f.mail,f.headline, f.subheadline, f.no_pass_hash, f.background_image, f.background_color, CASE WHEN (fo.last_update IS NULL) THEN f.time else fo.last_update end as last_update FROM freeforms f LEFT JOIN (SELECT max(fobj.time) as last_update, fobj.hashcode FROM freeformobject fobj GROUP BY fobj.hashcode) fo ON f.hashcode=fo.hashcode
        UNION SELECT * FROM (SELECT ti.hashcode as hash_pane, 'Timeline' as type, ti.mail,ti.headline, ti.subheadline, ti.no_pass_hash, ti.background_image, ti.background_color, CASE WHEN (tp.last_update IS NULL) THEN ti.time else tp.last_update end as last_update FROM timelines ti LEFT JOIN (SELECT max(tpost.time) as last_update, tpost.hashcode FROM timelineposts tpost GROUP BY tpost.hashcode) tp ON ti.hashcode=tp.hashcode) as ti) as t
            LEFT JOIN admin_panel a on t.mail = a.mail;
    `);

    await knex.schema.raw(`
    CREATE OR REPLACE FUNCTION insert_update_delete_freeformobject()
    RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
            DELETE FROM freeformobject where hashcode=new.freeform_id and id = new.id;
            INSERT INTO freeformobject (hashcode, id, title, "left", top, comment_length, shape, color, embedded_length, body, info, height, width, visible) VALUES (new.freeform_id, new.id, new.title, new.left, new.top, new.comment_length, new.shape, new.color, new.embedded_length, new.body, new.info, new.height, new.width, new.visible);
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformobject';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
                RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_freeformembedded WHERE freeform_id = old.freeform_id AND parent_id = old.id;
        DELETE FROM view_line WHERE freeform_id = old.freeform_id and start = old.id;
        DELETE FROM view_line WHERE freeform_id = old.freeform_id and "end" = old.id;
        DELETE FROM freeformobject where hashcode = old.freeform_id AND id = old.id;
        END IF;
        RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_freeformembedded()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_freeforms where hashcode = new.freeform_id) > 0) THEN
            DELETE FROM freeformembedded where hashcode=new.freeform_id and id = new.id;
            INSERT INTO freeformembedded (hashcode, parent_id, id, body, url, type, likes, shape, color, info, visible) VALUES (new.freeform_id, new.parent_id, new.id, new.body, new.url, new.type, new.likes, new.shape, new.color, new.info, new.visible);
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für Freeformembedded';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM freeformembedded where hashcode = old.freeform_id AND id = old.id AND parent_id = old.parent_id;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';    
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_line()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO lines (id, hashcode, start, "end", text, size, dash, color) VALUES (new.id, new.freeform_id, new.start, new."end", new.line_text, new.size, new.dash, new.color);
        ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
            DELETE FROM lines WHERE hashcode = old.freeform_id and id = old.id;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);
      
  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_freeforms()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO freeforms(hashcode, password_pane, password_admin, arrow, line_style, line_color, line_size, like_symbol, background_color, shape, shape_color, comment_color, circle, embeddeds, line_text, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, font) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.arrow, new.line_style, new.line_color, new.line_size, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.circle, new.embeddeds, new.line_text, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE freeforms SET password_pane=new.password_pane, password_admin = new.password_admin, arrow = new.arrow, line_style = new.line_style, line_color = new.line_color, line_size = new.line_size, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, circle = new.circle, embeddeds = new.embeddeds, line_text = new.line_text, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash = new.no_pass_hash, font = new.font
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM view_freeformembedded WHERE freeform_id = old.hashcode;
        DELETE FROM view_freeformobject where freeform_id = old.hashcode;
        DELETE FROM view_line WHERE freeform_id = old.hashcode;
        DELETE FROM freeforms where hashcode = old.hashcode;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
    `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_scheduleobject()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
            DELETE FROM scheduleobjects where hashcode=new.schedule_id and id = new.id;
            INSERT INTO scheduleobjects (hashcode, id, title, comments, color, shape, order_id, visible) VALUES
            (new.schedule_id, new.id, new.title, new.comments, new.color, new.shape, new.order_id, new.visible);
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleobject';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
                RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_scheduleposts where parent_id = old.id AND schedule_id = old.schedule_id;
        DELETE FROM scheduleobjects where hashcode = old.schedule_id AND id = old.id;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_schedulecomments()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
            DELETE FROM schedulecomments WHERE hashcode=new.schedule_id and id = new.id;
            INSERT INTO schedulecomments (id, parent_id, hashcode, message, likes, shape, color, info, body, type, visible) VALUES (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.shape, new.color, new.info, new.body, new.type, new.visible) ;
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für schedulecomments';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM schedulecomments where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
        END IF;
    RETURN NEW;

    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_scheduleposts()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
        IF ((SELECT COUNT(*) FROM view_schedules where hashcode = new.schedule_id) > 0) THEN
            DELETE FROM scheduleposts where hashcode=new.schedule_id and id = new.id;
            INSERT INTO scheduleposts (id, parent_id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, visible) VALUES
                                    (new.id, new.parent_id, new.schedule_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible) ;
        ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für scheduleposts';
        END IF;
        ELSEIF (TG_OP = 'UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM schedulecomments where hashcode = old.schedule_id AND parent_id = id;
        DELETE FROM scheduleposts where hashcode = old.schedule_id AND id = old.id AND parent_id = old.parent_id;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_schedules()
  RETURNS trigger AS
    $$
    DECLARE
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO schedules(hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, column_width, embeddeds, one_column,likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash, add_position,font) VALUES
            (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.column_width, new.embeddeds, new.one_column, new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.add_position, new.font);
        ELSEIF (TG_OP = 'UPDATE') THEN
            UPDATE schedules SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                                shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, one_column = new.one_column, embeddeds = new.embeddeds, column_width = new.column_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, add_position = new.add_position, font = new.font
            WHERE hashcode= old.hashcode;
        ELSEIF (TG_OP = 'DELETE') THEN
        DELETE FROM view_password_reset where hashcode = old.hashcode;
        DELETE FROM view_share where pane_hash = old.hashcode;
        DELETE FROM view_scheduleposts WHERE schedule_id = old.hashcode;
        DELETE FROM view_schedulecomments WHERE schedule_id = old.hashcode;
        DELETE FROM view_scheduleobjects where schedule_id = old.hashcode;
        DELETE FROM schedules where hashcode = old.hashcode;
        END IF;
    RETURN NEW;
    END;
    $$
    LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_password_reset() RETURNS trigger
  LANGUAGE plpgsql
  AS $$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO passwort_zuruecksetzen (hashcode,abgelaufen,code) VALUES (new.hashcode, new.expire, new.code);
        ELSEIF(tg_op='UPDATE') THEN
            IF (old.boardname = 'Schedule') THEN
            IF (NEW.password_admin IS NOT NULL) THEN
                UPDATE schedules SET password_admin = new.password_admin WHERE hashcode=old.hashcode;
            END IF;
            IF (NEW.password_pane IS NOT NULL) THEN
                UPDATE schedules SET password_pane = new.password_pane WHERE hashcode=old.hashcode;
                END IF;
            ELSEIF (old.boardname = 'Freeform') THEN
            IF (NEW.password_admin IS NOT NULL) THEN
                UPDATE freeforms SET password_admin = new.password_admin WHERE hashcode=old.hashcode;
            END IF;
            IF (NEW.password_pane IS NOT NULL) THEN
                UPDATE freeforms SET password_pane = new.password_pane WHERE hashcode=old.hashcode;
                END IF;
            ELSEIF (old.boardname = 'Timeline') THEN
            IF (NEW.password_admin IS NOT NULL) THEN
                UPDATE timelines SET password_admin = new.password_admin WHERE hashcode=old.hashcode;
            END IF;
            IF (NEW.password_pane IS NOT NULL) THEN
                UPDATE timelines SET password_pane = new.password_pane WHERE hashcode=old.hashcode;
                END IF;
            end if;
        ELSEIF(tg_op='DELETE') THEN
            DELETE FROM passwort_zuruecksetzen WHERE hashcode = old.hashcode AND abgelaufen = old.expire;
        END IF;
    RETURN NEW;
    END;
    $$;
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_share() RETURNS trigger
  LANGUAGE plpgsql
  AS $$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            DELETE FROM freigabe where pane_hash  = new.pane_hash;
            INSERT INTO freigabe (pane_hash ,ende, prev_hash, likes, comments) VALUES (new.pane_hash, new.ende, new.prev_hash, new.likes, new.comments);
        ELSEIF(tg_op='UPDATE') THEN
            RAISE EXCEPTION 'Update nicht erlaubt!';
        ELSEIF(tg_op='DELETE') THEN
            DELETE FROM freigabe where pane_hash  = old.pane_hash;
        END IF;
    RETURN NEW;
    END;
    $$;
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_adminpanel() RETURNS trigger AS $$
  DECLARE
  BEGIN
          IF (TG_OP = 'INSERT') THEN
              IF ((SELECT count(*) FROM admin_panel a WHERE a.mail = new.mail)<1)
              THEN INSERT INTO admin_panel (mail, dashboard, password, dnr) VALUES (new.mail, new.dashboard, new.password, new.dnr);
              ELSE 
              UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
              END IF;
          ELSEIF(tg_op='UPDATE') THEN
          UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
          ELSEIF(tg_op='DELETE') THEN
              DELETE FROM view_account_password_reset where mail = old.mail;
              DELETE FROM view_freeforms where mail = old.mail;
              DELETE FROM view_schedules where mail = old.mail;
              DELETE FROM admin_panel a WHERE a.mail = old.mail;
         END IF;
      RETURN NEW;
  END;
  $$
  LANGUAGE 'plpgsql';
  `);

  await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_account_password_reset() RETURNS trigger
  LANGUAGE plpgsql
  AS $$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO passwort_zuruecksetzen (mail,abgelaufen,code) VALUES (new.mail, new.expire, new.code);
        ELSEIF(tg_op='UPDATE') THEN
            IF (NEW.password IS NOT NULL) THEN
                UPDATE admin_panel SET password = new.password WHERE mail = old.mail;
                END IF;
        ELSEIF(tg_op='DELETE') THEN
            DELETE FROM passwort_zuruecksetzen WHERE code = old.code;
        END IF;
    RETURN NEW;

    END;
    $$;
`);

await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelines()
RETURNS trigger AS
    $$
    DECLARE
    BEGIN
    IF (TG_OP = 'INSERT') THEN
        INSERT INTO timelines (hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash,font, line_color, line_size) VALUES
        (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.element_width, new.embeddeds,new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font, new.line_color, new.line_size);
    ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE timelines SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                            shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, embeddeds = new.embeddeds, element_width = new.element_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, font = new.font, line_color=new.line_color, line_size = new.line_size
        WHERE hashcode= old.hashcode;
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM view_password_reset where hashcode = old.hashcode;
    DELETE FROM view_share where pane_hash = old.hashcode;
    DELETE FROM view_timelineposts WHERE timeline_id = old.hashcode;
    DELETE FROM view_timelinecomments WHERE timeline_id = old.hashcode;
    DELETE FROM timelines where hashcode = old.hashcode;
    END IF;
    RETURN NEW;
    END;
    $$
LANGUAGE 'plpgsql';
`);

await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelinecomments()
RETURNS trigger AS
    $$
    DECLARE

    BEGIN
    IF (TG_OP = 'INSERT') THEN
    IF ((SELECT COUNT(*) FROM view_timelines where hashcode = new.timeline_id) > 0) THEN
        DELETE FROM timelinecomments WHERE hashcode=new.timeline_id and id = new.id;
        INSERT INTO timelinecomments (id, parent_id, hashcode, message, likes, shape, color, info, body, type, sichtbar) VALUES (new.id, new.parent_id, new.timeline_id, new.message, new.likes, new.shape, new.color, new.info, new.body, new.type, new.visible) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für timelinecomments';
    END IF;
    ELSEIF (TG_OP = 'UPDATE') THEN
        RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM timelinecomments where hashcode = old.timeline_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
    RETURN NEW;

    END;
    $$
LANGUAGE 'plpgsql';
`);

await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_timelineposts()
RETURNS trigger AS
    $$
    DECLARE
    BEGIN
    IF (TG_OP = 'INSERT') THEN
    IF ((SELECT COUNT(*) FROM view_timelines where hashcode = new.timeline_id) > 0) THEN
        DELETE FROM timelineposts where hashcode=new.timeline_id and id = new.id;
        INSERT INTO timelineposts (id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, sichtbar) VALUES
                                (new.id, new.timeline_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für timelineposts';
    END IF;
    ELSEIF (TG_OP = 'UPDATE') THEN
        RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM timelinecomments where hashcode = old.timeline_id;
    DELETE FROM timelineposts where hashcode = old.timeline_id AND id = old.id;
    END IF;
    RETURN NEW;
    END;
    $$
LANGUAGE 'plpgsql';
`);

await knex.schema.raw(`
CREATE OR REPLACE FUNCTION insert_update_delete_adminpanel() RETURNS trigger AS $$
DECLARE
BEGIN
        IF (TG_OP = 'INSERT') THEN
            IF ((SELECT count(*) FROM admin_panel a WHERE a.mail = new.mail)<1)
            THEN INSERT INTO admin_panel (mail, dashboard, password, dnr) VALUES (new.mail, new.dashboard, new.password, new.dnr);
            ELSE 
            UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
            END IF;
        ELSEIF(tg_op='UPDATE') THEN
        UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
        ELSEIF(tg_op='DELETE') THEN
            DELETE FROM view_account_password_reset where mail = old.mail;
            DELETE FROM view_freeforms where mail = old.mail;
            DELETE FROM view_schedules where mail = old.mail;
            DELETE FROM view_timelines where mail = old.mail;
            DELETE FROM admin_panel a WHERE a.mail = old.mail;
       END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';
`);
  
await knex.schema.raw('DROP TRIGGER IF EXISTS view_scheduleposts_trigger ON view_scheduleposts');

  await knex.schema.raw(`CREATE TRIGGER view_scheduleposts_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_scheduleposts
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_scheduleposts();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_schedulecomments_trigger ON view_schedulecomments');
  await knex.schema.raw(`CREATE TRIGGER view_schedulecomments_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_schedulecomments
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_schedulecomments();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_scheduleobject_trigger ON view_scheduleobjects');
  await knex.schema.raw(`CREATE TRIGGER view_scheduleobject_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_scheduleobjects
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_scheduleobject();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_freeforms_trigger ON view_freeforms');
  await knex.schema.raw(`CREATE TRIGGER view_freeforms_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeforms
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeforms();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_line_trigger ON view_line');
  await knex.schema.raw(`CREATE TRIGGER view_line_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_line
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_line();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_freeformembedded_trigger ON view_freeformembedded');
  await knex.schema.raw(`CREATE TRIGGER view_freeformembedded_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeformembedded
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeformembedded();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_freeformobject_trigger ON view_freeformobject');
  await knex.schema.raw(`CREATE TRIGGER view_freeformobject_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_freeformobject
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_freeformobject();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS account_passwort_zuruecksetzen_trigger ON view_account_password_reset');
  await knex.schema.raw(`CREATE TRIGGER account_passwort_zuruecksetzen_trigger
  INSTEAD OF INSERT OR DELETE OR UPDATE ON view_account_password_reset
  FOR EACH ROW EXECUTE FUNCTION insert_update_delete_account_password_reset();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS admin_panel_trigger ON view_adminpanel');
  await knex.schema.raw(`CREATE TRIGGER admin_panel_trigger
  INSTEAD OF INSERT OR DELETE OR UPDATE ON view_adminpanel
  FOR EACH ROW EXECUTE FUNCTION insert_update_delete_adminpanel();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_schedules_trigger ON view_schedules');
  await knex.schema.raw(`CREATE TRIGGER view_schedules_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_schedules
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_schedules();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS freigabe_trigger ON view_share');
  await knex.schema.raw(`CREATE TRIGGER freigabe_trigger 
  INSTEAD OF INSERT OR DELETE OR UPDATE ON view_share 
  FOR EACH ROW EXECUTE FUNCTION insert_update_delete_share();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS passwort_zuruecksetzen_trigger ON view_password_reset');
  await knex.schema.raw(`CREATE TRIGGER passwort_zuruecksetzen_trigger
  INSTEAD OF INSERT OR DELETE OR UPDATE ON view_password_reset
  FOR EACH ROW EXECUTE FUNCTION insert_update_delete_password_reset();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_timelines_trigger ON view_timelines');
  await knex.schema.raw(`CREATE TRIGGER view_timelines_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelines
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelines();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_timelinescomments_trigger ON view_timelinecomments');
  await knex.schema.raw(`CREATE TRIGGER view_timelinescomments_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelinecomments
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelinecomments();
  `);

  await knex.schema.raw('DROP TRIGGER IF EXISTS view_timelinesposts_trigger ON view_timelineposts');
  await knex.schema.raw(`CREATE TRIGGER view_timelinesposts_trigger
  INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelineposts
  FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelineposts();
  `);

  await knex.schema.raw(`
      CREATE OR REPLACE VIEW pilot_oberflaechen AS
      SELECT t.anzahl_oberflaechen,
        (SELECT count(*) AS count
          FROM (SELECT view_freeforms.mail
                FROM view_freeforms
                UNION
                SELECT view_schedules.mail
                FROM view_schedules
                UNION
                SELECT view_timelines.mail
                FROM view_timelines) a) AS mails,
        (SELECT count(DISTINCT regexp_replace(t_1.dnr::text, '^\s+'::text, ''::text)) AS count
          FROM (SELECT view_schedules.dnr
                FROM view_schedules
                UNION
                SELECT view_freeforms.dnr
                FROM view_freeforms
                UNION
                SELECT view_timelines.dnr
                FROM view_timelines) t_1) AS schulen,
        (SELECT max(t_1.user_count) AS max
          FROM (SELECT dashboard.user_count
                FROM dashboard) t_1) AS max_user_overall,
        (SELECT max(dashboard.user_count) AS max
          FROM dashboard
          WHERE dashboard."timestamp"::date >= CURRENT_DATE AND dashboard."timestamp"::date < (CURRENT_DATE + '1 day'::interval)) AS max_user_day
      FROM (SELECT count(*) AS anzahl_oberflaechen
            FROM (SELECT view_schedules.hashcode
                  FROM view_schedules
                  UNION
                  SELECT view_freeforms.hashcode
                  FROM view_freeforms
                  UNION
                  SELECT view_timelines.hashcode
                  FROM view_timelines) b) t;
    `);
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};
