-- Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
--
-- SPDX-License-Identifier: AGPL-3.0-or-later
CREATE TABLE timelines (
	hashcode varchar,
	password_pane varchar,
	password_admin varchar,
	like_symbol varchar ,
	background_color varchar,
	shape varchar,
	shape_color varchar,
	comment_color varchar,
	element_width varchar,
	embeddeds BOOLEAN,
	likes BOOLEAN,
	font_size varchar ,
	comments BOOLEAN,
	count integer,
	unlocked varchar,
	headline varchar,
	subheadline varchar,
	background_image varchar,
	public_key varchar,
	private_key varchar,
	iv varchar,
	mail varchar,
	dnr varchar,
	no_pass_hash varchar,
	time varchar DEFAULT current_timestamp,
	font varchar DEFAULT 'default',
	line_color varchar DEFAULT '#666666',
	line_size integer DEFAULT 2,
	PRIMARY KEY(hashcode)
);


CREATE OR REPLACE VIEW view_timelines AS
	SELECT hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr, no_pass_hash, font, line_color, line_size FROM timelines s;
	

CREATE OR REPLACE FUNCTION insert_update_delete_timelines()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
        INSERT INTO timelines (hashcode, password_pane, password_admin, like_symbol, background_color, shape, shape_color, comment_color, element_width, embeddeds, likes, font_size, comments, count, unlocked, headline, subheadline, background_image, public_key, private_key, iv, mail, dnr , no_pass_hash,font, line_color, line_size) VALUES
	    (new.hashcode, new.password_pane, new.password_admin, new.like_symbol, new.background_color, new.shape, new.shape_color, new.comment_color, new.element_width, new.embeddeds,new.likes, new.font_size, new.comments, new.count, new.unlocked, new.headline, new.subheadline, new.background_image, new.public_key, new.private_key, new.iv, new.mail, new.dnr, new.no_pass_hash, new.font, new.line_color, new.line_size);
	ELSEIF (TG_OP = 'UPDATE') THEN
        UPDATE timelines SET password_pane=new.password_pane, password_admin = new.password_admin, like_symbol = new.like_symbol, background_color = new.background_color,
                             shape_color = new.shape_color, shape = new.shape, comment_color = new.comment_color, embeddeds = new.embeddeds, element_width = new.element_width, likes = new.likes, font_size = new.font_size, comments = new.comments, count = new.count, unlocked = new.unlocked, headline = new.headline, subheadline = new.subheadline, background_image = new.background_image, public_key = new.public_key, private_key = new.private_key, dnr = new.dnr, iv = new.iv, mail = new.mail, no_pass_hash=new.no_pass_hash, font = new.font, line_color=new.line_color, line_size = new.line_size
	    WHERE hashcode= old.hashcode;
    ELSEIF (TG_OP = 'DELETE') THEN
	DELETE FROM view_password_reset where hashcode = old.hashcode;
	DELETE FROM view_share where pane_hash = old.hashcode;
    DELETE FROM view_timelineposts WHERE timeline_id = old.hashcode;
    DELETE FROM view_timelinecomments WHERE timeline_id = old.hashcode;
    DELETE FROM timelines where hashcode = old.hashcode;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';



CREATE TRIGGER view_timelines_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelines
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelines();
	





CREATE TABLE timelinecomments(
	hashcode varchar,
	parent_id varchar,
	id varchar,
	message varchar,
	likes integer,
	shape varchar,
	color varchar,
	body varchar,
	info varchar,
	type varchar,
	time varchar DEFAULT current_timestamp,
	sichtbar boolean DEFAULT true,
	PRIMARY KEY(hashcode, parent_id, id)
);
CREATE TABLE timelineposts(
	hashcode varchar,
	id varchar,
	message varchar,
	body varchar,
	type varchar,
	likes integer,
	shape varchar,
	color varchar,
	info varchar,
	comment_length integer,
	order_id varchar,
	time varchar DEFAULT current_timestamp,
	sichtbar boolean DEFAULT true,
	PRIMARY KEY(hashcode, id)
);




CREATE OR REPLACE VIEW view_timelinecomments AS
    SELECT id, parent_id, hashcode as timeline_id, message, likes, shape, color, body, info, type, sichtbar as visible FROM timelinecomments;

CREATE OR REPLACE VIEW view_timelineposts AS
    SELECT id, hashcode as timeline_id, message, likes, type, body, color, shape, comment_length, info, order_id, sichtbar as visible FROM timelineposts;
	
	
	
CREATE TRIGGER view_timelinescomments_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelinecomments
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelinecomments();

CREATE TRIGGER view_timelinesposts_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_timelineposts
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_timelineposts();
	

CREATE OR REPLACE FUNCTION insert_update_delete_timelinecomments()
  RETURNS trigger AS
$$
DECLARE

BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_timelines where hashcode = new.timeline_id) > 0) THEN
		DELETE FROM timelinecomments WHERE hashcode=new.timeline_id and id = new.id;
        INSERT INTO timelinecomments (id, parent_id, hashcode, message, likes, shape, color, info, body, type, sichtbar) VALUES (new.id, new.parent_id, new.timeline_id, new.message, new.likes, new.shape, new.color, new.info, new.body, new.type, new.visible) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für timelinecomments';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM timelinecomments where hashcode = old.timeline_id AND id = old.id AND parent_id = old.parent_id;
    END IF;
  RETURN NEW;

END;
$$
LANGUAGE 'plpgsql';

	
CREATE OR REPLACE FUNCTION insert_update_delete_timelineposts()
  RETURNS trigger AS
$$
DECLARE
BEGIN
	IF (TG_OP = 'INSERT') THEN
	IF ((SELECT COUNT(*) FROM view_timelines where hashcode = new.timeline_id) > 0) THEN
		DELETE FROM timelineposts where hashcode=new.timeline_id and id = new.id;
        INSERT INTO timelineposts (id, hashcode, message, likes, type, body, color, shape, comment_length, info, order_id, sichtbar) VALUES
                                (new.id, new.timeline_id, new.message, new.likes, new.type, new.body, new.color, new.shape, new.comment_length, new.info, new.order_id, new.visible) ;
    ELSE RAISE EXCEPTION 'Keine Oberfläche gefunden für timelineposts';
    END IF;
	ELSEIF (TG_OP = 'UPDATE') THEN
		RAISE EXCEPTION 'Update nicht erlaubt!';
    ELSEIF (TG_OP = 'DELETE') THEN
    DELETE FROM timelinecomments where hashcode = old.timeline_id;
    DELETE FROM timelineposts where hashcode = old.timeline_id AND id = old.id;
    END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';
	
	
	
	
CREATE OR REPLACE VIEW view_password_reset AS
	SELECT t.hashcode, t.mail, t.boardname, pr.abgelaufen as expire, pr.code,password_admin, password_pane FROM (SELECT hashcode, mail, 'Schedule' as boardname, password_admin, password_pane from schedules
	UNION
	SELECT hashcode, mail, 'Freeform' as boardname, password_admin, password_pane from freeforms
	UNION SELECT hashcode, mail, 'Timeline' as boardname, password_admin, password_pane from timelines) t
	LEFT  JOIN passwort_zuruecksetzen pr ON t.hashcode = pr.hashcode;
	
	
CREATE OR REPLACE VIEW view_share AS
	SELECT f.pane_hash, f.ende, f.prev_hash, f.likes, f.comments,
	CASE WHEN pane_hash IN (SELECT hashcode FROM schedules) AND ((SELECT one_column from schedules where hashcode=pane_hash) = 'true') THEN 'Stream' WHEN pane_hash IN (SELECT hashcode FROM schedules) THEN 'Schedule' WHEN pane_hash IN (SELECT hashcode FROM timelines) THEN 'Timeline' ELSE 'Freeform' END as typname
	FROM freigabe f;
	
CREATE OR REPLACE VIEW view_all_boards AS
	SELECT t.hash_pane, a.dashboard as hash_dash, t.type, t.mail, t.headline, t.subheadline, t.no_pass_hash, t.background_image, t.background_color, t.last_update FROM (SELECT * FROM (SELECT s.hashcode as hash_pane, CASE WHEN (one_column IS TRUE) THEN 'Stream' ELSE 'Schedule' END as type, s.mail,s.headline, s.subheadline, s.no_pass_hash, s.background_image, s.background_color, CASE WHEN (sp.last_update IS NULL) THEN s.time else sp.last_update end as last_update FROM schedules s LEFT JOIN (SELECT max(spost.time) as last_update, spost.hashcode FROM scheduleposts spost GROUP BY spost.hashcode) sp ON s.hashcode=sp.hashcode) as s
     UNION SELECT f.hashcode as hash_pane, 'Freeform' as type, f.mail,f.headline, f.subheadline, f.no_pass_hash, f.background_image, f.background_color, CASE WHEN (fo.last_update IS NULL) THEN f.time else fo.last_update end as last_update FROM freeforms f LEFT JOIN (SELECT max(fobj.time) as last_update, fobj.hashcode FROM freeformobject fobj GROUP BY fobj.hashcode) fo ON f.hashcode=fo.hashcode
	 UNION SELECT * FROM (SELECT ti.hashcode as hash_pane, 'Timeline' as type, ti.mail,ti.headline, ti.subheadline, ti.no_pass_hash, ti.background_image, ti.background_color, CASE WHEN (tp.last_update IS NULL) THEN ti.time else tp.last_update end as last_update FROM timelines ti LEFT JOIN (SELECT max(tpost.time) as last_update, tpost.hashcode FROM timelineposts tpost GROUP BY tpost.hashcode) tp ON ti.hashcode=tp.hashcode) as ti) as t
         LEFT JOIN admin_panel a on t.mail = a.mail;
		 
		 

CREATE OR REPLACE FUNCTION insert_update_delete_password_reset() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
        IF (TG_OP = 'INSERT') THEN
            INSERT INTO passwort_zuruecksetzen (hashcode,abgelaufen,code) VALUES (new.hashcode, new.expire, new.code);
        ELSEIF(tg_op='UPDATE') THEN
           IF (old.boardname = 'Schedule') THEN
			IF (NEW.password_admin IS NOT NULL) THEN
               UPDATE schedules SET password_admin = new.password_admin WHERE hashcode=old.hashcode;
			END IF;
			IF (NEW.password_pane IS NOT NULL) THEN
               UPDATE schedules SET password_pane = new.password_pane WHERE hashcode=old.hashcode;
           	END IF;
		   ELSEIF (old.boardname = 'Freeform') THEN
			IF (NEW.password_admin IS NOT NULL) THEN
                UPDATE freeforms SET password_admin = new.password_admin WHERE hashcode=old.hashcode;
			END IF;
			IF (NEW.password_pane IS NOT NULL) THEN
                UPDATE freeforms SET password_pane = new.password_pane WHERE hashcode=old.hashcode;
           	END IF;
			ELSEIF (old.boardname = 'Timeline') THEN
			IF (NEW.password_admin IS NOT NULL) THEN
                UPDATE timelines SET password_admin = new.password_admin WHERE hashcode=old.hashcode;
			END IF;
			IF (NEW.password_pane IS NOT NULL) THEN
                UPDATE timelines SET password_pane = new.password_pane WHERE hashcode=old.hashcode;
           	END IF;
           end if;
        ELSEIF(tg_op='DELETE') THEN
			DELETE FROM passwort_zuruecksetzen WHERE hashcode = old.hashcode AND abgelaufen = old.expire;
       END IF;
    RETURN NEW;
END;
$$;



CREATE OR REPLACE FUNCTION insert_update_delete_adminpanel() RETURNS trigger AS $$
DECLARE
BEGIN
        IF (TG_OP = 'INSERT') THEN
            IF ((SELECT count(*) FROM admin_panel a WHERE a.mail = new.mail)<1)
			THEN INSERT INTO admin_panel (mail, dashboard, password, dnr) VALUES (new.mail, new.dashboard, new.password, new.dnr);
			ELSE 
			UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
			END IF;
		ELSEIF(tg_op='UPDATE') THEN
		UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
        ELSEIF(tg_op='DELETE') THEN
			DELETE FROM view_account_password_reset where mail = old.mail;
			DELETE FROM view_freeforms where mail = old.mail;
			DELETE FROM view_schedules where mail = old.mail;
			DELETE FROM view_timelines where mail = old.mail;
			DELETE FROM admin_panel a WHERE a.mail = old.mail;
       END IF;
    RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';