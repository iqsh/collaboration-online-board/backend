// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {

    columnExists = await knex.schema.hasColumn('admin_panel', 'ai');
    if (!columnExists) {
        await knex.schema.table('admin_panel', function (table) {
            table.boolean('ai').defaultTo(false);
        });
    }

    await knex.schema.createViewOrReplace('view_adminpanel', function (view) {
        view.as(knex.select('mail', 'dashboard', 'password', 'dnr', 'ai')
            .from('admin_panel'));
    });


    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_adminpanel() RETURNS trigger AS $$
        DECLARE
        BEGIN
            IF (TG_OP = 'INSERT') THEN
                IF ((SELECT count(*) FROM admin_panel a WHERE a.mail = new.mail)<1)
                THEN INSERT INTO admin_panel (mail, dashboard, password, dnr, ai) VALUES (new.mail, new.dashboard, new.password, new.dnr, new.ai);
                ELSE 
                    UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr, ai = new.ai WHERE mail = new.mail;
                END IF;
            ELSEIF(tg_op='UPDATE') THEN
                UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr, ai = new.ai WHERE mail = new.mail;
            ELSEIF(tg_op='DELETE') THEN
                DELETE FROM view_account_password_reset where mail = old.mail;
                DELETE FROM view_freeforms where mail = old.mail;
                DELETE FROM view_schedules where mail = old.mail;
                DELETE FROM admin_panel a WHERE a.mail = old.mail;
            END IF;
            RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';
    `);

    
    let tableExists = await knex.schema.hasTable('ai_user');
    if(!tableExists){
        await knex.schema.createTable('ai_user', function(table) {
            table.string('mail', 255);
            table.integer('token_used').notNullable().defaultTo(0);
            table.primary(['mail']);
        });
    }
    
    tableExists = await knex.schema.hasTable('ai_person');
    if(!tableExists){
        await knex.schema.createTable('ai_person', function(table) {
            table.uuid('uuid').defaultTo(knex.raw('gen_random_uuid()'));
            table.string('name',255);
            table.string('creator', 255);
            table.text('system_prompt');
            table.string('avatar', 255);
            table.boolean('shared').defaultTo(false);
            table.string('user_mail', 255);
            table.string('model');
            table.text('description');
            table.string('type');
            table.json('meta');            
            table.string('create_date', 255).defaultTo(knex.raw('CURRENT_DATE::varchar'));
            table.primary(['uuid']);
        });
    }


    tableExists = await knex.schema.hasTable('ai_chat');
    if (!tableExists) {
        await knex.schema.createTable('ai_chat', function (table) {
            table.uuid('uuid').defaultTo(knex.raw('gen_random_uuid()'));
            table.string('name', 255);
            table.uuid('person_uuid').references('uuid').inTable('ai_person');
            table.string('create_date', 255).defaultTo(knex.raw('now()::varchar'));
            table.string('model', 255);
            table.string('user_mail', 255);
            table.string('type', 255);
            table.text('system_prompt');
            table.json('meta');
            table.primary(['uuid']);
        });
    }

    tableExists = await knex.schema.hasTable('ai_chat_shared');
    if (!tableExists) {
        await knex.schema.createTable('ai_chat_shared', function (table) {
            table.uuid('chat_uuid').references('uuid').inTable('ai_chat');
            table.string('valid_until', 255);
            table.primary(['chat_uuid']);
        });
    }

    tableExists = await knex.schema.hasTable('ai_chat_message');
    if (!tableExists) {
        await knex.schema.createTable('ai_chat_message', function (table) {
            table.uuid('chat_uuid').references('uuid').inTable('ai_chat');
            table.uuid('uuid').defaultTo(knex.raw('gen_random_uuid()'));
            table.string('create_date', 255);
            table.string('creator', 255);
            table.string('token', 50);
            table.text('message');
            table.json('meta');
            table.string('user', 255);
            table.primary(['uuid']);
        });
    }

    tableExists = await knex.schema.hasTable('ai_chat_message_token_usage');
    if (!tableExists) {
        await knex.schema.createTable('ai_chat_message_token_usage', function (table) {
            table.uuid('uuid').defaultTo(knex.raw('gen_random_uuid()'));
            table.string('mail', 255);
            table.string('token', 50);
            table.string('create_date', 255);
            table.primary(['uuid']);
        });
    }

    tableExists = await knex.schema.hasTable('ai_chat_message_session');
    if (!tableExists) {
        await knex.schema.createTable('ai_chat_message_session', function (table) {
            table.uuid('chat_uuid').references('uuid').inTable('ai_chat');
            table.uuid('uuid').defaultTo(knex.raw('gen_random_uuid()'));
            table.text('message');
            table.json('meta');
            table.string('user', 255);
            table.primary(['uuid']);
        });
    }

    tableExists = await knex.schema.hasTable('ai_person_category');
    if (!tableExists) {
        await knex.schema.createTable('ai_person_category', function (table) {
            table.uuid('person_uuid').references('uuid').inTable('ai_person');
            table.string('category', 255);
            table.primary(['person_uuid','category']);
        });
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {
    await knex.schema.raw('DROP TRIGGER IF EXISTS view_adminpanel_trigger ON view_adminpanel');
    await knex.schema.raw(`CREATE OR REPLACE FUNCTION insert_update_delete_adminpanel() RETURNS trigger AS $$
        DECLARE
        BEGIN
            IF (TG_OP = 'INSERT') THEN
                IF ((SELECT count(*) FROM admin_panel a WHERE a.mail = new.mail)<1)
                THEN INSERT INTO admin_panel (mail, dashboard, password, dnr) VALUES (new.mail, new.dashboard, new.password, new.dnr);
                ELSE 
                    UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
                END IF;
            ELSEIF(tg_op='UPDATE') THEN
                UPDATE admin_panel SET dashboard = new.dashboard, password = new.password, dnr = new.dnr WHERE mail = new.mail;
            ELSEIF(tg_op='DELETE') THEN
                DELETE FROM view_account_password_reset where mail = old.mail;
                DELETE FROM view_freeforms where mail = old.mail;
                DELETE FROM view_schedules where mail = old.mail;
                DELETE FROM admin_panel a WHERE a.mail = old.mail;
            END IF;
            RETURN NEW;
        END;
        $$
        LANGUAGE 'plpgsql';
    `);
    await knex.schema.createViewOrReplace('view_adminpanel', function (view) {
        view.as(knex.select('mail', 'dashboard', 'password', 'dnr')
            .from('admin_panel'));
    });
    
    await knex.schema.alterTable('admin_panel', function (table) {
        table.dropColumn('ai');
    });
    await knex.schema.raw(`CREATE TRIGGER view_adminpanel_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON view_adminpanel
    FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_adminpanel();`);

    await knex.schema.dropTable('ai_chat_shared');
    await knex.schema.dropTable('ai_person_category');
    await knex.schema.dropTable('ai_chat_message_session');
    await knex.schema.dropTable('ai_chat_message');
    await knex.schema.dropTable('ai_user');
    await knex.schema.dropTable('ai_person');
    await knex.schema.dropTable('ai_chat');
};