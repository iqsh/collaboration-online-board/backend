// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    columnExists = await knex.schema.hasColumn('ai_chat', 'person_uuid');
    if (columnExists) {
        await knex.schema.table('ai_chat', function (table) {
            table.dropColumn('person_uuid');
        });
    }
    columnExists = await knex.schema.hasColumn('ai_chat', 'avatar');
    if (!columnExists) {
        await knex.schema.table('ai_chat', function (table) {
            table.string('avatar');
        });
    }
    columnExists = await knex.schema.hasColumn('ai_chat', 'description');
    if (!columnExists) {
        await knex.schema.table('ai_chat', function (table) {
            table.string('description');
        });
    }
    
    columnExists = await knex.schema.hasColumn('ai_chat', 'person_name');
    if (!columnExists) {
        await knex.schema.table('ai_chat', function (table) {
            table.string('person_name');
        });
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {    
    await knex.schema.alterTable('ai_chat', function (table) {
        table.dropColumn('avatar');
        table.dropColumn('description');
        table.dropColumn('person_name');
        table.uuid('person_uuid').references('uuid').inTable('ai_person');
    });
};