// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    columnExists = await knex.schema.hasColumn('ai_chat_shared', 'create_date');
    if (!columnExists) {
        await knex.schema.table('ai_chat_shared', function (table) {
            table.string('created_at').defaultTo(knex.fn.now());
        });
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {    
    await knex.schema.alterTable('ai_chat_shared', function (table) {
        table.dropColumn('created_at');
    });
};