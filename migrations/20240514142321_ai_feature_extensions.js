// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = async function (knex) {
    columnExists = await knex.schema.hasColumn('ai_chat_shared', 'limitation');
    if (!columnExists) {
        await knex.schema.table('ai_chat_shared', function (table) {
            table.integer('limitation').defaultTo(0);
        });
    }
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = async function (knex) {    
    await knex.schema.alterTable('ai_chat_shared', function (table) {
        table.dropColumn('limitation');
    });
};