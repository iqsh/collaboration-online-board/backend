// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import deleteRoute from './v1/delete.mjs';
import fileRoute from './v1/file.mjs';
import freeformRoute from './v1/freeform.mjs';
import passwordResetRoute from './v1/password_reset.mjs';
import scheduleRoute from './v1/schedule.mjs';
import statRoute from './v1/stats.mjs';
import unfurlRoute from './v1/unfurl.mjs';
import registrationRoute from './v1/registration.mjs';
import createRoute from './v1/create.mjs';
import overviewRoute from './v1/overview.mjs';
import boarddataRoute from './v1/boarddata.mjs';
import paneRoute from './v1/pane.mjs';
import accountRoute from './v1/account.mjs';
import timelineRoute from './v1/timeline.mjs';
import aiRoute from './v1/ai.mjs';

export default function(fastify, options, done){
    fastify.register(deleteRoute,{ prefix: 'delete' });
    fastify.register(fileRoute,{ prefix: 'file' });
    fastify.register(freeformRoute,{ prefix: 'freeform' });
    fastify.register(passwordResetRoute,{ prefix: 'password_reset' });
    fastify.register(scheduleRoute,{ prefix: 'schedule' });
    fastify.register(scheduleRoute,{ prefix: 'stream' });
    fastify.register(statRoute,{ prefix: 'stats' });
    fastify.register(unfurlRoute,{ prefix: 'unfurl' });
    fastify.register(registrationRoute,{ prefix: 'registration' });
    fastify.register(createRoute,{ prefix: 'create' });
    fastify.register(overviewRoute, {prefix: 'overview'});
    fastify.register(boarddataRoute, {prefix: 'boarddata'});
    fastify.register(paneRoute,{ prefix: 'pane' });
    fastify.register(accountRoute, { prefix: 'account'});
    fastify.register(timelineRoute, { prefix: 'timeline'});
    fastify.register(aiRoute, { prefix: 'ai'});
    done()
}