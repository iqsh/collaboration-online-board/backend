// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { ensureDir, mkdirs, copy } from "fs-extra";
import { transaction } from "objection";
import bcrypt from 'bcrypt';
import timeline from "../../database/models/timeline.mjs";
import Globals from "../../lib/globalvars.mjs";
import { updatePaneConfiguration, uploadBackground, checkValidDomain, createHashPane, createNoPasswordHash } from "../../lib/utils.mjs";
import { prepareRegistration as sendMailForDifferentUser } from "../../lib/registration.mjs";
import { copyDifferentUser, getCurrentShare, sharePane, unsharePane } from "../../lib/share.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import timelinePost from "../../database/models/timelinePost.mjs";
import timelineComment from "../../database/models/timelineComment.mjs";
import { addPaneToFavorites, removePaneFromFavorites } from "../../lib/favorites.mjs";

/**
 * Copies an existing pane
 * @param {*} params Parameters
 * @param {*} hashcode Hashcode of pane
 * @param {boolean} isSameUser If is for the same user or not
 * @returns success: true || false
 */
async function copyPane(params, hashcode, isSameUser, request) {
    try{
        let timelineData = await timeline.query().where('hashcode', hashcode).first();
        let token = request.headers.token;
        let session = null;
        if(token && isSameUser){
            session = await sessionStore.query().where('session_id',token).first();
        }
        if(timelineData && ((isSameUser && session && session.mail === timelineData.mail) || !isSameUser)){
            let userPassword = params.userPassword;
            let adminPassword = params.adminPassword;
            if (userPassword != null && userPassword != undefined && userPassword.length > 0) {
                const hashUser = await bcrypt.hash(userPassword, Globals.saltRounds);
                timelineData.password_pane = hashUser;
            }
            if (adminPassword != null && adminPassword != undefined && adminPassword.length > 0) {
                const hashAdmin = await bcrypt.hash(adminPassword, Globals.saltRounds);
                timelineData.password_admin = hashAdmin;
            }
            if(params.mail != null && params.mail.length > 0){
                const domain = checkValidDomain(request);
                timelineData.mail = params.mail.split('@')[0].toLowerCase() + domain;
            }
            let newHashcode = createHashPane();
            let newNoPassHash = createNoPasswordHash(newHashcode);
            timelineData.hashcode = newHashcode;
            timelineData.no_pass_hash = newNoPassHash;
            if (timelineData.background_image)
                timelineData.background_image = timelineData.background_image.replace('/api/file/' + hashcode, '/api/file/' + newHashcode);
            if(timelineData.element_width)
                timelineData.element_width = Number(timelineData.element_width);
            timelineData.headline = params.headline ? params.headline : timelineData.headline;
            let objects = await timelinePost.query().where('timeline_id', hashcode).where('authorized',true);
            if(objects){
                let comments = await timelineComment.query().where('timeline_id',hashcode);
                objects = objects.map(({ timeline_id, body, title , ...x }) => ({ ...x, timeline_id: newHashcode, body: body!==null && body!==undefined?body.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):body, title: title!==null&&title!==undefined?title.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):title}));
                comments = comments.map(({ timeline_id, body, url, likes, ...x }) => ({ ...x, timeline_id: newHashcode, body: body!==null && body!==undefined ? body.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):body, url: url!==null && url!==undefined ? url.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):url, likes: params.copyLikes?(likes?likes:0):0 }));
                await transaction(timelineComment, timelinePost, timeline,  async (timelineComment, timelinePost, timeline)=>{
                    await timeline.query().insert(timelineData);
                    if(objects.length>0)
                        await timelinePost.query().insert(objects);
                    if(comments.length>0)
                        await timelineComment.query().insert(comments);
                });
                let oldPath = 'uploads/' + hashcode;
                let newPath = 'uploads/' + newHashcode;
                await ensureDir(oldPath);
                await mkdirs(newPath);
                await copy(oldPath, newPath);
                if(isSameUser)
                    return { success: true, hashcode: newHashcode };                 
                else
                    return sendMailForDifferentUser(request,newHashcode,"timeline",false);
            }
        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}

export default function(fastify, options, done){
    fastify.post('/update/', async(request,reply)=>{
        let params = request.body;
        return updatePaneConfiguration(timeline, params)
    })

    fastify.post('/update/background/', async(request,reply)=>{
        let file = await request.body.file;
        const body = Object.fromEntries(
            Object.keys(request.body).map((key) => [key, request.body[key].value])
        );
        return uploadBackground(timeline, body, file);
    })

    fastify.post('/copy/', async(request,reply)=>{
        let params = request.body;
        return copyPane(params, params.hashcode, true, request)
    })

    fastify.post('/copy/different/', async(request,reply)=>{
        return copyDifferentUser(request, copyPane);
    })

    fastify.get('/share/:id', async(request,reply)=>{
        const id = request.params.id;
        return getCurrentShare(id, request);
    })

    fastify.post('/share/', async(request,reply)=>{
        return sharePane(request, 'Timeline', timeline);
    })

    fastify.delete('/share/', async(request,reply)=>{
        return unsharePane(request);
    })

    fastify.post('/favorite/:id', async(request,reply)=>{
        return addPaneToFavorites(request);
    })

    fastify.delete('/favorite/:id', async(request,reply)=>{
        return removePaneFromFavorites(request);
    })

    done()
}