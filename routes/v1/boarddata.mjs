// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import i18next from 'i18next';
import { readFile } from 'fs/promises';
import { compare } from 'bcrypt';
import { transaction } from "objection";
import allPanes from "../../database/models/allPanes.mjs";
import freeform from "../../database/models/freeform.mjs";
import freeformComment from "../../database/models/freeformComment.mjs";
import freeformObject from "../../database/models/freeformObject.mjs";
import freeformSize from "../../database/models/freeformSize.mjs";
import line from "../../database/models/line.mjs";
import share from "../../database/models/share.mjs";
import schedule from "../../database/models/schedule.mjs";
import schedulePost from '../../database/models/schedulePost.mjs';
import scheduleComment from '../../database/models/scheduleComment.mjs';
import scheduleObject from '../../database/models/scheduleObject.mjs';
import timeline from "../../database/models/timeline.mjs";
import timelinePost from '../../database/models/timelinePost.mjs';
import timelineComment from '../../database/models/timelineComment.mjs';
import Globals from "../../lib/globalvars.mjs";

import { appendFile } from 'fs';
import { join } from 'path';
const __dirname = new URL('.', import.meta.url).pathname;

function writeToLog (hashcode, type){
    if(Globals.debug){
        const date = new Date();
        let file = join(__dirname,"..","..", "logs",`${date.getFullYear() + "_" + String(date.getMonth()+1).padStart(2,'0') + "_" + String(date.getDate()).padStart(2,'0')}-log.txt`);
        let formatedDate = date.getFullYear() + "/" + String(date.getMonth()+1).padStart(2,'0') + "/" + String(date.getDate()).padStart(2,'0') + "-" + String(date.getHours()).padStart(2,'0') + ":" + String(date.getMinutes()).padStart(2,'0') + ":" + String(date.getSeconds()).padStart(2,'0');
        let log = formatedDate + ";" + type + ";" + hashcode + "\n";
        appendFile(file, log, (err) => {
            if (err) console.error(err);
        });
    }
}
const de = JSON.parse(
    await readFile(
      new URL('../../locales/de.json', import.meta.url)
    )
);

i18next.init({
    lng: 'de',
    resources: {
        de
    }
});

/**
 * Fetches the data from a schedule/stream pane
 * @param {*} id Hashcode of a pane
 * @param {*} previewOptions Options for preview
 * @returns success + data of pane
 */
async function fetchScheduleData(id, previewOptions = undefined){
    writeToLog(id, "open");
    try{
        let scheduleData = await schedule.query().where('hashcode',id).first();
        var scheduleStyle = {
            shape: scheduleData.shape,
            shapeColor: scheduleData.shape_color,
            commentColor: scheduleData.comment_color,
            backgroundColor: scheduleData.background_color,
            likeSymbol: scheduleData.like_symbol,
            embeddedsAllowed: scheduleData.embeddeds,
            commentsAllowed: scheduleData.comments,
            likesAllowed: scheduleData.likes,
            fontSize: scheduleData.font_size,
            columnWidth: scheduleData.column_width,
            oneColumn: scheduleData.one_column,
            backgroundImage: scheduleData.background_image,
            font: scheduleData.font,
            columnAddPosition: scheduleData.add_position,
            authorize: scheduleData.authorize
        };
        if(scheduleData){
            let objects = await scheduleObject.query().where('schedule_id',id);
            if(objects && objects.length>0){
                let posts = await schedulePost.query().where('schedule_id',id);
                let comments = await scheduleComment.query().where('schedule_id',id);
                comments = comments?comments.map(({parent_id, schedule_id, message, body, ...x})=>({...x, parent_id, text:message!=='"null"'? message:null, body:JSON.parse(body)})):[];
                posts = posts?posts.map(({schedule_id, parent_id, message, comment_length, body, ...x})=> ({...x, parent_id, commentLength: comment_length, title: message!=='"null"'? JSON.parse(message):null, body: body!=='"null"'?JSON.parse(body):null,comments: comments.filter(c=> c.parent_id === x.id)})):[];
                let scheduleDataSet = objects.map(({comments, schedule_id, ...x})=>({...x, posts:posts.filter(c=>c.parent_id === x.id), postLength: comments }));
                let data = {
                    count: Number(scheduleData.count), dataSet: scheduleDataSet, unlocked: JSON.parse(scheduleData.unlocked), style: scheduleStyle, 
                    headline: scheduleData.headline, subheadline: scheduleData.subheadline, adminMail: scheduleData.mail, hashcode: scheduleData.hashcode, previewOptions, type: 'schedule'
                };
                return { success: true, data };
            }else{
                let scheduleDataSet = [{
                    "title": i18next.t('example.column1'), "posts": [/*{
                        "id": "post_0_0", "message": {post: i18next.t('example.element'),headline:null}, "likes": 0, color: scheduleStyle.commentColor,
                        shape: scheduleStyle.shape, type: "comment", body: null, commentLength: 0, comments: []
                    }*/], "id": "column_0", "postLength": 1,
                    color: scheduleStyle.shapeColor, shape: scheduleStyle.shape, visible: true
                }];
                let data = {
                    count: 1, dataSet: scheduleDataSet, unlocked: { delete: true, move: true, all: true, editHeader: true, commentable: true }, style: scheduleStyle, 
                    headline: scheduleData.headline, subheadline: scheduleData.subheadline, adminMail: scheduleData.mail, hashcode: scheduleData.hashcode, previewOptions, type: 'schedule'
                };
                return { success: true, data };
            }
        }
    }catch(error){
        console.error(error);
        
    }
    return { success: false };
}


/**
 * Fetches the data from a freeform pane
 * @param {*} id Hashcode of a pane
 * @param {*} previewOptions Options for preview
 * @returns success + data of pane
 */
async function fetchFreeformData(id, previewOptions = undefined){
    writeToLog(id, "open");
    try{
        let freeformData = await freeform.query().where('hashcode',id).first();
        if(freeformData){
            let lineStyleObj = {
                color: freeformData.line_color, size: freeformData.line_size, dash: (freeformData.line_style !== 'solid'),
                endPlug: freeformData.arrow
            };
            let freeformStyle = {
                shape: freeformData.shape,
                shapeColor: freeformData.shape_color,
                commentColor: freeformData.comment_color,
                likeSymbol: freeformData.like_symbol,
                lineStyle: lineStyleObj,
                lineText: freeformData.line_text,
                circle: true,
                embeddedsAllowed: freeformData.embeddeds,
                commentsAllowed: freeformData.comments,
                likesAllowed: freeformData.likes,
                fontSize: freeformData.font_size,
                backgroundColor: freeformData.background_color,
                backgroundImage: freeformData.background_image,
                font: freeformData.font,
                authorize: freeformData.authorize
            };
            let objects = await freeformObject.query().where('freeform_id',id);
            if(objects){
                let comments = await freeformComment.query().where('freeform_id',id);
                let size = await freeformSize.query().where('hashcode',id).first();
                let lines = await line.query().where('freeform_id',id);    
                if(!size)
                    size = { max_height: 8000, max_width: 8000 };
                else{
                    size = { height:(Math.floor(Number(size.max_height))+60)+"px", width:Math.floor(Number(size.max_width))+"px" }
                }
                objects = objects.map(({freeform_id, title, left, top, comment_length, embedded_length, body, height, width, ...x})=>({...x,styleObject:{top:top!==undefined?String(top).replace('px',''):'',left:left!==undefined?String(left).replace('px',''):''}, commentLength: comment_length, body:body?JSON.parse(body):undefined, title:JSON.parse(title), size:{height:height?Math.floor(height.replace('px','')):height, width:width?Math.floor(width.replace('px','')):width}}));
                lines = lines?lines.map(x=>({...x,text:x.line_text})):[];
                comments = comments?comments.map(({parent_id, freeform_id, url, body, ...x})=>({...x, parent_id, body:body?JSON.parse(body):undefined,text: url})):[];
                let freeformDataSet = objects.map(x=>({...x, comments:comments.filter(c=>c.parent_id === x.id)}));
                let data = {
                    count: Number(freeformData.count), lineArray: lines, deleteObject: null, dataSet: freeformDataSet, addElement: null, adminMail: freeformData.mail, hashcode: freeformData.hashcode,
                    addLine: null, unlocked: JSON.parse(freeformData.unlocked), style: freeformStyle, headline: freeformData.headline, subheadline: freeformData.subheadline, size, previewOptions, type: 'freeform'
                };
                return { success: true, data }
            }else{
                let size = { max_height: 8000, max_width: 8000 };
                let data = {
                    count: 0, lineArray: [], deleteObject: null, dataSet: [], addElement: null, adminMail: freeformData.mail, hashcode: freeformData.hashcode,
                    addLine: null, unlocked: { delete: true, move: true, all: true, commentable: true}, style: freeformStyle,
                    headline: freeformData.headline, subheadline: freeformData.subheadline, size, previewOptions, type: 'freeform'
                };
                return { success: true, data };
            }

        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}

/**
 * Fetches the data from a timeline pane
 * @param {*} id Hashcode of a pane
 * @param {*} previewOptions Options for preview
 * @returns success + data of pane
 */
async function fetchTimelineData(id, previewOptions = undefined){
    writeToLog(id, "open");
    try{
        let timelineData = await timeline.query().where('hashcode',id).first();
        if(timelineData){
            let timelineStyle = {
                shape: timelineData.shape,
                shapeColor: timelineData.shape_color,
                commentColor: timelineData.comment_color,
                likeSymbol: timelineData.like_symbol,       
                embeddedsAllowed: timelineData.embeddeds,
                commentsAllowed: timelineData.comments,
                likesAllowed: timelineData.likes,
                fontSize: timelineData.font_size,
                backgroundColor: timelineData.background_color,
                backgroundImage: timelineData.background_image,
                font: timelineData.font,
                elementWidth: timelineData.element_width,
                lineSize: timelineData.line_size,
                lineColor: timelineData.line_color,
                authorize: timelineData.authorize
            };
            let posts = await timelinePost.query().where('timeline_id',id);
            if(posts && posts.length>0){
                let comments = await timelineComment.query().where('timeline_id',id);
                comments = comments?comments.map(({parent_id, timeline_id, message, body, ...x})=>({...x, parent_id, text:message!=='"null"'? message:null, body:JSON.parse(body)})):[];
                posts = posts?posts.map(({timeline_id, message, comment_length, body, ...x})=> ({...x, commentLength: comment_length, title: message!=='"null"'? JSON.parse(message):null, body: body!=='"null"'?JSON.parse(body):null,comments: comments.filter(c=> c.parent_id === x.id)})):[];
                let timelineDataSet = posts;
                let data = {
                    count: Number(timelineData.count), dataSet: timelineDataSet, unlocked: JSON.parse(timelineData.unlocked), style: timelineStyle, 
                    headline: timelineData.headline, subheadline: timelineData.subheadline, adminMail: timelineData.mail, hashcode: timelineData.hashcode, previewOptions, type: 'timeline'
                };
                return { success: true, data };
            }else{
                let timelineDataSet = [];
                let data = {
                    count: 0, dataSet: timelineDataSet, unlocked: { delete: true, move: true, all: true, commentable: true }, style: timelineStyle, 
                    headline: timelineData.headline, subheadline: timelineData.subheadline, adminMail: timelineData.mail, hashcode: timelineData.hashcode, previewOptions, type: 'timeline'
                };
                return { success: true, data };
            }

        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}

/**
 * Saves the pane data of a freeform to db
 * @param {*} hashcode Hashcode of the pane
 * @param {*} object Dataset of the pane
 */
async function saveFreeformData(hashcode, object){
    writeToLog(hashcode, "write");
    let lines = object.lineArray.filter((x)=>{ return x.start!=null && x.start.length>0 }).map(({text,...x})=> ({ ...x,line_text:text, freeform_id:hashcode }));
    let comments = [];
    object.dataSet.forEach(innerObjects=>{
        if(innerObjects.comments){
            let innerComments = innerObjects.comments.map(({body,text,likes,...x})=>({...x,body:JSON.stringify(body),freeform_id:hashcode,parent_id:innerObjects.id,url:text,likes:Number(likes)}));
            comments = comments.concat(innerComments);
        }
    });
    //let objects = object.dataSet.map(({comment_length,embedded_length,...item})=>({...item,comment_length:item.commentLength,embedded_length:item.embeddedLength }));
    let objects = object.dataSet.map(({comments, body, styleObject, 
        size, title,commentLength,...item})=>({...item, body:JSON.stringify(body), 
            top: styleObject?.top ? String(styleObject.top) : "0", left: styleObject?.left ? String(styleObject.left) : "0", 
            height:size && size.height?size.height.toString().replace('px',''):null, 
            width: size && size.width?size.width.toString().replace('px',''):null, 
            title: JSON.stringify(title), comment_length:commentLength, freeform_id:hashcode }));
    try {
        // response = await freeform.query().where('hashcode', hashcode).first();
        //if(response){
        await transaction(freeformComment, line, freeformObject, freeform,  async (freeformComment, line, freeformObject, freeform)=>{
            let response = await freeform.query().where('hashcode', hashcode).first();
            if(response){
                await freeform.query().patch({ count: object.count, unlocked: JSON.stringify(object.unlocked), hashcode: hashcode }).where('hashcode', hashcode);
                await line.query().delete().where('freeform_id',hashcode);
                await freeformComment.query().delete().where('freeform_id',hashcode);
                await freeformObject.query().delete().where('freeform_id',hashcode);
                if(objects?.length>0)
                    await freeformObject.query().insert(objects);
                if(comments?.length>0)
                    await freeformComment.query().insert(comments);
                if(lines?.length>0)
                    await line.query().insert(lines);  
            } 
        });
        return { success: 'ok' };
        //}    
    }
    catch(error){
        console.error(error);
    }
    return { success: 'error' };
}

/**
 * Saves the pane data of a schedule to db
 * @param {*} hashcode Hashcode of the pane
 * @param {*} object Dataset of the pane
 */
async function saveScheduleData(hashcode, object){
    writeToLog(hashcode, "write");
    let posts = object.dataSet.map(data =>data.posts?.map(({parent_id, order_id, commentLength, title, body, type,...x}) => {
        return ({...x, parent_id: data.id,comment_length:commentLength, message: JSON.stringify(title), body:JSON.stringify(body), type: 'null'})}));
    if(posts !== undefined && posts.length>0){
        posts = posts?.map(elem=>{return elem?.map((obj,index)=>({...obj,order_id:index.toString()}))});
        posts = posts?.reduce(function(pre, cur) {return pre.concat(cur);});
    }
    let comments = posts?.map(data=> data.comments.map(({parent_id, schedule_id, body, text, ...x})=>{
        return({...x,body:JSON.stringify(body),schedule_id:hashcode, parent_id:data.id, message: text})}));
    if(comments !== undefined && comments.length>0)
        comments = comments?.reduce(function(pre, cur) {return pre.concat(cur);});
    let objects = object.dataSet.map(({posts,schedule_id, order_id, postLength, ...item},index)=>({...item,schedule_id:hashcode,order_id:index.toString(), comments: postLength}));
    posts = posts?.map(({comments,...item})=>{return({...item,schedule_id:hashcode})});
    try {
        //let response = await schedule.query().where('hashcode',hashcode).first();
        //if(response){
        await transaction(schedule, scheduleObject, schedulePost, scheduleComment,  async (schedule, scheduleObject, schedulePost, scheduleComment)=>{
            const base = await schedule.query().where('hashcode',hashcode).first();
            if(base){
                await schedule.query().patch({ count: object.scheduleCount, unlocked: JSON.stringify(object.unlocked), hashcode: hashcode }).where('hashcode', hashcode);
                await scheduleComment.query().delete().where('schedule_id', hashcode);
                await schedulePost.query().delete().where('schedule_id', hashcode);
                await scheduleObject.query().delete().where('schedule_id', hashcode);
                if(objects?.length>0){
                    await scheduleObject.query().insert(objects);
                }
                if(comments?.length>0){
                    await scheduleComment.query().insert(comments);
                }
                if(posts?.length>0){
                    await schedulePost.query().insert(posts);   
                }
            }
        });
        return { success: 'ok' };            
        //}
    }
    catch(error){
        console.error(error);
    }
    return { success: 'error' };
}

/**
 * Saves the pane data of a timeline to db
 * @param {*} hashcode Hashcode of the pane
 * @param {*} object Dataset of the pane
 */
async function saveTimelineData(hashcode, object){
    writeToLog(hashcode, "write");
    if(true){
        let posts = object.dataSet.map(({order_id, commentLength, title, body, type,...x}) => {
            return ({...x, comment_length:commentLength, message: JSON.stringify(title), body:JSON.stringify(body), type: 'null'})});
        if(posts !== undefined && posts.length>0){
            posts = posts?.map((obj,index)=>({...obj,order_id:index.toString()}));
        }
        let comments = posts?.map(data=> data.comments.map(({ parent_id, timeline_id, body, text, ...x})=>{
            return({...x,body:JSON.stringify(body),timeline_id:hashcode, parent_id:data.id, message: text})}));
        if(comments !== undefined && comments.length>0)
            comments = comments?.reduce(function(pre, cur) {return pre.concat(cur);});
        posts = posts?.map(({comments,...item})=>{return({...item,timeline_id:hashcode})});
        try {
            //let response = await timeline.query().where('hashcode',hashcode).first();
            //if(response){
            await transaction(timeline, timelinePost, timelineComment,  async (timeline, timelinePost, timelineComment)=>{
                let response = await timeline.query().where('hashcode',hashcode).first();
                if(response){
                    await timeline.query().patch({ count: object.count, unlocked: JSON.stringify(object.unlocked), hashcode: hashcode }).where('hashcode', hashcode);
                    await timelineComment.query().delete().where('timeline_id', hashcode);
                    await timelinePost.query().delete().where('timeline_id', hashcode);
                    if(comments?.length>0){
                        await timelineComment.query().insert(comments);
                    }
                    if(posts?.length>0){
                        await timelinePost.query().insert(posts);   
                    }
                }
            });
            return { success: 'ok' };            
            //}
        }
        catch(error){
            console.error(error);
        }
    }
    return { success: 'error' };   
}


async function checkPassword(id, password, type){
    try{
        let boardType = "freeform";
        let paneData = await freeform.query().where('hashcode', id).first();
        if(!paneData){
            boardType = "schedule";
            paneData = await schedule.query().where('hashcode', id).first();
        }
        if(!paneData){
            boardType = "timeline";
            paneData = await timeline.query().where('hashcode', id).first();
        }
        if(paneData){
            if(type === "moderator"){
                const match = await compare(password, paneData.password_admin);
                if(match){
                    let dashboard = await allPanes.query().where('hash_pane',id).first();
                    return { success: true,  type: boardType, adminMail: paneData.mail, noPassHash: paneData.no_pass_hash };
                }
            }else{
                const match = await compare(password, paneData.password_pane);
                if(match){
                    return { success: true,  type: boardType, adminMail: paneData.mail };
                }
            }
        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}

async function checkPasswordLess(id, type){
    try{
        let boardType = "freeform";
        let paneData = await freeform.query().where('no_pass_hash', id).first();
        if(!paneData){
            boardType = "schedule";
            paneData = await schedule.query().where('no_pass_hash', id).first();
        }
        if(!paneData){
            boardType = "timeline";
            paneData = await timeline.query().where('no_pass_hash', id).first();
        }
        if(paneData){
            return { success: true,  type: boardType, adminMail: paneData.mail, hashcode: paneData.hashcode };
        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}


export default function(fastify, options, done){
    fastify.get('/:id', async (request, reply) => {
        let id = request.params.id;
        let isExport = request.query.export;
        let isPreview = request.query.preview;
        let previewOptions = undefined;
        try{
            let allPaneData = null;
            if(isExport===undefined && isPreview===undefined)
                allPaneData = await allPanes.query().where('hash_pane',id).first();
            else if(isPreview !== undefined){
                allPaneData = await share.query().where('prev_hash',id).first();
                allPaneData.type = allPaneData.typname;
                id = allPaneData.pane_hash;
                previewOptions = { type: allPaneData.typname.toLowerCase(), likesAllowed: allPaneData.likes, commentsAllowed: allPaneData.comments };
            }else{
                allPaneData = await allPanes.query().where('no_pass_hash',id).first();
                id = allPaneData.hash_pane;
            }
            if(allPaneData){
                if(allPaneData.type==='Freeform'){
                    return fetchFreeformData(id, previewOptions);
                }else if(allPaneData.type==='Schedule' || allPaneData.type==='Stream'){
                    return fetchScheduleData(id, previewOptions);
                }else{
                    return fetchTimelineData(id, previewOptions);
                }
            }
        }catch(error){
            console.error(error);
        }
        return {success: false};
    })

    fastify.get('/timelinedev', async(request,reply)=>{
        const id = "timelinedev";
        const previewOptions = undefined;
        return fetchTimelineData(id,previewOptions);
    })

    fastify.post('/:id', async(request,reply)=>{
        let id = request.params.id;
        let data = request.body.data;
        if(data){
            if(data.lineArray){
                return saveFreeformData(id, data);
            }else if(data.style.elementWidth){
                return saveTimelineData(id, data);
            }else{
                return saveScheduleData(id, data);
            }
        }
        return {success: true};
        
    })

    fastify.post('/timelinedev', async(request,reply)=>{
        const id = "timelinedev";
        const data = undefined;
        return saveTimelineData(id,data);
    })

    fastify.post('/login', async(request, reply)=>{
        let data = request.body;
        if(data && data.id && data.type && data.password){

            return checkPassword(data.id, data.password, data.type);
        }else{
            return {success: false};
        }
    });

    fastify.get('/passwordless/:id', async(request,reply)=>{
        let id = request.params.id;
        if(id!==undefined){
            return checkPasswordLess(id);
        }else{
            return {success: false};
        }
    });

    done()
}