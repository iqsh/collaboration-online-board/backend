// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import path, { join } from 'path';
import { ensureDir, pathExists, remove } from 'fs-extra';
import { checkFile, createImageFromPDF } from '../../lib/utils.mjs';
import { promisify } from 'util';
import { pipeline } from 'stream';
import { writeFile } from 'fs/promises';
import { extension } from 'mime-types';
import StreamZip from 'node-stream-zip';
const pump = promisify(pipeline)
const { randomUUID } = await import('crypto');
const __dirname = new URL('.', import.meta.url).pathname;

/**
 * Get the files, if they are not encrypted
 * @param reply Response-Handler
 * @param hashcode The id of the pane
 * @param fileName name of the file
 * @param isThumbnail If is thumbnail from pdf
 */
async function getFile(reply,hashcode, fileName, isThumbnail, isChat){
    try{
        const currentPath = (isChat?join("uploads", "chats", hashcode, fileName):join("uploads", hashcode, fileName)).split('&')[0];
        const root = join(__dirname, "..", "..");
        let result = await pathExists(join(root, currentPath));
        if(result){
            return await reply.sendFile(currentPath);
        }
        if(isThumbnail){
            const pdfPlaceholderImage = join("public","images","file-pdf.svg");
            return await reply.sendFile(pdfPlaceholderImage);
        }else{
            const placeholderImage = join("public","images","placeholder-image.png");
            return await reply.sendFile(placeholderImage);
        }        
    }catch(error){
        console.error(error);
    }
}

/**
 * Saves the files not encrypted
 * @param request Request-Handler
 */
async function saveFile(hashcode, file){
    try{
        let dir = 'uploads/' + hashcode;
        await ensureDir(dir);
        let fileExtension = null;
        if(file.filename.includes('.h5p'))
            fileExtension = "h5p";
        else if(file.filename.includes('.qmkr'))
            fileExtension = "qmkr";
        else if(file.filename.includes('.gtbz'))
            fileExtension = "gtbz";
        else if(file.filename.includes('.mtv30'))
            fileExtension = "mtv30";
        else if(file.filename.includes('.ubz'))
            fileExtension = "ubz";
        else if(file.filename.includes('.numbers'))
            fileExtension = "numbers";
        else if(file.filename.includes('.key'))
            fileExtension = "key";
        else if(file.filename.includes('.pages'))
            fileExtension = "pages";
        else if(file.filename.includes('.wscdoc'))
            fileExtension = "wscdoc"
        else
            fileExtension = extension(file.mimetype);
        const destinationPath = join(dir, randomUUID() + "." + fileExtension);
        await writeFile(destinationPath, await file.toBuffer());
        if(file.filename.includes('.h5p')){
            if(await delay(100,true)){    
                const root = join(__dirname, "..", "..");
                //initialize unzipper
                const zip = new StreamZip.async({ file: join(root, destinationPath) });
                //create folder for unzipper
                let zipLocation = join(root,destinationPath);
                await ensureDir(zipLocation.replace('.h5p',''));
                //extract and close unzipper after operation
                await zip.extract(null, zipLocation.replace('.h5p',''));
                await zip.close();
                //delete h5p file after extraction 
                await remove(join(root,zipLocation));
                let filePath = destinationPath.toString().replace(dir, '/api/file/h5p/'+ hashcode).replace('.h5p','');
                return {success: true, path: filePath, type: 'h5p'};
            }
        }
        else{
            let filePath = destinationPath.toString().replace('uploads/', '/api/file/');
            if(fileExtension === "pdf"){
                try{
                    const result = await createImageFromPDF(destinationPath);
                    return {success: true, path: filePath, type: file.mimetype};
                }catch(error){
                    console.error(error);
                    return {success: false, path: filePath, type: file.mimetype};
                }
            }else{
                return {success: true, path: filePath, type: file.mimetype};
            }
        }
    }catch(error){
        console.error(error);
        return {success: false};
    }
}

function delay(t, v) {
    return new Promise(function(resolve) { 
        setTimeout(resolve.bind(null, v), t)
    });
 }

export default function(fastify, options, done){
    fastify.get('/h5p/:hashcode/*', async (request, reply) => {
        let hashcode = request.params.hashcode;
        let file = request.params['*'].split('?')[0];
        return await getFile(reply, hashcode, file, false);
    })

    fastify.get('/:hashcode/:file', async (request, reply) => {
        let hashcode = request.params.hashcode;
        let file = request.params.file.replace("//","/");
        let queryPdf = request.query.pdf;
        return await getFile(reply, hashcode, file, queryPdf);
    })
    fastify.get('/chats/:hashcode/:file', async (request, reply) => {
        let hashcode = request.params.hashcode;
        let file = request.params.file.replace("//","/");
        return await getFile(reply, hashcode, file, false, true);
    })

    fastify.post('/upload/', async(request,reply)=>{
        let file = await request.body.file;
        const body = Object.fromEntries(
            Object.keys(request.body).map((key) => [key, request.body[key].value])
        );
        if(checkFile(file.filename, file.mimetype)) {
            return saveFile(body.hashcode, file);
        }
        return {status: "Error!", message: "Couldn't upload!"}
    })

    fastify.delete('/delete/', async(request,reply)=>{
        if(request.body.t != null && (request.body.t === 'f' || request.body.t === 's' || request.body.t === 't') && request.body.hashcode!==null) {
            let filePath = request.body.path.replace('/api/file/','uploads/').replace('h5p/','').split('?')[0];
            try{
                await remove(filePath);                
                if(filePath.split('.').pop()==="pdf"){
                    await remove(filePath.replace('.pdf','.png'));
                }
                return {success:true};
            }catch(error){
                console.error(error);
            }   
        }
        return {status: "Error!", message:"Couldn't delete!"};
    })

    fastify.delete('/delete/all', async(request, reply)=>{
        if(request.body.t != null && (request.body.t === 'f' || request.body.t === 's' || request.body.t === 't') && request.body.hashcode!=null) {
            let data = request.body.data;
            /*if(request.body.t === 's')
                array = array.filter(object => (object.type ==='file')).map(x=>x.message.post.replace('/api/file/','uploads/').replace('h5p/','').split('?')[0]);
            else if(request.body.t === 'f')*/
            let posts = data.filter(object => (object.body && object.body?.type ==='file')).map(x=>x.title.post.replace('/api/file/','uploads/').replace('h5p/','').split('?')[0]);
            let commentFiles = data.filter(object => object.comments.length>0)
                        .map(object => object.comments.filter(comment=>comment.body?.type ==='file')
                        .map(x=>x.text.replace('/api/file/','uploads/').replace('h5p/','').split('?')[0])).flat();
            let array = posts.concat(commentFiles);
            for(const filePath of array){
                await remove(filePath);
                if(filePath.split('.').pop()==="pdf"){
                    await remove(filePath.split('.').pop()+".png");
                }
            };
            return {success:true};
        }
        return {status: "Error!", message:"Couldn't delete!"};
    })

    done()
}