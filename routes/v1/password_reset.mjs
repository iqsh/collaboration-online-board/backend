// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { getPasswordResetData, preparePasswordReset, resetPassword } from "../../lib/passwordReset.mjs";

export default function(fastify, options, done){
    /*fastify.get('/', async (request, reply) => {
        return { hello: 'password-reset' }
    })

    fastify.get('/:hashcode', async(request,reply)=>{
        let hashcode = request.params.hashcode;
        return getPasswordResetData(hashcode);
    })*/

    fastify.post('/:hashcode', async(request,reply)=>{
        return resetPassword(request);
    })

    /*fastify.post('/:hashcode', async(request,reply)=>{
        let hashcode = request.params.hashcode;
        return preparePasswordReset(hashcode, token);
    })*/

    done()
}