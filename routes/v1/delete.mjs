// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import freeform from "../../database/models/freeform.mjs";
import schedule from "../../database/models/schedule.mjs";
import timeline from "../../database/models/timeline.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import { compareSync } from 'bcrypt';
import { join } from 'path';
import { URL } from 'url'
import { deleteAsync } from 'del';
import allPanes from "../../database/models/allPanes.mjs";

async function deleteFromTable(table,id,sessionMail){
    if(table && id && sessionMail){
        const tableData = await table.query().where('hashcode',id).first();
        if(tableData!==undefined && tableData!==null && sessionMail && tableData.mail === sessionMail){
            let deleteResult = await table.query().delete().where('hashcode',id);
            if(deleteResult==0){
                const __dirname = new URL('.', import.meta.url).pathname;
                const dir = join(__dirname, "..","..", "uploads", id);
                let deleting = await deleteAsync(dir);
                if(deleting){
                    return {status:"ok"}
                }
            }
            return {status:false,error:"unexpected"};
        }else{
            return {status:false,error:"password wrong"}
        }
    }
}

async function deletePane(id, token) {
    if(token){
        const session = await sessionStore.query().where('session_id',token).first();
        const tableType = (await allPanes.query().where('hash_pane',id).first()).type;
        const table = tableType==="Freeform"?freeform:tableType==="Stream" || tableType==="Schedule"?schedule:timeline;
        return await deleteFromTable(table, id,session.mail);
    }
    return {status:false,error:"unexpected"};
}       

export default function(fastify, options, done){
    fastify.post('/:id', async (request, reply) => {
        let id = request.params.id.toLowerCase();
        let token = request.headers.token;
        return deletePane(id, token);
    })

    done()
}