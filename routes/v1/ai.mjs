// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { raw, transaction } from "objection";
const { randomUUID } = await import('crypto');
import { access, rm, unlink, writeFile as writeFilePromise, readFile, copyFile, appendFile } from 'fs/promises';
import { ensureDir } from "fs-extra";
import i18next from 'i18next';
import { join } from "path";
import sessionStore from "../../database/models/sessionStore.mjs";
import adminPanel from "../../database/models/adminPanel.mjs";
import { getUserFromToken } from "../../lib/utils.mjs";
import aiChat from "../../database/models/aiChat.mjs";
import aiUser from "../../database/models/aiUser.mjs";
import aiPerson from "../../database/models/aiPerson.mjs";
import aiChatMessage from "../../database/models/aiChatMessage.mjs";
import aiPersonCategory from "../../database/models/aiPersonCategory.mjs";
import aiChatShared from "../../database/models/aiChatShared.mjs";
import aiChatMessageSession from "../../database/models/aiChatMessageSession.mjs";
import Globals from "../../lib/globalvars.mjs";

const __dirname = new URL('.', import.meta.url).pathname
const de = JSON.parse(
    await readFile(
        new URL('../../locales/de.json', import.meta.url)
    )
);

function writeToLog (prompt, user, chatId){
    if(Globals.debug){
        const date = new Date();
        let file = join(__dirname,"..","..", "logs",`${date.getFullYear() + "_" + String(date.getMonth()+1).padStart(2,'0') + "_" + String(date.getDate()).padStart(2,'0')}-ai-log.txt`);
        let formatedDate = date.getFullYear() + "/" + String(date.getMonth()+1).padStart(2,'0') + "/" + String(date.getDate()).padStart(2,'0') + "-" + String(date.getHours()).padStart(2,'0') + ":" + String(date.getMinutes()).padStart(2,'0') + ":" + String(date.getSeconds()).padStart(2,'0');
        let log = formatedDate + ";" + chatId + ";" + user + ";" + prompt.replace("\n", " ") + "\n";
        appendFile(file, log, (err) => {
            if (err) console.error(err);
        });
    }
}

i18next.init({
    lng: 'de',
    resources: {
        de
    }
});

export default function (fastify, options, done) {
    fastify.get('/authorized', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            let session = await sessionStore.query().where('session_id', token).first();
            if (session) {
                const user = await adminPanel.query().where('mail', session.mail).first();
                if (user) {
                    return { success: user.ai }
                }
            }
        }
        return { success: false };
    });

    fastify.get('/', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chats = await aiChat.query().where('ai_chat.user_mail', user.mail).select('ai_chat.uuid', 'ai_chat.name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', 'ai_chat.description');
                const token = await aiUser.query().where('mail', user.mail).first();
                return { success: true, chats: chats, tokensUsed: token ? token.token_used : 0, tokenQuota: token ? token.token_quota : 0 };
            }
        }
        return { success: false };
    });

    fastify.get('/tokens', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const token = await aiUser.query().where('mail', user.mail).first();
                return { success: true, tokensUsed: token ? token.token_used : 0, tokenQuota: token ? token.token_quota : 0 };
            }
        }
        return { success: false };
    });


    fastify.get('/overview', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const subquery = aiChatMessage.query()
                    .where('ai_chat_message.user', user.mail)
                    .whereRaw('ai_chat_message.meta::jsonb->\'error\' is null')
                    .andWhere('ai_chat_message.creator', 'system')
                    .groupBy('chat_uuid')
                    .as('subquery')
                    .select('chat_uuid', raw('max(create_date) as max_date'));

                const query = aiChat.query()
                    .where('ai_chat.user_mail', user.mail)
                    .leftJoin(subquery.as('subquery'), 'ai_chat.uuid', 'subquery.chat_uuid')
                    .leftJoin('ai_chat_message', 'subquery.chat_uuid', 'ai_chat_message.chat_uuid')
                    .whereRaw('ai_chat_message.create_date = subquery.max_date')
                    .select('ai_chat.uuid', 'ai_chat.name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', 'subquery.max_date', 'ai_chat_message.message', 'ai_chat.person_name as person')
                    .orderBy('subquery.max_date', 'desc')
                    .limit(5);
                //console.log(query.toKnexQuery().toSQL());
                const chats = await query.execute();
                const personQuery = aiPerson.query().leftJoin('admin_panel', 'admin_panel.mail', 'ai_person.creator').leftJoin('ai_person_category', 'ai_person_category.person_uuid', 'ai_person.uuid').select('ai_person.uuid', 'ai_person.name', 'ai_person.avatar', 'ai_person.model', raw('array_agg(ai_person_category.category) as categories'), 'ai_person.description', 'ai_person.creator as username', 'ai_person.system_prompt').groupBy('ai_person.uuid', 'ai_person.name', 'ai_person.avatar').where(function () {
                    this.where('ai_person.shared', true).andWhere('admin_panel.dnr', user.dnr)
                }).orWhere('ai_person.creator', user.mail)

                    .orderBy('ai_person.create_date', 'desc').limit(5);
                const persons = await personQuery;
                return { success: true, chats: chats, persons: persons }
            }
        }
        return { success: false };
    });

    fastify.get('/person/', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const persons = await aiPerson.query().leftJoin('admin_panel', 'admin_panel.mail', 'ai_person.creator').leftJoin('ai_person_category', 'ai_person_category.person_uuid', 'ai_person.uuid').select('ai_person.uuid', 'ai_person.name', 'ai_person.avatar', 'ai_person.model', raw('array_agg(ai_person_category.category) as categories'), 'ai_person.description', 'ai_person.creator as username', 'ai_person.system_prompt').groupBy('ai_person.uuid', 'ai_person.name', 'ai_person.avatar').where(function () {
                    this.where('ai_person.shared', true).andWhere('admin_panel.dnr', user.dnr)
                }).orWhere('ai_person.creator', user.mail);
                return { success: true, persons: persons }
            }
        }
        return { success: false };
    });

    fastify.put('/person/', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                try {
                    const data = request.body;
                    if (data === null || data === undefined || data.name === null || data.name === undefined)
                        return { success: false };
                    //console.log({data});
                    const databaseObject = {
                        name: data.name.value,
                        creator: user.mail,
                        system_prompt: data.systemprompt.value || "",
                        description: data.description.value || "",
                        type: 'person',
                        model: data.model.value || Globals.defaultTextModel

                    };
                    const person = await aiPerson.query().insertAndFetch(databaseObject);
                    if (data.file && (data.file.value || data.file.file)) {
                        //TODO: check if file png
                        let dir = 'uploads/chats/' + person.uuid;
                        try {
                            await ensureDir(dir);
                        } catch (error) {
                            console.error(error);
                            return { success: false };
                        }
                        let backgroundImage;
                        try {
                            const destinationPath = join(dir, randomUUID() + ".png");
                            let base64Data;
                            if (data.file.value)
                                base64Data = data.file.value.replace(/^data:image\/\w+;base64,/, '');
                            else {
                                const file = data.file.toBuffer();
                                base64Data = file.toString('base64');
                            }
                            await writeFilePromise(destinationPath, base64Data, 'base64');
                            backgroundImage = destinationPath.toString().replace("uploads/chats/", "/api/file/chats/");
                        } catch (error) {
                            console.error(error);
                            return { success: false };
                        }
                        await aiPerson.query().where('uuid', person.uuid).patch({ avatar: backgroundImage, shared: data.shared.value === 'true' });
                    } else {
                        await aiPerson.query().where('uuid', person.uuid).patch({ shared: data.shared.value === 'true' });
                    }
                    const categories = JSON.parse(data.categories.value);
                    for (let i = 0; i < categories.length; i++) {

                        await aiPersonCategory.query().insert({ person_uuid: person.uuid, category: categories[i] });
                    }
                    return { success: true };
                } catch (error) {
                    console.error(error);
                    return { success: false };
                }
            }
        }
        return { success: false };
    });

    fastify.post('/person/:personId', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const personId = request.params.personId;
                const person = await aiPerson.query().where('uuid', personId).andWhere(function () { this.where('user_mail', user.mail).orWhere('creator', user.mail) }).first();
                if (person) {
                    try {
                        const data = request.body;
                        if (data.file && data.file.value) {
                            //TODO: check if file is png
                            let dir = 'uploads/chats/' + person.uuid;
                            try {
                                await ensureDir(dir);
                            } catch (error) {
                                console.error(error);
                                return { success: false };
                            }
                            const destinationPath = join(dir, randomUUID() + ".png");
                            try {
                                const base64Data = data.file.value.replace(/^data:image\/\w+;base64,/, '');
                                await writeFilePromise(destinationPath, base64Data, 'base64');
                            } catch (error) {
                                console.error(error);
                                return { success: false };
                            }
                            let backgroundImage = destinationPath.toString().replace("uploads/chats/", "/api/file/chats/");
                            //delete old image
                            let oldPath = person.avatar;
                            if (oldPath !== null && oldPath.length > 0) {
                                let filePath = oldPath.replace('/api/file/chats/', 'uploads/chats/').split('?')[0];
                                try {
                                    await unlink(filePath);
                                } catch (error) {
                                    console.error(error);
                                    return { success: false };
                                }
                            }
                            await aiPerson.query().where('uuid', person.uuid).patch({ avatar: backgroundImage, shared: data.shared.value === 'true', name: data.name.value, system_prompt: data.systemprompt.value, description: data.description.value });
                        } else {
                            await aiPerson.query().where('uuid', person.uuid).patch({ shared: data.shared.value === 'true', name: data.name.value, system_prompt: data.systemprompt.value, description: data.description.value });;
                        }
                        await aiPersonCategory.query().where('person_uuid', person.uuid).delete();
                        const categories = JSON.parse(data.categories.value);
                        for (let i = 0; i < categories.length; i++) {
                            await aiPersonCategory.query().insert({ person_uuid: person.uuid, category: categories[i] });
                        }
                        return { success: true };
                    } catch (error) {
                        console.error(error);
                        return { success: false };
                    }
                }
            }
        }
        return { success: false };
    });

    fastify.get('/person/:personId', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            const personId = request.params.personId;
            if (user && user.ai) {
                try {
                    const person = await aiPerson.query().leftJoin('ai_person_category', 'ai_person_category.person_uuid', 'ai_person.uuid').select('ai_person.uuid', 'ai_person.name', 'ai_person.avatar', 'ai_person.description', raw('array_agg(ai_person_category.category) as categories'), 'ai_person.creator as username', 'ai_person.system_prompt', 'ai_person.shared').groupBy('ai_person.uuid', 'ai_person.name', 'ai_person.avatar').where('uuid', personId).andWhere(function () {
                        this.where('creator', user.mail).orWhere('user_mail', user.mail)
                    }).first();
                    if (person) {
                        return { success: true, person: person };
                    }
                } catch (error) {
                    //console.error(error);
                    return { success: false };
                }
            }
        }
        return { success: false };
    });

    fastify.delete('/person/:personId', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const personId = request.params.personId;
                try {
                    const person = await aiPerson.query().where('uuid', personId).andWhere(function () {
                        this.where('creator', user.mail).orWhere('user_mail', user.mail)
                    }).first();
                    if (person) {
                        await aiPersonCategory.query().where('person_uuid', personId).delete();
                        const result = await aiPerson.query().where('uuid', personId).delete();
                        if (result > 0) {
                            let dir = 'uploads/chats/' + person.uuid;
                            try {
                                if (await access(dir).then(() => true, () => false))
                                    await rm(dir, { recursive: true });
                            } catch (error) {
                                console.error(error);
                                return { success: false };
                            }
                        }
                        return { success: true };
                    }
                } catch (error) {
                    //console.error(error);
                    return { success: false };
                }
            }
        }
        return { success: false };
    });

    fastify.get('/chat/', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const subquery = aiChatMessage.query()
                    .where('ai_chat_message.user', user.mail)
                    .groupBy('ai_chat_message.chat_uuid')
                    .as('subquery')
                    .select('ai_chat_message.chat_uuid', raw('max(ai_chat_message.create_date) as max_date'));
                const chats = await aiChat.query().where('ai_chat.user_mail', user.mail).select('ai_chat.uuid', 'ai_chat.name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', raw('coalesce (??,??)', ['subquery.max_date', 'ai_chat.create_date']).as('max_date'), 'ai_chat_shared.valid_until').groupBy('ai_chat.uuid', 'ai_chat.name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', 'subquery.max_date', 'ai_chat_shared.valid_until').leftJoin(subquery.as('subquery'), 'ai_chat.uuid', 'subquery.chat_uuid').leftJoin('ai_chat_shared', 'ai_chat.uuid', 'ai_chat_shared.chat_uuid').orderBy(raw('coalesce (??,??)', ['subquery.max_date', 'ai_chat.create_date']), 'desc');
                return { success: true, chats: chats };
            }
        }
        return { success: false };
    });

    fastify.get('/chat/:chatId', async (request, reply) => {
        const chatId = request.params.chatId;
        const token = request.headers.token;
        const withMessages = request.query.messages;
        if (chatId) {
            if (token) {
                const user = await getUserFromToken(token);
                if (user && user.ai) {
                    const chat = await aiChat.query().where('ai_chat.uuid', chatId).where('ai_chat.user_mail', user.mail).select('ai_chat.name as name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', 'ai_chat.meta', 'ai_chat.model', 'ai_chat.system_prompt', 'ai_chat.person_name as person_name', 'ai_chat.description as person_description', 'ai_chat.user_mail').first();
                    if (chat) {
                        if (withMessages) {
                            const messages = await aiChatMessage.query().select('message', 'create_date', 'creator as user', 'meta').where('chat_uuid', chatId).where('user', user.mail).orderBy('create_date', 'asc');
                            return { success: true, chat: chat, messages: messages };
                        }
                        else {
                            return { success: true, chat: chat };
                        }
                    }
                }
            } else {
                const chat = await aiChat.query().where('ai_chat.uuid', chatId).first();
                if (chat) {
                    return { success: true };
                }
            }
        }
        return { success: false };
    });

    fastify.get('/chat/:chatId/:userId', async (request, reply) => {
        const chatId = request.params.chatId;
        const userId = request.params.userId;
        const chat = await aiChat.query().where('ai_chat.uuid', chatId).select('ai_chat.name as name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', 'ai_chat.meta', 'ai_chat.model', 'ai_chat.system_prompt', 'ai_chat.person_name as person_name', 'ai_chat.description as person_description', 'ai_chat.user_mail').first();
        if (chat) {
            const date = new Date();
            date.setHours(date.getHours() + 2);

            const isShared = await aiChatShared.query().where('chat_uuid', chatId).whereRaw('valid_until::timestamp > ?::timestamp', date).first();
            const messages = await aiChatMessage.query().select('message', 'create_date', 'creator as user', 'meta').where('chat_uuid', chatId).where('user', userId).orderBy('create_date', 'asc');
            let tokens;
            if (chat.type === 'image') {
                tokens = (await aiChatMessage.query().where('chat_uuid', chatId).where('user', userId).where('creator', 'system').count('* as images').first()).images;
            } else {
                tokens = (await aiChatMessage.query().where('chat_uuid', chatId).where('user', userId).select(raw('sum(token::float) as tokens')).first()).tokens;
            }
            if (chat && (isShared || (!isShared && messages.length > 0))) {
                return { success: true, chat: chat, messages: messages, isShared: isShared && (tokens === null || tokens < isShared.limitation) ? true : false };
            }
        }
        return { success: false };
    });


    fastify.put('/chat/', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const data = request.body;
                if (data && data.name) {
                    console.log({ data });
                    const type = data.type || 'chat';
                    const model = data.model || (type === 'chat' ? Globals.defaultTextModel : Globals.defaultImageModel);
                    const style = data.style || "";
                    const temperature = data.temperature || undefined;
                    const top_p = data.top_p || undefined;
                    let metaObject = undefined;
                    if (style)
                        metaObject = { style: style };
                    if (temperature)
                        metaObject = { ...metaObject, temperature: temperature };
                    if (top_p)
                        metaObject = { ...metaObject, top_p: top_p };
                    console.log({ user_mail: user.mail, name: data.name, model: model, type: type, system_prompt: data.systemprompt, meta: metaObject });
                    const chat = await aiChat.query().insert({ user_mail: user.mail, name: data.name, model: model, type: type, system_prompt: data.systemprompt, meta: metaObject });
                    if (chat)
                        return { success: true, uuid: chat.uuid };
                }
            }
        }
        return { success: false };
    });

    fastify.put('/chat/person/:personId', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const data = request.body;
                if (data && data.name) {
                    const personId = request.params.personId;
                    const person = await aiPerson.query().where('uuid', personId).first();
                    if (person) {
                        const type = person.type || 'chat';
                        const model = person.model || (type === 'chat' ? Globals.defaultTextModel : Globals.defaultImageModel);
                        const system_prompt = data.systemprompt;
                        const temperature = data.temperature ?? person.temperature;
                        const top_p = data.top_p ?? person.top_p;
                        let metaObject = undefined;
                        if (temperature)
                            metaObject = { ...metaObject, temperature: temperature };
                        if (top_p)
                            metaObject = { ...metaObject, top_p: top_p };

                        const chat = await aiChat.query().insert({ user_mail: user.mail, name: data.name, model: model, type: type, system_prompt: system_prompt, description: person.description, person_name: person.name, meta: metaObject });
                        if (chat) {
                            if (person.avatar && person.avatar.length > 0) {
                                let dir = 'uploads/chats/' + chat.uuid;
                                const personAvatarPath = person.avatar.replace('/api/file/chats/', 'uploads/chats/').split('?')[0];
                                try {
                                    await ensureDir(dir);
                                } catch (error) {
                                    console.error(error);
                                }
                                try {
                                    const destinationPath = personAvatarPath.replace('uploads/chats/' + person.uuid, 'uploads/chats/' + chat.uuid);
                                    await copyFile(personAvatarPath, destinationPath);
                                    let backgroundImage = destinationPath.toString().replace("uploads/chats/", "/api/file/chats/");
                                    await aiChat.query().where('uuid', chat.uuid).update({ avatar: backgroundImage });
                                } catch (error) {
                                    console.error(error);
                                }
                            }
                            return { success: true, uuid: chat.uuid };
                        }
                    }
                }
            }
        }
        return { success: false };
    });

    fastify.post('/chat/:chatId', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const data = request.body;
                if (data && data.name) {
                    try {
                        const result = await transaction(aiChat.knex(), async (trx) => {
                            const chat = await aiChat.query(trx).where('uuid', chatId).where('user_mail', user.mail).first();
                            if (chat) {
                                let updateChats = false;
                                const updateObject = {};
                                if (data.systemprompt || data.temperature || data.top_p) {

                                    if (data.systemprompt && data.systemprompt !== chat.system_prompt) {
                                        updateObject.system_prompt = data.systemprompt;
                                        updateChats = true;
                                    }
                                    if (data.temperature || data.top_p) {
                                        let meta = chat.meta;
                                        if (data.temperature && data.temperature !== chat.meta?.temperature) {
                                            meta = { ...meta, temperature: data.temperature };
                                        }
                                        if (data.top_p && data.top_p !== chat.meta?.top_p) {
                                            meta = { ...meta, top_p: data.top_p };
                                        }
                                        updateObject.meta = meta;
                                    }

                                    if (updateChats) {
                                        await aiChatMessage.query(trx)
                                            .patch({
                                                meta: raw(`
                                                    CASE
                                                        WHEN json_typeof(meta) = 'null' OR json_typeof(meta) ='string' THEN 
                                                            '{"noContext": true}'::json
                                                        WHEN meta->>'noContext' IS NULL THEN 
                                                            jsonb_set(meta::jsonb, '{noContext}', 'true')::json
                                                        ELSE meta
                                                    END
                                                `)
                                            }).where('chat_uuid', chat.uuid);
                                    }
                                }
                                updateObject.name = data.name;
                                await aiChat.query(trx).where('uuid', chatId).update(updateObject);
                                return { success: true, updateChats: updateChats };
                            } else {
                                return { success: false };
                            }
                        });
                        return result;
                    } catch (error) {
                        console.error(error);
                        return { success: false };
                    }
                }
            }
        }
        return { success: false };

    });

    fastify.put('/chat/:chatId/message', async (request, reply) => {
        const chatId = request.params.chatId;
        console.log({ chatId });
        const data = request.body;
        const token = request.headers.token;
        let message = data.message;
        let meta = data.meta;
        if (data.option) {
            meta = { ...meta, option: data.option, hide: true };
        }
        if (message !== undefined || (message === undefined && data.option !== undefined)) {
            if (token) {
                const user = await getUserFromToken(token);
                if (user === null || user === undefined) return { success: false };
                const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                const userToken = await aiUser.query().where('mail', user.mail).first();
                if (userToken && userToken.token_quota - userToken.token_used <= 0) return { success: true, message: "Tokens exceeded" };
                if (chat) {
                    const session = await aiChatMessageSession.query().insert({ chat_uuid: chatId, user: user.mail, message: message, meta: meta });
                    if(session){
                        writeToLog(message, user.mail, chatId);
                        return { success: true, uuid: session.uuid };
                    }
                }
            } else {
                const chat = await aiChat.query().where('uuid', chatId).first();
                if (chat) {
                    const shared = await aiChatShared.query().where('chat_uuid', chatId).first();
                    const date = new Date();
                    date.setHours(date.getHours() + 2);
                    if (shared && new Date(shared.valid_until) > date) {
                        let tokens;
                        if (chat.type === 'image') {
                            tokens = (await aiChatMessage.query().where('chat_uuid', chatId).where('user', data.user).where('creator', 'system').count('* as images').first()).images;
                        } else {
                            tokens = (await aiChatMessage.query().where('chat_uuid', chatId).where('user', data.user).select(raw('sum(token::float) as tokens')).first()).tokens;
                        }
                        if (tokens === null || tokens < shared.limitation) {
                            const session = await aiChatMessageSession.query().insert({ chat_uuid: chatId, user: data.user, message: message, meta: meta });
                            if(session){
                                writeToLog(message, data.user, chatId);
                                return { success: true, uuid: session.uuid };
                            }
                        } else {
                            return { success: true, message: "Tokens exceeded" };
                        }
                    }
                }
            }
        }
        return { success: false };
    });

    fastify.delete('/chat/:chatId', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                if (chat) {
                    await aiChatMessageSession.query().where('chat_uuid', chatId).delete();
                    await aiChatMessage.query().where('chat_uuid', chatId).delete();
                    await aiChatShared.query().where('chat_uuid', chatId).delete();
                    await aiChat.query().where('uuid', chatId).delete();
                    return { success: true };
                }
            }
        }
        return { success: false };
    });

    fastify.get('/chat/:chatId/share', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                if (chat) {
                    const shared = await aiChatShared.query().where('chat_uuid', chatId).first();
                    const messages = await aiChatMessage.query().where('chat_uuid', chatId).andWhereNot('user', user.mail);
                    const date = new Date();
                    date.setHours(date.getHours() + 2);
                    if (shared && new Date(shared.valid_until) <= date && messages.length > 0) {
                        return { success: true, canShare: false };
                    } else if (shared && new Date(shared.valid_until) <= date && messages.length === 0) {
                        // success false because it should be shared again
                        await aiChatShared.query().where('chat_uuid', chatId).delete();
                        return { success: false };
                    }
                    else if (shared)
                        return { success: true, validUntil: shared.valid_until, limitation: shared.limitation, createdAt: shared.created_at };
                }
            }
        }
        return { success: false };
    });

    fastify.put('/chat/:chatId/share', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const data = request.body;
                const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                if (chat) {
                    await aiChatShared.query().insert({ chat_uuid: chatId, valid_until: data.validUntil, limitation: data.limitation });
                    return { success: true };
                }
            }
        }
        return { success: false };
    });

    fastify.post('/chat/:chatId/share', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const data = request.body;
                const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                if (chat) {
                    const shared = await aiChatShared.query().where('chat_uuid', chatId).first();
                    if (shared) {
                        const limitation = data.limitation ?? shared.limitation;
                        const validUntil = data.validUntil ?? shared.valid_until;
                        const date = new Date(shared.created_at);
                        date.setDate(date.getDate() + 14);
                        if (date >= new Date(validUntil)) {
                            await aiChatShared.query().where('chat_uuid', chatId).update({ valid_until: validUntil, limitation: limitation, chat_uuid: chatId });
                            return { success: true };
                        }
                    } else {
                        await aiChatShared.query().insert({ chat_uuid: chatId, valid_until: data.validUntil, limitation: data.limitation });
                        return { success: true };
                    }
                }
            }
        }
        return { success: false };
    });

    fastify.delete('/chat/:chatId/share', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const query = request.query.chats;
                if (query !== undefined) {
                    const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                    if (chat) {
                        await aiChatMessage.query().where('chat_uuid', chatId).andWhereNot('user', user.mail).delete();
                        await aiChatShared.query().where('chat_uuid', chatId).delete();
                        return { success: true };
                    }
                } else {
                    const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                    if (chat) {
                        await aiChatShared.query().where('chat_uuid', chatId).update({ valid_until: new Date().toISOString(), chat_uuid: chatId });
                        return { success: true };
                    }
                }
            }
        }
        return { success: false };
    });

    fastify.post('/chat/:chatId/copy', async (request, reply) => {
        const token = request.headers.token;
        if (token) {
            const user = await getUserFromToken(token);
            if (user && user.ai) {
                const chatId = request.params.chatId;
                const data = request.body;
                const chat = await aiChat.query().where('uuid', chatId).where('user_mail', user.mail).first();
                if (chat) {
                    const newChat = await aiChat.query().insert({ user_mail: user.mail, name: data.name, model: chat.model, type: chat.type, system_prompt: chat.system_prompt, meta: chat.meta ? chat.meta : undefined, person_name: chat.person_name, description: chat.description });
                    if (newChat) {
                        if (chat.avatar && chat.avatar.length > 0) {
                            const dir = 'uploads/chats/' + newChat.uuid;
                            const personAvatarPath = chat.avatar.replace('/api/file/chats/', 'uploads/chats/').split('?')[0];
                            try {
                                await ensureDir(dir);
                            } catch (error) {
                                console.error(error);
                            }
                            try {
                                const destinationPath = personAvatarPath.replace('uploads/chats/' + chat.uuid, 'uploads/chats/' + newChat.uuid);
                                await copyFile(personAvatarPath, destinationPath);
                                let backgroundImage = destinationPath.toString().replace("uploads/chats/", "/api/file/chats/");
                                await aiChat.query().where('uuid', newChat.uuid).update({ avatar: backgroundImage });
                            } catch (error) {
                                console.error(error);
                            }
                        }
                        return { success: true, uuid: newChat.uuid };
                    }
                }
            }
        }
        return { success: false };
    });

    done();
}
