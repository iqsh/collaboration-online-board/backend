

// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import dashboard from "../../database/models/dashboard.mjs";
import stats from "../../database/models/stats.mjs";
import { totalmem } from "os";
import { promisify } from "util";
import { exec } from "child_process";
import Globals from "../../lib/globalvars.mjs";
const execPromise = promisify(exec);

const getData = async (date)=>{
    let dashboardData = await dashboard.query().where('timestamp', 'like', date + "%");
    if(dashboardData){
        let dashboardStats = await stats.query().first();
        if(dashboardStats){
            let os_data = {totalmem: null, freemem: null};
            os_data.totalmem = totalmem() / 1024 / 1024;
            let freeExecData = await execPromise('free');
            if(freeExecData){
                let data = freeExecData.stdout.split("\n"); 
                let headers = data[0].split(/[ ]+/);
                let index = headers.indexOf("available");
                let mem = data[1].split(/[ ]+/);
                let available = mem[index];
                os_data.freemem = Number(available / 1024).toFixed(0);                    
                let storageExecData = await execPromise('df ' + Globals.uploadsPath);
                if(storageExecData){
                    let storageData = storageExecData.stdout.split("\n");
                    let storageHeaders = storageData[0].split(/[ ]+/);
                    let storageUsedIndex = storageHeaders.indexOf('Used');
                    let storageAvailableIndex = storageHeaders.indexOf('Available');
                    let storageSpaceData = storageData[1].split(/[ ]+/);
                    let storageUsed = storageSpaceData[storageUsedIndex];
                    let storageAvailable = storageSpaceData[storageAvailableIndex];
                    let totalStorage = Number(storageUsed) + Number(storageAvailable);
                    os_data.storage = {total: Number(totalStorage / 1024).toFixed(0), 
                        used: Number(storageUsed / 1024).toFixed(0), 
                        available: Number(storageAvailable / 1024).toFixed(0)};
                    return {data: dashboardData, os: os_data, 
                        pane: { count_panes: dashboardStats.anzahl_oberflaechen,
                            users: dashboardStats.mails, institutions: dashboardStats.schulen,
                            max_user_overall: dashboardStats.max_user_overall, max_user_day: dashboardStats.max_user_day}
                        }
                }
            }
        }
    }
    return { status: 'error' }
}

export default function(fastify, options, done){
    fastify.get('/', async (request, reply) => {
        return getData(new Date().toISOString().substring(0,10));
    })

    fastify.post('/', async(request,reply)=>{
        let data = request.body;
        if(data){
            try{
                let response = await dashboard.query().insert(data).onConflict().ignore();
                if(response)
                    return { status: 'ok' };
            }
            catch(error){
                console.error(error);
            }
            return { status: 'error' };
        }else{
            return { status: 'ok' };
        }
    })

    fastify.post('/login', async(request,reply)=>{
        let password = request.body.password;
        if(password && Globals.dasboardPassword === password){
            try{
                return { status: 'ok' };
            }catch(error){
                console.error(error);
            }
        }
        return { status: 'Not Allowed' };
    });

    fastify.get('/:date', async(request,reply)=>{
       return getData(request.params.date);
    })

    fastify.get('/status', async(request, reply)=>{
        let dashboardData = await dashboard.query().where('timestamp', 'like', new Date().toISOString().substring(0,10) + "%");
        if(dashboardData)
            return { status: 'ok' }
        else
            return { status: 'error' }
    })

    done()
}