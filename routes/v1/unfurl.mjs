// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import createMetascraper from 'metascraper';
import metaAudio from '../../lib/metascraper/audio_fork.js';
import metaVideo from '../../lib/metascraper/video_fork.js';
//import metaAudio from 'metascraper-audio';
//import metaVideo from 'metascraper-video';
import metaTitle from 'metascraper-title';
import metaImage from 'metascraper-image';
import metaDescription from 'metascraper-description';
import metaIframe from 'metascraper-iframe';
import metaLogo from 'metascraper-logo-favicon';
import metaURL from 'metascraper-url';
import metaPublisher from 'metascraper-publisher';
import metaYoutube from 'metascraper-youtube';
import got from 'got';
import { readFile } from 'fs/promises';
import { join } from 'path';
const __dirname = new URL('.', import.meta.url).pathname;

var metascraper = createMetascraper([
    metaTitle(),
    metaImage(),
    metaDescription(),
    metaIframe(),
    metaLogo(),
    metaURL(),
    metaPublisher(),
    metaYoutube(),
    metaAudio({getIframe: ()=>{}}),
    metaVideo({getIframe: ()=>{}})
]);
import 'dotenv/config'

export default function(fastify, options, done){
    fastify.get('/:url', async(request, reply)=>{
        return await (async () => {
            try{
                let reqURL = request.params.url; 
                reqURL = reqURL.replace('&amp;','&');
                const {body: html, url} = await got(decodeURIComponent(reqURL), {timeout: {request: 2000}});
                if({body: html, url}){
                    let metadata = null;
                    try{
                        metadata = await metascraper({html, url});
                    }
                    catch(error){
                        console.log(error);
                    }
                    let validPage = false;
                    let file = join(__dirname,"..","..", "validurls.json")
                    let data = await readFile(file,{encoding: 'utf8'});
                    if(data){
                        let pages = JSON.parse(data).urls;
                        for(var page of pages){
                            if(reqURL.includes(page)){
                                validPage = true;
                                break;
                            }
                        }
                        if(!validPage){
                            if(reqURL.includes("learningapps.org")){
                                metadata.modal = true;
                                var regex = 'src\s*=\s*"([^"]+)"';
                                metadata.iframe = metadata.iframe.match(regex)[1].replace('http://','https://');
                            }else{
                                metadata.iframe = null;
                            }
                        }
                        return JSON.stringify(metadata);
                    }
                }
            }catch(error) {
                console.error(error);
                return {status: 404};
            }
        })()
    })

    done()
}