// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { ensureDir, mkdirs, copy } from "fs-extra";
import { transaction } from "objection";
import bcrypt from 'bcrypt';
import freeform from "../../database/models/freeform.mjs";
import freeformObject from "../../database/models/freeformObject.mjs";
import freeformComment from "../../database/models/freeformComment.mjs";
import line from "../../database/models/line.mjs";
import Globals from "../../lib/globalvars.mjs";
import { updatePaneConfiguration, uploadBackground, checkValidDomain, createHashPane, createNoPasswordHash } from "../../lib/utils.mjs";
import { createPaneMail as sendMailForSameUser } from "../../lib/mail.mjs";
import { prepareRegistration as sendMailForDifferentUser } from "../../lib/registration.mjs";
import { copyDifferentUser, getCurrentShare, sharePane, unsharePane } from "../../lib/share.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import { addPaneToFavorites, removePaneFromFavorites } from "../../lib/favorites.mjs";
import fastify from "fastify";

/**
 * Copies an existing pane
 * @param {*} params Parameters
 * @param {*} hashcode Hashcode of pane
 * @param {boolean} isSameUser If is for the same user or not
 * @returns success: true || false
 */
async function copyPane(params, hashcode, isSameUser, request) {
    try{
        let freeformData = await freeform.query().where('hashcode', hashcode).first();
        let token = request.headers.token;
        let session = null;
        if(token && isSameUser){
            session = await sessionStore.query().where('session_id',token).first();
        }
        if(freeformData && ((isSameUser && session && session.mail === freeformData.mail) || !isSameUser)){
            let userPassword = params.userPassword;
            let adminPassword = params.adminPassword;
            if (userPassword != null && userPassword != undefined && userPassword.length > 0) {
                const hashUser = await bcrypt.hash(userPassword, Globals.saltRounds);
                freeformData.password_pane = hashUser;
            }
            if (adminPassword != null && adminPassword != undefined && adminPassword.length > 0) {
                const hashAdmin = await bcrypt.hash(adminPassword, Globals.saltRounds);
                freeformData.password_admin = hashAdmin;
            }
            if(params.mail != null && params.mail.length > 0){
                const domain = checkValidDomain(request);
                freeformData.mail = params.mail.split('@')[0].toLowerCase() + domain;
            }
            
            let newHashcode = createHashPane();
            let newNoPassHash = createNoPasswordHash(newHashcode);
            freeformData.hashcode = newHashcode;
            freeformData.no_pass_hash = newNoPassHash;
            if (freeformData.background_image)
                freeformData.background_image = freeformData.background_image.replace('/api/file/' + hashcode, '/api/file/' + newHashcode);
            let objects = await freeformObject.query().where('freeform_id', hashcode).where('authorized',true);
            freeformData.headline = params.headline ? params.headline : freeformData.headline;
            if(objects){
                let comments = await freeformComment.query().where('freeform_id',hashcode);
                let lines = await line.query().where('freeform_id',hashcode);
                objects = objects.map(({ freeform_id, body, title, width, height,embedded_length, ...x }) => ({ ...x, freeform_id: newHashcode, body: body!==null && body!==undefined?body.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):body, title: title!==null&&title!==undefined?title.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):title, width: width, height:height, embedded_length:0}));
                comments = comments.map(({ freeform_id, body, url, likes, ...x }) => ({ ...x, freeform_id: newHashcode, body: body!==null && body!==undefined ? body.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):body, url: url!==null && url!==undefined ? url.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):url, likes: params.copyLikes?(likes?likes:0):0 }));
                lines = lines.map(({ freeform_id, ...x }) => ({ ...x, freeform_id: newHashcode }));
                await transaction(freeformComment, line, freeformObject, freeform,  async (freeformComment, line, freeformObject, freeform)=>{
                    await freeform.query().insert(freeformData);
                    if(objects.length>0)
                        await freeformObject.query().insert(objects);
                    if(comments.length>0)
                        await freeformComment.query().insert(comments);
                    if(lines.length>0)
                        await line.query().insert(lines);   
                });
                let oldPath = 'uploads/' + hashcode;
                let newPath = 'uploads/' + newHashcode;
                await ensureDir(oldPath);
                await mkdirs(newPath);
                await copy(oldPath, newPath);
                if(isSameUser)
                    return { success: true, hashcode: newHashcode };                 
                else{
                    return sendMailForDifferentUser(request,newHashcode,"freeform",false);
                }
            }
        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}

export default function(fastify, options, done){
    fastify.post('/update/', async(request,reply)=>{
        let params = request.body;
        return updatePaneConfiguration(freeform, params)
    })

    fastify.post('/update/background/', async(request,reply)=>{
        let file = await request.body.file;
        const body = Object.fromEntries(
            Object.keys(request.body).map((key) => [key, request.body[key].value])
        );
        return uploadBackground(freeform, body, file);
    })

    fastify.post('/copy/', async(request,reply)=>{
        let params = request.body;
        return copyPane(params, params.hashcode, true, request)
    })

    fastify.post('/copy/different/', async(request,reply)=>{
        return copyDifferentUser(request, copyPane);
    })

    fastify.get('/share/:id', async(request,reply)=>{
        const id = request.params.id;
        return getCurrentShare(id, request);
    })

    fastify.post('/share/', async(request,reply)=>{
        return sharePane(request, 'Freeform', freeform);
    })

    fastify.delete('/share/', async(request,reply)=>{
        return unsharePane(request);
    })

    fastify.post('/favorite/:id', async(request,reply)=>{
        return addPaneToFavorites(request);
    })

    fastify.delete('/favorite/:id', async(request,reply)=>{
        return removePaneFromFavorites(request);
    })

    done()
}