// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { hash } from "bcrypt";
import adminPanel from "../../database/models/adminPanel.mjs";
import freeform from "../../database/models/freeform.mjs";
import registration from "../../database/models/registration.mjs";
import schedule from "../../database/models/schedule.mjs";
import timeline from "../../database/models/timeline.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import Globals from "../../lib/globalvars.mjs";
import { createPaneMail } from "../../lib/mail.mjs";
import { prepareRegistration } from "../../lib/registration.mjs";
import { checkIfAllowed, checkValidDomain, createHashPane, createNoPasswordHash, hashMail } from "../../lib/utils.mjs";

export default function(fastify, options, done){
    fastify.post('/', async(request,reply)=>{
        let token = request.headers.token;
        try{
            if(token){
                let params = request.body;
                const session = await sessionStore.query().where('session_id',token).first();
                if(session && session.mail === params.mail){
                    let type = request.body.type.charAt(0).toUpperCase() + request.body.type.slice(1);
                    let hashAdmin = "";
                    if(params.aPassword && params.aPassword.length>=8)
                        hashAdmin = await hash(params.aPassword, Globals.saltRounds);
                    const hashUser = await hash(params.uPassword, Globals.saltRounds);
                    let hashcode = params.hashcode;
                    let isCopy = true;
                    if(!params.hashcode){
                        hashcode = createHashPane();
                        isCopy = false;
                    }
                    const noPassHash = createNoPasswordHash(hashcode);
                    const domain = checkValidDomain(request);
                    //const hashedMail = hashMail(params.mail.toLowerCase().split('@')[0] + domain);
                    let object = {
                        hashcode: hashcode,
                        password_admin: hashAdmin,
                        password_pane: hashUser,
                        like_symbol: params.likeIcon,
                        shape: params.elementShape,
                        shape_color: params.elementColor,
                        comment_color: params.commentColor,
                        comments: params.allowComments,
                        embeddeds: params.allowEmbeddeds,
                        likes: params.allowLikes,
                        font_size: params.fontSize,      
                        background_color: params.backgroundColor,
                        headline: params.headline,
                        subheadline: params.subheadline,
                        mail: params.mail.toLowerCase().split('@')[0] + domain,
                        dnr: params.accesscode,
                        no_pass_hash: noPassHash,
                        font: params.font
                    };
                    if (type === "Freeform") {
                        object.circle = true;
                        object.arrow = params.arrowEnding;
                        object.line_style = params.lineStyle;
                        object.line_color = params.lineColor;
                        object.line_text = params.lineText;
                        object.line_size = Number(params.lineSize);
                        object.unlocked = JSON.stringify({ delete: true, move: true, all: true, commentable: true });
                        try{
                            if(isCopy){
                                await freeform.query().update(object).where('hashcode',object.hashcode);
                            }else{
                                await freeform.query().insert(object);
                            }
                            //await adminPanel.query().insert({mail: object.mail, dashboard: hashedMail});
                            //return createPaneMail(object.mail, object.hashcode, ()=>{return {sucess: true}});
                            return {success: true, hashcode: object.hashcode};
                        }catch(error){
                            console.error(error);
                        }
                    } else if (type === "Schedule" || type === "Stream") {
                        object.one_column = type ==='Stream';
                        object.column_width = type ==='Stream'?1000:Number(params.columnWidth);
                        object.add_position = params.columnAddPosition;
                        object.unlocked = JSON.stringify({ delete: true, move: true, all: true, editHeader: true, commentable: true });
                        try{
                            if(isCopy){
                                await schedule.query().update(object).where('hashcode',object.hashcode);
                            }else{
                                await schedule.query().insert(object);
                            }
                            //await adminPanel.query().insert({mail: object.mail, dashboard: hashedMail});
                            //return createPaneMail(object.mail, object.hashcode, ()=>{return {sucess: true}});
                            return {success: true, hashcode: object.hashcode};
                        }catch(error){
                            console.error(error);
                        }
                    } else if(type === "Timeline"){
                        object.unlocked = JSON.stringify({ delete: true, move: true, all: true, commentable: true });
                        object.element_width = Number(params.elementWidth);
                        object.line_size = Number(params.lineSize);
                        object.line_color = params.lineColor;
                        try{
                            if(isCopy){
                                await timeline.query().update(object).where('hashcode',object.hashcode);
                            }else{
                                await timeline.query().insert(object);
                            }
                            return {success: true, hashcode: object.hashcode};
                        }catch(error){
                            console.error(error);
                        }
                    }
                }
            }
        }catch(e){
                console.error(e);
        }
        return {success: false};
    })

    done();
}