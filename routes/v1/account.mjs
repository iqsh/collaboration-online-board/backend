// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import bcrypt from 'bcrypt';
import * as jose from 'jose';
import { join } from 'path';
import { URL } from 'url'
import { deleteAsync } from 'del';
import { transaction, raw } from 'objection';
import adminPanel from "../../database/models/adminPanel.mjs";
import allPanes from "../../database/models/allPanes.mjs";
import passwordReset from '../../database/models/passwordReset.mjs';
import registration from "../../database/models/registration.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";

import aiUser from '../../database/models/aiUser.mjs';
import aiUserBackup from '../../database/models/aiUserBackup.mjs';
import aiChatMessageTokenUsage from '../../database/models/aiChatMessageTokenUsage.mjs';
import aiChatMessageSession from '../../database/models/aiChatMessageSession.mjs';
import aiChatShared from '../../database/models/aiChatShared.mjs';
import aiChatMessage from '../../database/models/aiChatMessage.mjs';
import aiChat from '../../database/models/aiChat.mjs';

import Globals from "../../lib/globalvars.mjs";
import { preparePasswordReset, resetAccountPassword, resetAccountPasswordLoggedIn } from '../../lib/passwordReset.mjs';
import { checkParameter, checkValidDomain, hashMail, checkPassword, checkIfAllowed } from "../../lib/utils.mjs";
import { getPanesFromFavorites } from '../../lib/favorites.mjs';
import ai from './ai.mjs';

/**
 * Creates the token for the cookie of the user
 * @param {string} mail Mail address of user
 * @param {string} accesscode Accesscode from instance
 * @param {boolean} canUseAI If the user can use the AI
 * @returns the token
 */
export async function createToken(mail, accesscode, canUseAI) {
    let dateNight = new Date();
    if (dateNight.getUTCHours() > 2)
        dateNight = new Date(new Date(dateNight.setDate(dateNight.getDate() + 1)).setUTCHours(2)).setUTCMinutes(0);
    try {
        const jwt = await new jose.UnsecuredJWT({ 'mail': mail, 'accesscode': accesscode, 'canUseAI': canUseAI })
            .setIssuedAt()
            .setExpirationTime(dateNight)
            .encode();
        await sessionStore.query().insert({ mail: mail, expire: dateNight.toString(), session_id: jwt });
        return jwt;
    } catch (error) {
        console.error(error);
        return null;
    }
}

export default function (fastify, options, done) {
    fastify.post('/', async (request, reply) => {
        let mailUsername = request.body.mailUsername;
        let mailDomain = request.body.mailDomain;
        let password = request.body.password;
        let passwordConfirm = request.body.passwordConfirm;
        let id = request.body.id;
        try {
            if (checkValidDomain(request) && checkParameter(mailUsername) && checkParameter(id)
                && checkPassword(password) && password === passwordConfirm) {
                try {
                    let mail = mailUsername + mailDomain;
                    let dashboardHash = hashMail(mail);
                    let passwordHash = bcrypt.hashSync(password, Globals.saltRounds);
                    let registrationQuery = await registration.query().where('code', id).first();
                    if (registrationQuery && registrationQuery.dnr) {
                        const insertUserResult = await transaction(adminPanel, aiUserBackup, registration, aiUser, async (adminPanel, aiUserBackup, registration, aiUser)=>{
                            let canUseAI = false;
                            await adminPanel.query().insert({ mail: mail, password: passwordHash, dashboard: dashboardHash, dnr: registrationQuery.dnr });
                            const date = new Date();
                            let dateFormated = date.toISOString().substring(0, 7);
                            const aiBackup = await aiUserBackup.query().where('mail', mail).andWhere('time', dateFormated).first();
                            if (aiBackup) {
                                await aiUser.query().insert({ mail: mail, token_used: aiBackup.token_used, token_quota: Number(aiBackup.token_quota), tokens_per_model: aiBackup.tokens_per_model });
                                await adminPanel.query().patch({ ai: true }).where('mail', mail);
                                canUseAI = true;
                            }
                            await registration.query().delete().where('code', id);
                            return { success: true, canUseAI };
                        });
                        if(insertUserResult.success){
                            const jwt = await createToken(mail, registrationQuery.dnr, insertUserResult.canUseAI);
                            if (jwt)
                                return { success: true, token: jwt };
                        }
                    }
                } catch (error) {
                    console.error(error);
                }
            }
        } catch (e) {
            console.error(e);
        }
        return { success: false }
    });

    fastify.post('/login', async (request, reply) => {
        let password = request.body.password;
        let mail = request.body.mail;
        if (checkParameter(password) && checkParameter(mail)) {
            try {
                let account = await adminPanel.query().where('mail', mail).first();
                if (account) {
                    const match = await bcrypt.compare(password, account.password);
                    const token = await createToken(mail, account.dnr, account.ai);
                    if (match && token) {
                        return { success: true, token: token };
                    }
                }
            } catch (error) {
                console.error(error);
            }
        }
        return { success: false };
    });

    fastify.post('/checkmail', async (request, reply) => {
        let mail = request.body.mail;
        if (checkParameter(mail)) {
            try {
                let account = await adminPanel.query().where('mail', mail).first();
                if (account && account.password) {
                    return { success: true };
                }
            } catch (error) {
                console.error(error);
            }
        }
        return { success: false };
    });

    fastify.post('/profile', async (request, reply) => {
        let accesscode = request.body.accesscode;
        let token = request.headers.token;
        if (accesscode && checkIfAllowed(request) && token) {
            try {
                let session = await sessionStore.query().where('session_id', token).first();
                if (session && session.expire > +new Date()) {
                    await adminPanel.query().update({ dnr: accesscode, mail: session.mail }).where('mail', session.mail);
                    return { success: true };
                }
            } catch (error) {
                console.error(error);
            }
        }
        return { success: false };
    });

    fastify.get('/password-reset/:id', async (request, reply) => {
        let id = request.params.id;
        if (id) {
            let response = await passwordReset.query().where('code', id).first();
            if (response) {
                try {
                    if (new Date(response.expire) >= new Date()) {
                        return { success: true }
                    } else {
                        await passwordReset.query().delete().where('mail', response.mail);
                    }
                }
                catch (error) {
                    console.error(error);
                }
            }
        }
        return { success: false };
    });

    fastify.post('/password-reset-request', async (request, reply) => {
        if (request.body.mail)
            return await preparePasswordReset(request.body.mail.toLowerCase());
        else
            return { success: false };
    });

    fastify.post('/password-reset', async (request, reply) => {
        return await resetAccountPasswordLoggedIn(request);
    });

    fastify.post('/password-reset/:id', async (request, reply) => {
        return await resetAccountPassword(request);
    });

    fastify.delete('/logout', async (request, reply) => {
        let token = request.headers.token;
        if (token) {
            await sessionStore.query().delete().where('session_id', token);
        }
        return { success: true };
    });

    fastify.get('/favorite', async (request, reply) => {
        if (request.headers.token) {
            try {
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                if (session) {
                    const favoritePanes = await getPanesFromFavorites(session.mail);
                    return { success: true, favorites: favoritePanes };
                }
            } catch (error) {
                console.error(error);
            }
        }
        return { success: false };
    });

    fastify.delete('/', async (request, reply) => {
        let token = request.headers.token;
        let password = request.body?.password;
        if (checkParameter(password) && token) {
            let session = await sessionStore.query().where('session_id', token).first();
            if (session && session.expire > +new Date()) {
                let account = await adminPanel.query().where('mail', session.mail).first();
                const match = await bcrypt.compare(password, account.password);
                if (match) {
                    const allBoards = await allPanes.query().where('mail', session.mail);
                    const __dirname = new URL('.', import.meta.url).pathname;
                    allBoards.forEach((board) => {
                        const dir = join(__dirname, "..", "..", "uploads", board.hash_pane);
                        deleteAsync(dir);
                    });
                    const user = await aiUser.query().where('mail', session.mail).first();
                    if (user) {
                        try {
                            await transaction(aiChatMessageTokenUsage, aiUser, aiChat, aiUserBackup, aiChatMessageSession, aiChatMessage, aiChatShared, async (aiChatMessageTokenUsage, aiUser, aiChat, aiUserBackup, aiChatMessageSession, aiChatMessage, aiChatShared) => {
                                const messagesTokens = await aiChatMessageTokenUsage.query().where('mail', session.mail);
                                const sum = messagesTokens.reduce((a, b) => a + Number(b.token), 0);
                                const user = await aiUser.query().where('mail', session.mail).first();
                                const tokensPerModel = await aiChatMessageTokenUsage.query().where('mail', session.mail).select('mail', 'model', raw(`SUM(token::real) as total_tokens`)).groupBy(['mail', 'model']);
                                const mergedObject = user.tokens_per_model || {};
                                for (const t of tokensPerModel) {
                                    if (!mergedObject[t.model])
                                        mergedObject[t.model] = t.total_tokens;
                                    else
                                        mergedObject[t.model] += t.total_tokens;
                                }
                                if (user) {
                                    await aiUser.query().patch({ token_used: String(Math.ceil(Number(Number(user.token_used) + Number(sum)))), tokens_per_model: mergedObject }).where('mail', session.mail);
                                }
                                for (const t of messagesTokens) {
                                    await aiChatMessageTokenUsage.query().where('uuid', t.uuid).delete();
                                }
                                const tokens = await aiUser.query().select('mail', raw('token_used::text'), 'token_quota', raw('TO_CHAR(DATE_TRUNC(\'month\', CURRENT_DATE), \'YYYY-MM\') AS time'), 'tokens_per_model').where('mail', session.mail).first();
                                await aiUserBackup.query().insert(tokens);
                                for (const chat of aiChats) {
                                    await aiChatMessage.query().delete().where('chat_uuid', chat.uuid);
                                    await aiChatMessageSession.query().delete().where('chat_uuid', chat.uuid);
                                    await aiChatShared.query().delete().where('chat_uuid', chat.uuid);
                                    const __dirname = new URL('.', import.meta.url).pathname;
                                    const dir = join(__dirname, "..", "..", "uploads", "chats", chat.uuid);
                                    deleteAsync(dir);
                                    await aiChat.query().where('uuid', chat.uuid).delete();
                                }
                                await aiUser.query().where('mail', session.mail).delete();
                            })
                        } catch (error) {
                            console.error(error);
                        }

                    }
                    await adminPanel.query().delete().where('mail', session.mail);
                    //await favorites.query().delete().where().orWhere();
                    await sessionStore.query().delete().where('mail', session.mail);
                    return { success: true };
                }
            }
        }
        return { success: false };
    });

    done();
}