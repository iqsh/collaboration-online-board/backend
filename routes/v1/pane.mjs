// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import allPanes from "../../database/models/allPanes.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import schedule from "../../database/models/schedule.mjs";
import freeform from "../../database/models/freeform.mjs";
import timeline from "../../database/models/timeline.mjs";

export default function(fastify, options, done){

    fastify.get('/:id', async(request,reply)=>{
        let token = request.headers.token;
        const result = await allPanes.query().where('hash_pane', request.params.id).first();
        if(result){
            if(token){
                const session = await sessionStore.query().where('session_id',token).first();
                if(session && session.mail === result.mail){
                    return { success: true, type: result.type, noPassHash: result.no_pass_hash, adminMail: result.mail };
                }
            }else{
                return { success: true };
            }
        }
        return { success: false };
    });
    
    //checks if password for moderator is set
    fastify.get('/:id/moderation', async(request,reply)=>{
        const query = schedule.query().select('hashcode', 'password_admin').union(freeform.query().select('hashcode', 'password_admin').where('hashcode', request.params.id)).union(timeline.query().select('hashcode', 'password_admin').where('hashcode', request.params.id)).where('hashcode', request.params.id).first();
        const result = await query;
        if(result && result.password_admin){
            return { success: true };
        }
        return { success: false };
    });

    done();
};
