// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import registration from "../../database/models/registration.mjs";
import { prepareRegistration } from "../../lib/registration.mjs";
import { checkIfAllowed } from "../../lib/utils.mjs";

export default function(fastify, options, done){
    fastify.post('/', async(request,reply)=>{
        let params = request.body;
        if(await checkIfAllowed(request)){
            return prepareRegistration(request);
        }else{
            return {success: false};
        }
    })

    fastify.get('/:id', async(request,reply)=>{
        let id = request.params.id.toLowerCase();
        let data = await registration.query().where('code',id).orderBy('expire', 'DESC').first();
        if(data){
            try{
                //await registration.query().delete().where('code',id);
                if (new Date() <= new Date(data.expire)) {
                    return {
                        status:true,
                        mail: data.mail,
                        accesscode: data.dnr,
                        hashcode: data.hashcode,
                        type: data.type
                    }
                }else{
                    await registration.query().delete().where('code',id);
                }
            }catch(error){
                console.error(error);
            }
        }
        return {status: false};
    })

    done();
}

