// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { ensureDir, mkdirs, copy } from "fs-extra";
import { transaction } from "objection";
import bcrypt from 'bcrypt';
import schedule from "../../database/models/schedule.mjs";
import schedulePost from "../../database/models/schedulePost.mjs";
import scheduleComment from "../../database/models/scheduleComment.mjs";
import scheduleObject from "../../database/models/scheduleObject.mjs";
import Globals from "../../lib/globalvars.mjs";
import { updatePaneConfiguration, uploadBackground, checkValidDomain, createHashPane, createNoPasswordHash } from "../../lib/utils.mjs";
import { createPaneMail as sendMailForSameUser } from "../../lib/mail.mjs";
import { prepareRegistration as sendMailForDifferentUser } from "../../lib/registration.mjs";
import { copyDifferentUser, getCurrentShare, sharePane, unsharePane } from "../../lib/share.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import { addPaneToFavorites, removePaneFromFavorites } from "../../lib/favorites.mjs";

/**
 * Copies an existing pane
 * @param {*} params Parameters
 * @param {*} hashcode Hashcode of pane
 * @param {boolean} isSameUser If is for the same user or not
 * @returns success: true || false
 */
async function copyPane(params, hashcode, isSameUser, request) {
    try{
        let scheduleData = await schedule.query().where('hashcode', hashcode).first();
        let token = request.headers.token;
        let session = null;
        if(token && isSameUser){
            session = await sessionStore.query().where('session_id',token).first();
        }
        if(scheduleData && ((isSameUser && session && session.mail === scheduleData.mail) || !isSameUser)){
            let userPassword = params.userPassword;
            let adminPassword = params.adminPassword;
            if (userPassword != null && userPassword != undefined && userPassword.length > 0) {
                const hashUser = await bcrypt.hash(userPassword, Globals.saltRounds);
                scheduleData.password_pane = hashUser;
            }
            if (adminPassword != null && adminPassword != undefined && adminPassword.length > 0) {
                const hashAdmin = await bcrypt.hash(adminPassword, Globals.saltRounds);
                scheduleData.password_admin = hashAdmin;
            }
            if(params.mail != null && params.mail.length > 0){
                const domain = checkValidDomain(request);
                scheduleData.mail = params.mail.split('@')[0].toLowerCase() + domain;
            }
            let newHashcode = createHashPane();
            let newNoPassHash = createNoPasswordHash(newHashcode);
            scheduleData.hashcode = newHashcode;
            scheduleData.no_pass_hash = newNoPassHash;
            if (scheduleData.background_image)
                scheduleData.background_image = scheduleData.background_image.replace('/api/file/' + hashcode, '/api/file/' + newHashcode);
            if(scheduleData.column_width)
                scheduleData.column_width = Number(scheduleData.column_width);
            scheduleData.headline = params.headline ? params.headline : scheduleData.headline;
            let objects = await scheduleObject.query().where('schedule_id', hashcode);
            if(objects){
                let comments = await scheduleComment.query().where('schedule_id',hashcode);
                let posts = await schedulePost.query().where('schedule_id',hashcode).where('authorized',true);
                objects = objects.map(({ schedule_id, ...x }) => ({ ...x, schedule_id: newHashcode }));
                posts = posts.map(({ schedule_id, body, url, likes, message, ...x }) => ({ ...x, schedule_id: newHashcode, body: body!==null && body!==undefined ? body.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):body, likes: 0, url: url!==null && url!==undefined ? url.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode):url, message: message!==null && message!==undefined ? message.replace('/api/file/' + hashcode, '/api/file/' + newHashcode).replace('/api/file/h5p/' + hashcode, '/api/file/h5p/' + newHashcode): message, likes: params.copyLikes?likes: 0 }));
                comments = comments.map(({ schedule_id, likes, ...x }) => ({ ...x, schedule_id: newHashcode, likes: params.copyLikes?likes: 0 }));   
                await transaction(schedule, scheduleObject, schedulePost, scheduleComment,  async (schedule, scheduleObject, schedulePost, scheduleComment)=>{
                    await schedule.query().insert(scheduleData);
                    if(objects.length>0)
                        await scheduleObject.query().insert(objects);
                    if(comments.length>0)
                        await scheduleComment.query().insert(comments);
                    if(posts.length>0)
                        await schedulePost.query().insert(posts);   
                });
                let oldPath = 'uploads/' + hashcode;
                let newPath = 'uploads/' + newHashcode;
                await ensureDir(oldPath);
                await mkdirs(newPath);
                await copy(oldPath, newPath);
                if(isSameUser)
                    return { success: true, hashcode: newHashcode };                  
                else
                    return sendMailForDifferentUser(request,newHashcode,"schedule",false);
            }
        }
    }catch(error){
        console.error(error);
    }
    return { success: false };
}


export default function(fastify, options, done){
    fastify.post('/update/', async(request,reply)=>{
        let params = request.body;
        return updatePaneConfiguration(schedule, params)
    })

    fastify.post('/update/background/', async(request,reply)=>{
        let file = await request.body.file;
        const body = Object.fromEntries(
            Object.keys(request.body).map((key) => [key, request.body[key].value])
        );
        return uploadBackground(schedule, body, file);
    })

    fastify.post('/copy/', async(request,reply)=>{
        let params = request.body;
        return copyPane(params, params.hashcode, true, request)
    })

    fastify.post('/copy/different/', async(request,reply)=>{
        return copyDifferentUser(request, copyPane);
    })

    fastify.get('/share/:id', async(request,reply)=>{
        const id = request.params.id;
        return getCurrentShare(id, request);
    })

    fastify.post('/share/', async(request,reply)=>{
        return sharePane(request, 'Schedule', schedule);
    })

    fastify.delete('/share/', async(request,reply)=>{
        return unsharePane(request);
    })

    fastify.post('/favorite/:id', async(request,reply)=>{
        return addPaneToFavorites(request);
    })

    fastify.delete('/favorite/:id', async(request,reply)=>{
        return removePaneFromFavorites(request);
    })

    done()
}