// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import allPanes from "../../database/models/allPanes.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import folder from "../../database/models/folder.mjs";
import folderPane from "../../database/models/folderPane.mjs";
import favorites from "../../database/models/favorites.mjs";
import { randomUUID } from "crypto";
import { join } from 'path';
import { URL } from 'url'
import { deleteAsync } from 'del';
import { addPaneToFavorites, getPanesFromFavorites, removePaneFromFavorites } from "../../lib/favorites.mjs";
import { raw } from "objection";

export default function(fastify, options, done){
    fastify.get('/', async (request, reply) => {
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                if(session){
                    const data = await allPanes.query()
                        .select('hash_pane', 'type', 'view_all_boards.mail', 'headline', 'subheadline', 'background_image', 'background_color', 'last_update', 'folder').distinct().select(
                            raw('CASE WHEN view_all_boards.mail = ANY (array_agg(view_favourites.mail)::text[]) THEN view_all_boards.mail else NULL END as origin_mail')
                        )
                    .leftJoin('view_favourites','view_all_boards.hash_pane','=','view_favourites.hashcode')
                        .where('view_all_boards.mail', session.mail).groupBy('hash_pane', 'type', 'view_all_boards.mail', 'headline', 'subheadline', 'background_image', 'background_color', 'last_update', 'folder');
                    const folders = await folder.query().where('user', session.mail);
                    const favoritePanes = await getPanesFromFavorites(session.mail);
                    return { success: true, data: {panes: data, folders: folders, favorites: favoritePanes } };
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.post('/folder', async(request, reply)=>{
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                let data = request.body;
                if(session && data){
                    const id = randomUUID();
                    data.id = id;
                    data.user = session.mail;
                    await folder.query().insert(data);
                    return { success: true };
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.post('/folder/:id', async(request, reply)=>{
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                let data = request.body;
                let id = request.params.id;
                if(session && data && id){
                    const response = await folder.query().where('id',id).where('user',session.mail).first();
                    if(response){
                        const folderId = randomUUID();
                        data.id = folderId;
                        data.user = session.mail;
                        await folder.query().insert(data);
                        return { success: true };
                    }
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.put('/folder/:id', async(request, reply)=>{
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                let data = request.body;
                let id = request.params.id;
                if(session && data && id){
                    const response = await folder.query().where('id',id).where('user',session.mail).first();
                    if(response){
                        await folder.query().update(data).where('id',id).where('user',session.mail);
                        return { success: true };
                    }
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.put('/pane/move/:id', async(request, reply)=>{
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                let data = request.body;
                let id = request.params.id;
                if(session && data && id){
                    let response;
                    if(data.folder!==""){
                        response = await folder.query().where('id',data.folder).where('user',session.mail).first();
                    }else{
                        response = false;
                    }
                    if(response){
                        const folderData = {hashcode: id, id:data.folder};
                        await folderPane.query().insert(folderData);
                        return { success: true };
                    }
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.put('/pane/move', async(request, reply)=>{
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                let data = request.body;
                if(session && data){
                    let response;
                    if(data.folder!==""){
                        response = await folder.query().where('id',data.folder).where('user',session.mail).first();
                    }else{
                        const folderData = data.panes.map((pane)=>{ return {hashcode: pane,id: ""};});
                        await folderPane.query().insert(folderData);
                        return { success: true };
                    }
                    if(response){
                        const folderData = data.panes.map((pane)=>{ return {hashcode: pane, id:data.folder};});
                        await folderPane.query().insert(folderData);
                        return { success: true };
                    }
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.delete('/folder/:id', async(request, reply)=>{
        if(request.headers.token){
            try{
                let session = await sessionStore.query().where('session_id', request.headers.token).first();
                let id = request.params.id;
                if(session && id){
                    try{
                        if(request.body.deletePanes){
                            const response = await folder.transaction(async trx =>{
                                const folders = await folder.query().where(function(){this.where('parent',id).orWhere('id',id)}).andWhere('user',session.mail);
                                const folderIds = folders.map((item)=>item.id);
                                const panes = (await folderPane.query(trx).where('id', 'in', folderIds)).map((item)=>item.hashcode);
                                await folderPane.query(trx).where('id', 'in', folderIds).delete();
                                await folder.query(trx).where('id','in',folderIds).delete();
                                return panes;
                            });
                            
                            response.forEach((pane)=>{
                                const __dirname = new URL('.', import.meta.url).pathname;
                                const dir = join(__dirname, "..","..", "uploads", pane);
                                deleteAsync(dir);
                            })
                        }
                        else{
                            await folder.query().delete().where(function(){this.where('parent',id).orWhere('id',id)}).andWhere('user',session.mail);
                        }
                        return { success: true };
                    }
                    catch(error){
                        console.error(error);
                    }
                }
            }catch(error){
                console.error(error);
            }
        }
        return { success: false };
    })

    fastify.post('/favorite/:id', async(request,reply)=>{
        return addPaneToFavorites(request);
    })

    fastify.delete('/favorite/:id', async(request,reply)=>{
        return removePaneFromFavorites(request);
    })

    fastify.get('/:id', async (request, reply)=>{
        let id = request.params.id;
        try{
            let data = await allPanes.query()
            .select('hash_pane', 'type','mail','headline','subheadline','background_image','background_color','last_update')
            .where('hash_dash',id);
            return { success: true, data: data };
        }catch(error){
            console.error(error);
            return { success: false };
        }
    })

    done()
}