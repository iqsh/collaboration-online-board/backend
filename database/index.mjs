// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import objection from 'objection';
const { ValidationError,
    NotFoundError,
    DBError,
    ConstraintViolationError,
    UniqueViolationError,
    NotNullViolationError,
    ForeignKeyViolationError,
    CheckViolationError,
    DataError } = objection;
import { Model } from 'objection';
import Knex  from 'knex';
  
// Initialize knex.
const knex = Knex({
    client: 'pg',
    useNullAsDefault: true,
    connection: {
        host: process.env.BACKEND_DB_HOST,
        port: process.env.BACKEND_DB_PORT,
        user: process.env.BACKEND_DB_USER,
        password: process.env.BACKEND_DB_PASSWORD,
        database: process.env.BACKEND_DB_DATABASE
    }
});
  
  
function errorHandler(err, reply) {
    console.error(err.message);
    if (err instanceof ValidationError) {
      switch (err.type) {
        case 'ModelValidation':
          reply.code(400).send({
            message: 'ModelValidation',
            type: 'ModelValidation'
          });
          break;
        case 'RelationExpression':
          reply.code(400).send({
            message: 'RelationExpression',
            type: 'RelationExpression'
          });
          break;
        case 'UnallowedRelation':
          reply.code(400).send({
            message: 'UnallowedRelation',
            type: 'UnallowedRelation'
          });
          break;
        case 'InvalidGraph':
          reply.code(400).send({
            message: 'InvalidGraph',
            type: 'InvalidGraph'
          });
          break;
        default:
          reply.code(400).send({
            message: 'UnknownValidationError',
            type: 'UnknownValidationError'
          });
          break;
      }
    } else if (err instanceof NotFoundError) {
      reply.code(404).send({
        message: 'NotFound',
        type: 'NotFound',
        });
    } else if (err instanceof UniqueViolationError) {
      reply.code(409).send({
        message: 'UniqueViolation',
        type: 'UniqueViolation'
  
      });
    } else if (err instanceof NotNullViolationError) {
      reply.code(400).send({
        message: 'NotNullViolation',
        type: 'NotNullViolation'
  
      });
    } else if (err instanceof ForeignKeyViolationError) {
      reply.code(409).send({
        message: 'ForeignKeyViolation',
        type: 'ForeignKeyViolation'
      });
    } else if (err instanceof CheckViolationError) {
      reply.code(400).send({
        message: 'CheckViolation',
        type: 'CheckViolation'
      });
    } else if (err instanceof DataError) {
      reply.code(400).send({
        message:  'InvalidData',
        type: 'InvalidData'
      });
    } else if (err instanceof DBError) {
      reply.code(500).send({
        message: 'UnknownDatabaseError',
        type: 'UnknownDatabaseError'
      });
    } else {
      reply.code(500).send({
        message:'UnknownError',
        type: 'UnknownError'
      });
    }
}
  
Model.knex(knex);
export default Model;
export { errorHandler, knex };