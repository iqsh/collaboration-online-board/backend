// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class freeformSize extends Model{
    static get tableName(){
        return 'view_freeform_size';
    }

    static get idColumn() {
        return ['hashcode','no_pass_hash'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['hashcode','no_pass_hash'],
        
            properties: {
                hashcode: { type: 'string'},
                no_pass_hash: { type: 'string'},
                max_height: { type: 'float'},
                max_width: { type: 'float'},
            }
        };
    }

}
export default freeformSize;