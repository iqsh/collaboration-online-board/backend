// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiUser extends Model{
    static get tableName(){
        return 'ai_user';
    }

    static get idColumn() {
        return 'mail';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['mail', 'token_used'],
            properties: {
                mail: { type: 'string'},
                token_used: { type: 'string'},
                token_quota: { type: 'integer'},
                tokens_per_model: { type: 'object'}
            }
        };
    }

}
export default aiUser;