// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiUserBackup extends Model{
    static get tableName(){
        return 'ai_user_backup';
    }

    static get idColumn() {
        return 'mail';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['mail', 'time'],
            properties: {
                mail: { type: 'string'},
                token_used: { type: 'string'},
                token_quota: { type: 'integer'},
                models: { type: 'object'},
                time: { type: 'string'}
            }
        };
    }

}
export default aiUserBackup;