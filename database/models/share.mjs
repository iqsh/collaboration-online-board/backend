// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class share extends Model{
    static get tableName(){
        return 'view_share';
    }

    static get idColumn() {
        return 'pane_hash';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['pane_hash'],
        
            properties: {
                pane_hash: { type: 'string'},
                ende: { type: 'string'},
                prev_hash: { type: 'string'},
                likes: { type: 'boolean'},
                comments: { type: 'boolean'},
                typname: { type: 'string'}
            }
        };
    }

}
export default share;