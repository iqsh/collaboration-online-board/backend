// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class dashboard extends Model{
    static get tableName(){
        return 'view_dashboard';
    }

    static get idColumn() {
        return 'timestamp';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['timestamp'],
        
            properties: {
                timestamp: { type: 'string'},
                cpu: { type: 'string'},
                memory: { type: 'string'},
                heap: { type: 'string'},
                response_time: { type: ['string', 'null']},
                requests_per_second: { type: ['string', 'null']},
                user_count: { type: 'integer'},
                pane_count: { type: 'integer'}
            }
        };
    }

}
export default dashboard;