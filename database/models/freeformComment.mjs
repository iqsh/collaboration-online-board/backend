// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class freeformComment extends Model{
    static get tableName(){
        return 'view_freeformembedded';
    }

    static get idColumn() {
        return ['freeform_id','parent_id','id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['freeform_id','parent_id','id'],
        
            properties: {
                freeform_id: { type: 'string'},
                parent_id: { type: 'string'},
                id: { type: 'string'},
                body: { type: ['string', 'null']},
                url: { type: 'string'},
                type: { type: ['string', 'null']},
                shape: { type: ['string','null']},
                color: { type: ['string','null']},
                likes: { type: 'integer'},
                info: { type: ['string', 'null']},
                visible: {type:['boolean', 'null']},
            }
        };
    }

}
export default freeformComment;