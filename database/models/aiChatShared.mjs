// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { create } from 'got';
import Model from '../index.mjs';

class aiChatShared extends Model{
    static get tableName(){
        return 'ai_chat_shared';
    }

    static get idColumn() {
        return 'chat_uuid';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['chat_uuid'],
        
            properties: {
                chat_uuid: { type: 'string'},
                valid_until: { type: 'string'},
                limitation: { type: 'integer'},
                created_at: { type: 'string'},
            }
        };
    }

}
export default aiChatShared;