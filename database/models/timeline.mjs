// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class timeline extends Model{
    static get tableName(){
        return 'view_timelines';
    }

    static get idColumn() {
        return 'hashcode';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['hashcode'],
        
            properties: {
                hashcode: { type: 'string'},
                like_symbol: { type: 'string'},
                shape: { type: 'string'},
                shape_color: { type: 'string'},
                comment_color: { type: 'string'},
                comments: { type: 'boolean'},
                embeddeds: { type: 'boolean'},
                likes: { type: 'boolean'},
                font_size: { type: 'string'},
                element_width: { type: 'integer'},
                background_color: { type: 'string'},
                password_admin: { type: 'string'},
                password_pane: { type: 'string'},
                count:{ type: ['integer','null']},
                unlocked: { type: 'string'},
                headline: { type: 'string'},
                subheadline: { type: 'string'},
                background_image: { type: ['string','null']},
                public_key: { type: ['string','null']},
                private_key: { type: ['string','null']},
                iv: { type: ['string','null']},
                mail: { type: 'string'},
                dnr: { type: 'string'},
                no_pass_hash: { type: 'string'},
                font: { type: 'string'},
                line_color: { type: 'string'},
                line_size: { type: 'integer'},
                authorize: {type:['boolean', 'null']}
            }
        };
    }

}
export default timeline;