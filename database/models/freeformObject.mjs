// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class freeformObject extends Model{
    static get tableName(){
        return 'view_freeformobject';
    }

    static get idColumn() {
        return ['freeform_id','id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['freeform_id','id'],
        
            properties: {
                freeform_id: { type: 'string'},
                id: { type: 'string'},
                title: { type: 'string'},
                left: { type: 'string'},
                top: { type: 'string'},
                comment_length: { type: 'integer'},
                shape: { type: ['string','null']},
                color: { type: ['string','null']},
                embedded_length: { type: 'integer'},
                body: { type: ['string','null']},
                info: { type: ['string', 'null']},
                height: { type: ['string', 'null']},
                width: { type: ['string', 'null']},
                visible: {type:['boolean', 'null']},
                authorized: {type:['boolean', 'null']},
            }
        };
    }

}
export default freeformObject;