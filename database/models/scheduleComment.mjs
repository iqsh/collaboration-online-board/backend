// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class scheduleComment extends Model{
    static get tableName(){
        return 'view_schedulecomments';
    }

    static get idColumn() {
        return ['schedule_id','parent_id','id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['schedule_id','parent_id','id'],
        
            properties: {
                schedule_id: { type: 'string'},
                parent_id: { type: 'string'},
                id: { type: 'string'},
                message: { type: 'string'},
                likes: { type: 'integer'},
                shape: { type: ['string', 'null']},
                color: { type: ['string', 'null']},
                body: { type: ['string', 'null']},
                type: { type: ['string', 'null']},
                info: { type: ['string', 'null']},
                visible: {type:['boolean', 'null']},
            }
        };
    }

}
export default scheduleComment;