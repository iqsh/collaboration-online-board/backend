// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiPersonCategory extends Model{
    static get tableName(){
        return 'ai_person_category';
    }

    static get idColumn() {
        return ['person_uuid', 'category'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['person_uuid', 'category'],
        
            properties: {
                person_uuid: { type: 'string'},
                category: { type: 'string'},
            }
        };
    }

}
export default aiPersonCategory;
