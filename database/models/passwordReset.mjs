// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class passwordReset extends Model{
    static get tableName(){
        return 'view_account_password_reset';
    }

    static get idColumn() {
        return ['mail','expire'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['mail','expire'],
        
            properties: {
                mail: { type: 'string'},
                expire: { type: 'string'},
                code: { type: 'string'},
                password: { type: 'string'}
            }
        };
    }

}
export default passwordReset;