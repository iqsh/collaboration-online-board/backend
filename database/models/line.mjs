// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class line extends Model{
    static get tableName(){
        return 'view_line';
    }

    static get idColumn() {
        return ['freeform_id','id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['freeform_id','id'],
        
            properties: {
                freeform_id: { type: 'string'},
                id: { type: 'string'},
                start: { type: 'string'},
                end: { type: 'string'},
                line_text: { type: ['string', 'null']},
                size: { type: ['string','null']},
                dash: { type: 'boolean'},
                color: { type: 'string'}
            }
        };
    }

}
export default line;