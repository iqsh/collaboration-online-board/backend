// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class stats extends Model{
    static get tableName(){
        return 'pilot_oberflaechen';
    }

    static get idColumn() {
        return ['anzahl_oberflaechen','mails','schulen','max_user_overall','max_user_day'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['anzahl_oberflaechen','mails','schulen','max_user_overall','max_user_day'],
        
            properties: {
                anzahl_oberflaechen: { type: 'integer'},
                mails: { type: 'integer'},
                schulen: { type: 'integer'},
                max_user_overall: { type: 'integer'},
                max_user_day: { type: 'integer'}
            }
        };
    }
}

export default stats;