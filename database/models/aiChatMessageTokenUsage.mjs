// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiChatMessageTokenUsage extends Model {
    static get tableName() {
        return 'ai_chat_message_token_usage';
    }

    static get idColumn() {
        return 'uuid';
    }

    static get jsonSchema() {
        return {
            type: 'object',

            properties: {
                uuid: { type: 'string'},
                create_date: { type: 'string' },
                token: { type: 'string' },
                mail: { type: 'string' }
            }
        };
    }

}
export default aiChatMessageTokenUsage;