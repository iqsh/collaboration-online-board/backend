// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class favorites extends Model{
    static get tableName(){
        return 'view_favourites';
    }

    static get idColumn() {
        return ['mail', 'hashcode'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['mail', 'hashcode'],
        
            properties: {
                mail: { type: 'string'},
                hashcode: { type: 'string'},
                origin_mail: { type: 'string'},
            }
        };
    }

}
export default favorites;