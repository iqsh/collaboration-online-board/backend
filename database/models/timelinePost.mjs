// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class timelinePost extends Model{
    static get tableName(){
        return 'view_timelineposts';
    }

    static get idColumn() {
        return ['timeline_id','id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['timeline_id','id'],
        
            properties: {
                timeline_id: { type: 'string'},
                id: { type: 'string'},
                message: { type: 'string'},
                likes: { type: 'integer'},
                type: { type: 'string'},
                body: { type: ['string','null']},
                color: { type: ['string', 'null']},
                shape: { type: ['string', 'null']},
                comment_length: { type: 'integer'},
                info: { type: ['string', 'null']},
                order_id: { type: 'string'},
                visible: {type:['boolean', 'null']},
                authorized: {type:['boolean', 'null']},
            }
        };
    }

}
export default timelinePost;