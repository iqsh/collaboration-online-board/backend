// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class allPanes extends Model{
    static get tableName(){
        return 'view_all_boards';
    }

    static get idColumn() {
        return ['hash_pane','hash_dash', 'mail'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['hash_pane','hash_dash', 'mail'],
        
            properties: {
                hash_pane: { type: 'string'},
                hash_dash: { type: 'string'},
                mail: { type: 'string'},
                type: { type: 'string'},
                headline: { type: 'string'},
                subheadline: { type: 'string'},
                no_pass_hash: { type: 'string'},
                background_image: { type: 'string'},
                background_color: { type: 'string'},
                last_update: { type: 'string'},
                folder:{ type: 'string'},
            }
        };
    }

}
export default allPanes;