// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class folder extends Model{
    static get tableName(){
        return 'view_folder';
    }

    static get idColumn() {
        return ['id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['id'],
        
            properties: {
                id: { type: 'string'},
                name: { type: 'string'},
                color: { type: 'string'},
                parent: { type: 'string'},
                user: { type: 'string'},
            }
        };
    }

}
export default folder;