// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class folder_pane extends Model{
    static get tableName(){
        return 'view_folder_pane';
    }

    static get idColumn() {
        return ['id', 'hashcode'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['id', 'hashcode'],
        
            properties: {
                id: { type: 'string'},
                hashcode: { type: 'string'}
            }
        };
    }

}
export default folder_pane;