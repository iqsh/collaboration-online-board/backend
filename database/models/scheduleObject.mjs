// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class scheduleObject extends Model{
    static get tableName(){
        return 'view_scheduleobjects';
    }

    static get idColumn() {
        return ['schedule_id','id'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['schedule_id','id'],
        
            properties: {
                schedule_id: { type: 'string'},
                id: { type: 'string'},
                title: { type: 'string'},
                comments: { type: 'integer'},
                color: { type: ['string', 'null']},
                shape: { type: ['string', 'null']},
                order_id: { type: 'string'},
                visible: {type:['boolean', 'null']},
            }
        };
    }

}
export default scheduleObject;