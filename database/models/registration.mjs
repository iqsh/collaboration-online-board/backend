// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class registration extends Model{
    static get tableName(){
        return 'registration';
    }

    static get idColumn() {
        return ['mail','expire'];
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['mail','expire'],
        
            properties: {
                mail: { type: 'string'},
                expire: { type: 'string'},
                code: { type: 'string'},
                dnr: { type: 'string'},
                hashcode: { type: 'string'},
                type: { type: 'string'}
            }
        };
    }

}
export default registration;