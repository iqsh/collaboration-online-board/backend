// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiChatMessageSession extends Model {
    static get tableName() {
        return 'ai_chat_message_session';
    }

    static get idColumn() {
        return 'uuid';
    }

    static get jsonSchema() {
        return {
            type: 'object',

            properties: {
                chat_uuid: { type: 'string' },
                uuid: { type: ['string', 'null']},
                message: { type: 'string' },
                meta: { type: 'object' },
                user: { type: 'string' }
            }
        };
    }

}
export default aiChatMessageSession;