// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiPerson extends Model{
    static get tableName(){
        return 'ai_person';
    }

    static get idColumn() {
        return 'uuid';
    }

    static get jsonSchema() {
        return {
            type: 'object',
        
            properties: {
                uuid: { type: 'string'},
                name: { type: 'string'},
                creator: { type: 'string'},
                system_prompt: { type: 'string'},
                avatar: { type: 'string'},
                shared: { type: 'boolean'},
                description: { type: 'string'},
                model: { type: 'string'},
                user_mail: { type: 'string'},
                type: { type: 'string'},
                meta: { type: 'object'},
                create_date: { type: 'string'}
            }
        };
    }

}
export default aiPerson;