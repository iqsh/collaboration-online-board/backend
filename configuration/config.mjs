// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { join } from 'path';
import { URL } from 'url';
const __dirname = new URL('.', import.meta.url).pathname;

const mailAttachments = [{
    filename: 'logo.png',
    path: join(__dirname, "..", "public", "images", "logo.png"),
    cid: 'logo@app'
},{
    filename: 'home_logo.png',
    path: join(__dirname, "..", "public", "images", "home_logo.png"),
    cid: 'logo@og'
}]

export { mailAttachments }