### Deutsch
## Collaborative Online Board - Backend
Dient als RESTful-API und als Datei-Server für die von den Nutzenden hochgeladenen Dateien.
Diese Software nutzt *fastify* als Framework und *Objection.js / Knex.js* für die Objektrelationale Abbildung (ORM).


### System Anforderungen:
* NodeJS 18+ 
* NPM
* Libc6 (or libc6-compat)
* Graphicsmagick
* Ghostscript
* PostgreSQL 14+
* *optional: pgAdmin4 oder DBeaver für die Administration der Datenbank*
* Getestet mit *Ubuntu 20.04 LTS* und *Alpine Linux*

### Installation von den Software-Abhängigkeiten:
Gehen Sie in den *backend*-Ordner und führen Sie `npm install` aus. Außerdem müssen Sie das Paket `knex` global installieren, also `npm install -g knex`. Anschließend führen Sie folgenden Befehl aus: `knex migrate:latest`. Dieser Befehl kann auch vor jedem Start durchgeführt werden, denn es wird nur ausstehende Migrationen durchgeführt.

### Vorbereitung der Datenbank (veraltet ab Version 1.3.0):
Erstellen Sie eine neue Datenbank und laden Sie alle Tabellen, Funktionen und Trigger von *Database_Initial.sql* in ihre Datenbank.

### Ausführen der Anwendung:
Kopieren und benennen Sie die .env.example in .env um und passen Sie die Werte an.  
Zum Ausführen des Servers: `npm run start`

***

### English
## Collaborative Online Board - Backend
Serves as a RESTful-API and file server for the uploaded files of the user.   
The software uses *fastify* as framework and *Objection.js / Knex.js* for the object-relational-mapping (ORM). 


### System Requirements:
* NodeJS 18+ 
* NPM
* Libc6 (or libc6-compat)
* Graphicsmagick
* Ghostscript
* PostgreSQL 14+
* *optional: pgAdmin4 or DBeaver for administration of the database*
* Tested with *Ubuntu 20.04 LTS* and *Alpine Linux*

### Installation of the software dependencies:
Go into the *backend* folder and run `npm install`. You also need to install the `knex` package globally, i.e. `npm install -g knex`. Then run the following command: `knex migrate:latest`. This command can also be run before each startup, because it will only run pending migrations.

### Preparation of the database (deprecated as of version 1.3.0):
Create a new database and load all tables, views, functions and triggers from *Database_Initial.sql* into your database.

### Run application:
Copy and rename .env.example to .env and adapt the values.  
Run Server: `npm run start`

