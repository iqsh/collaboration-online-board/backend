// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import 'dotenv/config'

var Globals = {
    /* general */
    'uploadsPath':typeof process.env.BACKEND_UPLOADS_PATH!==undefined?process.env.BACKEND_UPLOADS_PATH:__dirname,
    'mailDomains':typeof process.env.BACKEND_VALID_MAIL_DOMAINS!==undefined?process.env.BACKEND_VALID_MAIL_DOMAINS:"@domain.com",
    'backendServerAddress':typeof process.env.BACKEND_SERVER_ADRESS!==undefined?process.env.BACKEND_SERVER_ADRESS:"",
    'frontendServerAddress':typeof process.env.FRONTEND_SERVER_ADRESS!==undefined?process.env.FRONTEND_SERVER_ADRESS:"",
    'frontendBasePath':typeof process.env.FRONTEND_BASE_PATH!==undefined?process.env.FRONTEND_BASE_PATH:"",
    'dasboardPassword':typeof process.env.BACKEND_DASHBOARD_PASSWORD!==undefined?process.env.BACKEND_DASHBOARD_PASSWORD:"",
    'sessionDeleteInterval':typeof process.env.BACKEND_SESSION_DELETE_INTERVAL!==undefined?process.env.BACKEND_SESSION_DELETE_INTERVAL:60,
    'fileUploadExtensions':typeof process.env.BACKEND_FILE_UPLOAD_EXTENSIONS!==undefined?process.env.BACKEND_FILE_UPLOAD_EXTENSIONS:"",
    'fileUploadMimeTypes':typeof process.env.BACKEND_FILE_UPLOAD_MIMETYPES!==undefined?process.env.BACKEND_FILE_UPLOAD_MIMETYPES:"",
    'executeCronJobs':typeof process.env.BACKEND_EXECUTE_CRON_JOBS!==undefined?process.env.BACKEND_EXECUTE_CRON_JOBS:true,
    'debug':typeof process.env.BACKEND_DEBUG!==undefined?process.env.BACKEND_DEBUG:false,
    /* passwords */
    'saltRounds':10,
    /* mail configuration */
    'realMail':typeof process.env.BACKEND_REAL_MAIL!==undefined?process.env.BACKEND_REAL_MAIL:false,
    'smtpServer':typeof process.env.BACKEND_STMP_SERVER!==undefined?process.env.BACKEND_STMP_SERVER:"smtp.ethereal.email",
    'mailPort':typeof process.env.BACKEND_MAIL_PORT!==undefined?process.env.BACKEND_MAIL_PORT:587,
    'mailSecure':typeof process.env.BACKEND_MAIL_SECURE!==undefined?process.env.BACKEND_MAIL_SECURE:false,
    'mailUser':typeof process.env.BACKEND_MAIL_USER!==undefined?process.env.BACKEND_MAIL_USER:null,
    'mailPassword':typeof process.env.BACKEND_MAIL_PASS!==undefined?process.env.BACKEND_MAIL_PASS:null,
    'noreplyMail':typeof process.env.BACKEND_NOREPLY_MAIL!==undefined?process.env.BACKEND_NOREPLY_MAIL:'"Noreply" <noreply@domain.com>',
    'activePaneRegistration':typeof process.env.BACKEND_ACTIVE_PANE_REGISTRATION!==undefined?process.env.BACKEND_ACTIVE_PANE_REGISTRATION:false,
    'metaScrapperUserAgent':typeof process.env.BACKEND_USER_AGENT!==undefined?process.env.BACKEND_USER_AGENT:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36",

    /* ai */
    'tokenCalculation':typeof process.env.BACKEND_TOKEN_CALCULATION!==undefined?process.env.BACKEND_TOKEN_CALCULATION:false,
    'tokenCalculationInterval':typeof process.env.BACKEND_TOKEN_CALCULATION_INTERVAL!==undefined?process.env.BACKEND_TOKEN_CALCULATION_INTERVAL:60,
    'defaultTextModel':typeof process.env.BACKEND_DEFAULT_TEXT_MODEL!==undefined?process.env.BACKEND_DEFAULT_TEXT_MODEL:"gpt-3.5-turbo",
    'defaultImageModel':typeof process.env.BACKEND_DEFAULT_IMAGE_MODEL!==undefined?process.env.BACKEND_DEFAULT_IMAGE_MODEL:"dall-e-2",
    'deleteChatsCrontab':typeof process.env.BACKEND_DELETE_CHATS_CRONTAB!==undefined?process.env.BACKEND_DELETE_CHATS_CRONTAB:"0 0 1 * * *",
    'resetTokensCrontab': typeof process.env.BACKEND_RESET_TOKENS_CRONTAB!==undefined?process.env.BACKEND_RESET_TOKENS_CRONTAB:"0 0 0 1 * *"

}

export default Globals;