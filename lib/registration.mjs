// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import i18next from 'i18next';
import { readFile } from 'fs/promises';
import registration from '../database/models/registration.mjs';
import { confirmCreateMail, sendMail } from './mail.mjs';
import { checkValidDomain } from './utils.mjs';
import adminPanel from '../database/models/adminPanel.mjs';
const { randomUUID } = await import('crypto');


const de = JSON.parse(
    await readFile(
      new URL('../locales/de.json', import.meta.url)
    )
  );
  
  i18next.init({
      lng: 'de',
      resources: {
          de
      }
  });

/**
 * Creates a registration
 * @param {*} request Request handler
 * @param {*} hashcode (optional) Hashcode of pane
 * @param {*} type type of pane
 * @returns success: true + optional additional data | false
 */
async function prepareRegistration(request, hashcode, type, sendmail = true){
    let data = {};
    data.code = (+new Date).toString(25) + "-" + randomUUID();
    let today = new Date();
    today.setHours( today.getHours() + 2);
    data.expire = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() + " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    try{
        let domain = checkValidDomain(request);
        if(domain){
            if(request.body.mail!==undefined)
                data.mail = request.body.mail.toLowerCase().split('@')[0] + domain;
            else
                data.mail = request.body.params.mail.toLowerCase().split('@')[0] + domain;    
            let accountExisting = await adminPanel.query().where('mail', data.mail).first();
            
            if(!accountExisting || (accountExisting && accountExisting.password === null)){
                data.dnr = request.body.accesscode;
                if(request.body.accesscode==undefined)
                    data.dnr = request.body.params.accesscode;
                if(hashcode!==undefined)
                    data.hashcode = hashcode;
                if(type!==undefined)
                    data.type = type;
                let registrationData = await registration.query().insert(data);
                if(registrationData){
                    if(sendmail){
                        await confirmCreateMail(data.mail, data.code);
                        if(hashcode!==undefined)
                            return {success: true};
                        else{
                            return {success: true, title: i18next.t('createPane.confirm'), mail: domain};
                        }
                    }else{
                        return {success: true, redirectID: data.code};
                    }
                }
            }else{
                return {success: true, message: "account-existing"};
            }
        }
    }catch(e){
        console.error(e);
    }
    return {success: false}
}


export { prepareRegistration }