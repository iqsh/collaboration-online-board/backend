// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const { randomUUID } = await import('crypto');
import i18next from 'i18next';
import { readFile } from 'fs/promises';
import bcrypt from 'bcrypt';
import passwordReset from '../database/models/passwordReset.mjs';
import Globals from './globalvars.mjs';
import { sendMail } from './mail.mjs';
import allPanes from '../database/models/allPanes.mjs';
import sessionStore from '../database/models/sessionStore.mjs';
import freeform from '../database/models/freeform.mjs';
import schedule from '../database/models/schedule.mjs';
import adminPanel from '../database/models/adminPanel.mjs';
import timeline from '../database/models/timeline.mjs';

const de = JSON.parse(
    await readFile(
      new URL('../locales/de.json', import.meta.url)
    )
);

i18next.init({
    lng: 'de',
    resources: {
        de
    }
});

/**
 * Creates an unique link for the password reset
 * @param {string} mail Id of the account
 * @returns success: true || false
 */
async function preparePasswordReset(mail) {
    let paneData = await adminPanel.query().where('mail',mail).first();
    if(paneData){
        let code = (+new Date).toString(20) + "-" + randomUUID();
        let today = new Date();
        today.setHours( today.getHours() + 2);
        let expire = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate() + " " + today.getHours() + ":" + today.getMinutes();  
        try{
            let response = await passwordReset.query().insert({ mail: mail, code: code, expire: expire});
            if(response!=null){
                let data = await passwordReset.query().where('mail',mail).orderBy('expire','DESC').first();
                if(data!==null && data.mail !==null && data.mail !==undefined){
                    let to = data.mail;
                    let message= i18next.t('mail.passwordReset.body',{SERVERADDRESS: Globals.frontendServerAddress+Globals.frontendBasePath, CODE: code});
                    await sendMail(to, i18next.t('mail.passwordReset.subject'), message);
                    return { success: true };
                }
                return { success: true, code: code };
            }
            
        }catch(error){
            console.error(error);
        }
    }
    return { success: false };
}

/**
 * Resets a password for a pane
 * @param {*} request Request Handler
 */
async function resetPassword(request){
    let body = {};
    let token = request.headers.token;
    let hashcode = request.params.hashcode;
    if(token){
        let paneData = await allPanes.query().where('hash_pane',hashcode).first();
        let sessionData = await sessionStore.query().where('session_id', token).first();
        if(paneData && sessionData && paneData.mail === sessionData.mail){
            if(request.body.modPassword && request.body.modPassword.length>0){
                body.password_admin = bcrypt.hashSync(request.body.modPassword, Globals.saltRounds);
            }
            if(request.body.panePassword && request.body.panePassword.length>0){
                body.password_pane = bcrypt.hashSync(request.body.panePassword, Globals.saltRounds);
            }
            body.hashcode = hashcode;
            if(body.password_admin !== undefined || body.password_pane !== undefined){
                try{
                    if(paneData.type === "Freeform"){
                        await freeform.query().patch(body).where('hashcode', hashcode);
                    }else if(paneData.type === "Schedule" || paneData.type === "Stream"){
                        await schedule.query().patch(body).where('hashcode', hashcode);
                    }else if(paneData.type === "Timeline"){
                        await timeline.query().patch(body).where('hashcode', hashcode);
                    }
                    return { success: true }
                } catch(err){
                    console.error(err);
                };
            }
        }
    }
    return { success: false };
}

/** 
 * Resets a password for a account from outside - with id
 * @param {*} request Request Handler
 */
 async function resetAccountPassword(request){
    let body = {};
    let code = request.params.id;
    let adminPassword = request.body.adminPassword;
    let adminPasswordConfirm = request.body.adminPasswordConfirm;
    if(adminPassword && adminPasswordConfirm && adminPassword === adminPasswordConfirm){
        let response = await passwordReset.query().where('code',code).first();
        if(response){
            if(adminPassword && adminPassword.length>=8){
                body.password = bcrypt.hashSync(adminPassword, Globals.saltRounds);
            }
            body.code = code;
            if(body.password !== undefined){
                try{
                    await passwordReset.query().patch(body).where('code',code);
                    await passwordReset.query().delete().where('mail',response.mail);
                    return { success: true }
                } catch(error){
                    console.error(error);
                };
            }
        }
    }
    return { success: false };
}

/** 
 * Resets a password for a account from inside - with token
 * @param {*} request Request Handler
 */
 async function resetAccountPasswordLoggedIn(request){
    let body = {};
    let token = request.headers.token;
    let adminPassword = request.body.adminPassword;
    let adminPasswordConfirm = request.body.adminPasswordConfirm;
    if(token && adminPassword && adminPasswordConfirm && adminPassword === adminPasswordConfirm){
        let session = await sessionStore.query().where('session_id',token).first();
        if(session){
            if(adminPassword && adminPassword.length>=8){
                body.password = bcrypt.hashSync(adminPassword, Globals.saltRounds);
            }
            body.mail = session.mail;
            if(body.password !== undefined){
                try{
                    await adminPanel.query().patch(body).where('mail',session.mail);
                    return { success: true }
                } catch(error){
                    console.error(error);
                };
            }
        }
    }
    return { success: false };
}

/**
 * Checks if the hashcode is valid and send the hashcode
 * @param {string} hashcode Hashcode of password reset
 * @returns status: true || false, [hashcode: string]
 */
async function getPasswordResetData(hashcode){
    if(hashcode){
        try{
            let response = await passwordReset.query().where('code', hashcode).first();
            if(response){
                let responseDate = new Date(response.expire);
                let newDate = new Date();
                if(responseDate > newDate){
                    return { status: true, hashcode: response.hashcode };
                }else{
                    await passwordReset.query().delete().where('code', hashcode);
                }
            }
        } catch(err){
            console.error(err);
        };
    }
    return { status: false };
}

export { preparePasswordReset, resetPassword, resetAccountPassword, resetAccountPasswordLoggedIn, getPasswordResetData }