// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { unlink, mkdir, readFile, writeFile } from 'fs/promises';
import { ensureDir } from 'fs-extra';
import { extension } from 'mime-types';
import { promisify } from 'util';
import { pipeline } from 'stream';
import { join } from 'path';
import crypto from 'crypto';
import Globals from './globalvars.mjs';
import sessionStore from '../database/models/sessionStore.mjs';
import adminPanel from '../database/models/adminPanel.mjs';
import { PDFDocument } from '@cantoo/pdf-lib';
import { fromBuffer } from 'pdf2pic';

const pump = promisify(pipeline)
const { randomUUID } = await import('crypto');
const __dirname = new URL('.', import.meta.url).pathname;

/**
 * Updates the configuration of a pane
 * @param {*} table objection model of table
 * @param {*} params data to be updated
 * @returns success: true | false
 */
async function updatePaneConfiguration(table, params) {
    let data = await table.query().where('hashcode',params.hashcode.toLowerCase()).first();
    if(data){
        if(params.backgroundImage === null){
            let oldPath = data.background_image;
            if(oldPath !== null && oldPath.length>0){
                let filePath =  oldPath.replace("/api/file/", "uploads/").split("?")[0];
                try{
                    await unlink(filePath);
                }catch(error){
                    console.error(error);
                }
            }
        }
        if(params.column_width!==undefined)
            params.column_width = Number(params.column_width);
        if(params.line_size !==undefined)
            params.line_size = Number(params.line_size);
        if(params.line_color !==undefined)
            params.line_color = String(params.line_color);
        if(params.line_text)
            params.line_text = Boolean(params.line_text);
        if(params.element_width!==undefined)
            params.element_width = Number(params.element_width);
        let update = await table.query().patch(params).where('hashcode',params.hashcode.toLowerCase());
        if(update && update>0){
            return { success: true };
        }
    }
    return { success:false };
}

/**
 * Uploads a background file
 * @param {*} table objection model of table
 * @param {*} body body data
 * @param {*} file file
 * @returns success: true with additional information | false
 */
async function uploadBackground(table, body, file) {
    if(file.mimetype.includes('image')){
        let data = await table.query().where('hashcode',body.hashcode.toLowerCase()).first();
        if(data){
            let oldPath = data.background_image;
            if(oldPath !== null && oldPath.length>0){
                let filePath = oldPath.replace('/api/file/', 'uploads/').split('?')[0];
                try{
                    await unlink(filePath);
                }catch(error){
                    console.error(error);
                }
            }
            let dir = 'uploads/' + body.hashcode;
            try{
                await ensureDir(dir);
            }catch(error){
                console.error(error);
            }
            let fileExtension = extension(file.mimetype);
            const destinationPath = join(dir, randomUUID() + "." + fileExtension);
            try{
                await writeFile(destinationPath, await file.toBuffer());
            }catch(error){
                console.error(error);
            }
            let backgroundImage = destinationPath.toString().replace("uploads/", "/api/file/");
            body.background_image = backgroundImage;
            body.likes = Boolean(body.likes);
            body.authorize = Boolean(body.authorize);
            if(body.column_width!==undefined)
                body.column_width = Number(body.column_width);
            if(body.line_size !==undefined)
                body.line_size = Number(body.line_size);
            if(body.line_text)
                body.line_text = Boolean(body.line_text);
            if(body.element_width!==undefined)
                body.element_width = Number(body.element_width);
            delete body.file;
            try{
                let update = await table.query().patch(body).where('hashcode',body.hashcode.toLowerCase());
                if(update && update>0){
                    return { success: true, path: backgroundImage, type: file.mimetype };
                }
            }catch(error){
                console.error(error);
                return { success: false, path: backgroundImage, type: file.mimetype };
            }
        }
    }
    return { status: "Error!", message: "Could't upload!" };
}

/**
 * 
 * @param {*} request Request handler
 * @returns null | domains
 */
function checkValidDomain(request){
    let domain = null;
    if(request.body.domain)
        domain = request.body.domain;
    else if(request.body.mailDomain)
        domain = request.body.mailDomain;
    else if(request.body.params && request.body.params.domain) 
        domain = request.body.params.domain; 
    if(domain!==null && domain!==undefined){
        if(Globals.mailDomains.includes(domain.toLowerCase())){
            return domain.toLowerCase();
        }else{
            throw "No Domain";
            //return;
        }
    }else{
        return Globals.mailDomains[0].toLowerCase();
    }
}

/**
 * Creates a hash for a pane
 * @returns {string} Hash of a pane
 */
 function createHashPane(){
    return (+new Date).toString(36) + crypto.randomBytes(2).toString('hex');
}

/**
 * Hashes the hashcode from pane
 * @param {string} hashcode hashcode from pane
 * @returns {string} Hashed hashcode from pane
 */
function createNoPasswordHash(hashcode){
    return crypto.createHash('md5').update((+new Date).toString(18) + hashcode.toLowerCase() + crypto.randomBytes(2)).digest("hex");
}

/**
 * Checks if an user is allowed
 * e.g. if an user is allowed to create a pane based on the dnr
 * @param {*} response Response-Handler
 * @param {*} request Request-Handler
 * @param {*} cb Callback
 */
async function checkIfAllowed(request){
    if(Globals.activePaneRegistration) {  
        let file = join(__dirname,"..","accesscode.json");
        let bodyAccesscode = request.body.accesscode!==undefined?request.body.accesscode:request.body.params.accesscode; 
        const data = await readFile(file, {encoding: 'utf-8'});
        if(data){
            let accesscodes = JSON.parse(data).accesscode;
            for(let accesscode of accesscodes){
                if(bodyAccesscode.includes(accesscode)){
                    return true;
                }
            }
        }
        return false;
    }else{
        return true;
    }
}

/**
 * Checks if a file is allowed for upload
 * @param filename Name of file
 * @param mimetype Mimetype of file
 * @returns {boolean}
 */
 function checkFile(filename, mimetype){
    if(Globals.fileUploadExtensions.split(",").some((x)=>filename.includes(x)))
        return true;
    else{
        return Globals.fileUploadMimeTypes.split(",").some((x)=>mimetype.includes(x));
    }
}


/**
* Creates a png file from a pdf
* @param {*} pdfPath Path of the pdf file
*/
async function createImageFromPDF(pdfPath){
    if(pdfPath){
        const root = join(__dirname, "..");
        const splitPdf = pdfPath.split("/");
        const savePath = join(root, "uploads", splitPdf[1]);
        const filePath = join(root, pdfPath);
        try{
            const pdfBuffer = await readFile(filePath);
            const pdfDoc = await PDFDocument.load(pdfBuffer,{ ignoreEncryption: true });    
            const [firstPage] = pdfDoc.getPages();
            const { width, height } = firstPage.getSize();
            const outputPath = join(savePath, splitPdf[splitPdf.length - 1].replace(".pdf", ".png"));
            const options = {
				density: 100,
				width,
				height,
				savePath: savePath
			};
            const storeAsImage = fromBuffer(pdfBuffer, options);
			const output = await new Promise((resolve, reject) => {
                storeAsImage(1, { responseType: "base64" }).then(resolve).catch(reject);
            });
            const fileSizeInBytes = Buffer.from(output.base64, 'base64').length;
            if(fileSizeInBytes>0){
                await writeFile(outputPath, output.base64, "base64");
                return true;
            }
            return false;
        }catch(error){
            console.error(error);
            return false;
        }
    }else{
        return false;
    }
}
  
/**
 * Hashes an E-Mail-Address
 * @param {string} mail E-Mail-Address
 * @returns {string} Hashed E-Mail-Address
 */
 function hashMail(mail){
    return crypto.createHash('md5').update((+new Date).toString(33) + mail.toLowerCase()).digest("hex");
}

/**
 * Checks if a password is valid
 * @param {*} password 
 * @returns boolean
 */
function checkPassword(password){
    var regexp = new RegExp("^(?=[0-9a-zA-Z!\-@#\$%\^&*]{8,}$)(?=.*?[a-z])(?=.*?[0-9])(?=.*?[A-Z])(?=.*?[!\-@#\$%\^&*]).*");
    return password!==null && password!==undefined && regexp.test(password);
}

/**
 * Check if parameter is valid - not null, not undefined and not contains spaces
 * @param {string} param 
 * @returns boolean
 */
function checkParameter(param){
    return param!==null && param!==undefined && !param.includes(' ');
}

/**
 * Retrieves a user from the database based on the provided token.
 *
 * @param {string} token - The token used to query the database for user information.
 * @return {Promise<adminPanel>} The user object if found, otherwise undefined.
 */
async function getUserFromToken(token){
    let session = await sessionStore.query().where('session_id',token).first();
    if(session){
        return await adminPanel.query().where('mail',session.mail).first();
    }
}

export { updatePaneConfiguration, uploadBackground, checkValidDomain, createHashPane, createNoPasswordHash, checkIfAllowed, checkFile, createImageFromPDF, hashMail, checkParameter, checkPassword, getUserFromToken }