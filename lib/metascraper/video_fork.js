// Copyright © 2019 Microlink hello@microlink.io (microlink.io) 
// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

'use strict'

const {
  $filter,
  $jsonld,
  video,
  findRule,
  has,
  $twitter,
  loadIframe,
  normalizeUrl,
  toRule
} = require('@metascraper/helpers')

const { find, chain, isEqual } = require('lodash')
const pReflect = require('p-reflect')

const toVideo = toRule(video)

const toVideoFromDom = toRule((domNodes, opts) => {
  const values = chain(domNodes)
    .map(domNode => ({
      src: domNode?.attribs.src,
      type: chain(domNode)
        .get('attribs.type')
        .split(';')
        .get(0)
        .split('/')
        .get(1)
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
        //.replace('mpeg', 'mp3')
        .value()
    }))
    .uniqWith(isEqual)
    .value()

  let result
  find(
    values,
    ({ src, type }) => (result = video(src, Object.assign({ type }, opts)))
  )
  return result
})

const videoRules = [
  ({ url, htmlDom: $ }) => {
    const src =
      $('meta[property="og:video:secure_url"]').attr('content') ||
      $('meta[property="og:video:url"]').attr('content') ||
      $('meta[property="og:video"]').attr('content')

    return src
      ? video(src, {
        url,
        type: $('meta[property="og:video:type"]').attr('content')
      })
      : undefined
  },
  ({ url, htmlDom: $ }) => {
    const src = $twitter($, 'twitter:player:stream')
    return src
      ? video(src, {
        url,
        type: $twitter($, 'twitter:player:stream:content_type')
      })
      : undefined
  },
  toVideo($jsonld('contentUrl')),
  toVideoFromDom($ => $('video').get()),
  toVideoFromDom($ => $('video > source').get()),
  //({ htmlDom: $ }) => $filter($, $('a[href]'), el => video(el.attr('href')))
]

const _getIframe = (url, $, { src }) =>
  loadIframe(url, $.load(`<iframe src="${src}"></iframe>`))

module.exports = ({ getIframe = _getIframe } = {}) => {
  return {
    video: videoRules.concat(
      async ({ htmlDom, url }) => {
        const srcs = [...new htmlDom('iframe').map((_, element) => htmlDom(element).attr('src')).get().map(src => normalizeUrl(url, src))]
        if (srcs.length === 0) return
        return pReflect(
          Promise.any(
            srcs.map(async src => {
                //const htmlDom = await getIframe(url, $, { src })
                const result = await findRule(videoRules, { htmlDom, url })
                if (!has(result)) throw TypeError('no result')
                return result
            })
          )
        ).then(({ value }) => value)
      },
      async ({ htmlDom, url }) => {
        const src = $twitter(htmlDom, 'twitter:player')
        return src
          ? findRule(videoRules, {
            htmlDom: htmlDom,
            url
          })
          : undefined
      }
    )
  }
}