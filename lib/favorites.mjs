// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import sessionStore from "../database/models/sessionStore.mjs";
import favorites from "../database/models/favorites.mjs";

async function getPanesFromFavorites(mail){
    const favoritePanes = await favorites.query().where('view_favourites.mail', mail)
        .join('view_all_boards','view_all_boards.hash_pane','=','view_favourites.hashcode')
        .select('view_all_boards.*','view_favourites.mail','view_favourites.origin_mail');
    return favoritePanes;
}

async function addPaneToFavorites(request){
    const id = request.params.id;
    if(id && request.headers.token){
        try{
            const session = await sessionStore.query().where('session_id', request.headers.token).first();
            if(session){
                const response = await favorites.query().insert({hashcode: id, mail: session.mail});
                if(response){
                    const favoritePanes = await getPanesFromFavorites(session.mail);
                    return { success: true, favorites: favoritePanes };
                }
            }
        }catch(error){
            console.error(error);
        }
    }
    return { success: false };
}

async function removePaneFromFavorites(request){
    const id = request.params.id;
    if(id && request.headers.token){
        try{
            const session = await sessionStore.query().where('session_id', request.headers.token).first();
            if(session){
                await favorites.query().delete().where('hashcode', id).where('mail', session.mail);
                const favoritePanes = await getPanesFromFavorites(session.mail);
                return { success: true, favorites: favoritePanes };
            }
        }catch(error){
            console.error(error);
        }
    }
    return { success: false }; 
}

export { getPanesFromFavorites, addPaneToFavorites, removePaneFromFavorites };