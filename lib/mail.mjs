// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import i18next from 'i18next';
import { readFile } from 'fs/promises';
import adminPanel from '../database/models/adminPanel.mjs';
import Globals from './globalvars.mjs';
import { mailAttachments } from '../configuration/config.mjs';
import nodemailer from 'nodemailer';

const de = JSON.parse(
  await readFile(
    new URL('../locales/de.json', import.meta.url)
  )
);

i18next.init({
    lng: 'de',
    resources: {
        de
    }
});

/**
 * Sends a mail
 * @param {string} to To
 * @param {string} subject Subject of mail
 * @param {string} message Message of mail
 */
 async function sendMail(to, subject, message){
    let transporter = null;
    if(!Globals.realMail){
        let testAccount = await nodemailer.createTestAccount();
        transporter = nodemailer.createTransport({
            host: "smtp.ethereal.email",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
            },
        });
    }else if(process.env.NODE_ENV == "test"){
        transporter = nodemailer.createTransport({
            host: "localhost",
            port: 2525,
            secure: false,
            tls: {
                rejectUnauthorized: false
            }
        });
    }else{
        transporter = nodemailer.createTransport({
            host: Globals.smtpServer,
            port: Globals.mailPort,
            secure: Globals.mailSecure,
            auth: {
                user: Globals.mailUser,
                pass: Globals.mailPassword,
            },
            tls: {
                rejectUnauthorized: false // Verwenden Sie selbstsignierte Zertifikate nicht
            }
        });
    }
    let mailObject = {
        from: Globals.noreplyMail, // sender address
        to: to, 
        subject: subject,
        html: message,
        attachments: []
    };

    mailObject.attachments = mailAttachments;
    // send mail with defined transport object
    
    let info = await transporter.sendMail(mailObject).catch(console.error);
    //console.log("Message sent: %s", info.messageId);
    if(!Globals.realMail){
        // Preview only available when sending through an Ethereal account
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
}



/**
 * Sends a confirmation mail after creating a pane
 * @param {*} to To
 * @param {*} hashcode Hashcode
 * @param {*} res Response-Handler
 * @param {*} cb Callback
 */
async function createPaneMail(to, hashcode, cb){
    let apData = await adminPanel.query().where('mail',to);
    if(apData){
        var message= i18next.t('mail.create.body',{SERVERADDRESS:Globals.frontendServerAddress+Globals.frontendBasePath, HASHCODE:hashcode, DASHBOARD:apData.dashboard});
        await sendMail(to, i18next.t('mail.create.subject'), message);
        return cb();
    }else{
        return {status: "false"}
    }
}

/**
 * Sends a confirmation mail after registration
 * @param {*} to To
 * @param {*} hashcode Hashcode of create
 */
async function confirmCreateMail(to, hashcode){
    let message= i18next.t('mail.confirmCreate.body', {SERVERADDRESS:Globals.frontendServerAddress+Globals.frontendBasePath, HASHCODE:hashcode});
    await sendMail(to, i18next.t('mail.confirmCreate.subject'), message);
}

export {createPaneMail, confirmCreateMail, sendMail}