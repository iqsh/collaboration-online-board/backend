// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import crypto from 'crypto';
import { checkIfAllowed } from "./utils.mjs";
import share from '../database/models/share.mjs';
import sessionStore from '../database/models/sessionStore.mjs';
import allPanes from '../database/models/allPanes.mjs';

/**
 * Copies a pane for a different user
 * @param {*} req Request-Handler
 * @param {*} cb Callback
 */
async function copyDifferentUser(request,cb){
    if(await checkIfAllowed(request)){
        let shareData = await share.query().where('prev_hash',request.body.params.id).first();
        if(shareData){
            return cb(request.body.params, shareData.pane_hash, false, request);
        }
    }   
    return {status: false, reason: 'Not Allowed'};
}

/**
 * Shares a pane
 * @param {*} request Request-Handler
 * @param {*} type Type of the pane: Freeform || Schedule || Stream
 * @param {*} table objection table
 */
async function sharePane(request,type,table){
    let hashcode = request.body.hashcode;
    try{
        let data = await table.query().where('hashcode',hashcode).first();
        let token = request.headers.token;
        if(token){
            const session = await sessionStore.query().where('session_id',token).first();
            if(data && session && data.mail === session.mail){
                let previewHashcode = crypto.createHash('md5').update((+new Date).toString(36) + hashcode).digest("hex");
                let params = { 
                    pane_hash: hashcode,
                    prev_hash: previewHashcode,
                    likes: request.body.copyLikes,
                    comments: request.body.copyComments,
                    ende: String(request.body.shareDate),
                    typname: type
                };
                let success = await share.query().insert(params);
                if(success){
                    return {success:true,hashcode:previewHashcode};
                }
            }
        }
    }catch(error){
        console.error(error);
    }
    return{success:false};
}

/**
 * Unshares a pane
 * @param {*} request Request handler
 * @returns success: true || false
 */
async function unsharePane(request){
    try{
        let hashcode = request.body.hashcode;
        if(hashcode){
            let success = await share.query().delete().where('pane_hash',hashcode);
            if(success==0){
                return {success: true}
            }
        }
    }catch(error){
        console.error(error);
    }
    return {success: false};
}

/**
 * Gets current shared data from pane
 * @param {string} hashcode Hashcode of pane
 * @returns success: true | false or end and url
 */
async function getCurrentShare(hashcode, request){
    const token = request.headers.token;
    if(token){
        try{
            const result = await share.query().where('pane_hash',hashcode).first();
            const pane = await allPanes.query().where('hash_pane',hashcode).first();
            const session = await sessionStore.query().where('session_id',token).first();
            if(result && session && pane && session.mail === pane.mail){
                if(new Date(Number(result.ende))>new Date()){
                    return { success: true, end: result.ende, hashcode: result.prev_hash };
                }else{
                    await share.query().delete().where('pane_hash',hashcode);
                } 
            }
        }catch(error){
            console.error(error);
        }
    }
    return { success: false };
}

export { copyDifferentUser, sharePane, unsharePane, getCurrentShare }