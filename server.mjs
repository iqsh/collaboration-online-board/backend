// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { ToadScheduler, SimpleIntervalJob, AsyncTask, CronJob } from 'toad-scheduler';
import build from './app.mjs';
import Globals from './lib/globalvars.mjs';
import sessionStore from './database/models/sessionStore.mjs';
import aiChatMessageTokenUsage from './database/models/aiChatMessageTokenUsage.mjs';
import aiChat from './database/models/aiChat.mjs';
import aiChatMessage from './database/models/aiChatMessage.mjs';
import aiChatMessageSession from './database/models/aiChatMessageSession.mjs';
import aiChatShared from './database/models/aiChatShared.mjs';
import aiUser from './database/models/aiUser.mjs';
import aiUserBackup from './database/models/aiUserBackup.mjs';
import { transaction, raw } from 'objection';
import { existsSync, rmSync } from 'fs';
import { knex } from './database/index.mjs';

const server = build({
    logger: true,
    maxParamLength: 200,
    bodyLimit: 104857600
});
const scheduler = new ToadScheduler();


const deleteInvalidSessions = async () => {
    await sessionStore.query().delete().where(raw('CAST(expire as BIGINT) < ?', +new Date()));
}

const updateTokens = async () => {
    const mails = await aiChatMessageTokenUsage.query().groupBy('mail').select('mail');
    for (const m of mails) {
        try {
            await transaction(aiChatMessageTokenUsage, aiUser, async (aiChatMessageTokenUsage, aiUser) => {
                const messagesTokens = await aiChatMessageTokenUsage.query().where('mail', m.mail);
                const sum = messagesTokens.reduce((a, b) => a + Number(b.token), 0);
                const user = await aiUser.query().where('mail', m.mail).first();

                const tokensPerModel = await aiChatMessageTokenUsage.query().where('mail', m.mail).select('mail', 'model', raw(`SUM(token::real) as total_tokens`)).groupBy(['mail', 'model']);
                const mergedObject = user.tokens_per_model || {};
                for (const t of tokensPerModel) {
                    if (!mergedObject[t.model])
                        mergedObject[t.model] = t.total_tokens;
                    else
                        mergedObject[t.model] += t.total_tokens;
                }
                if (user) {
                    await aiUser.query().patch({ token_used: String(Math.ceil(Number(Number(user.token_used) + Number(sum)))), tokens_per_model: mergedObject }).where('mail', m.mail);
                }
                for (const t of messagesTokens) {
                    await aiChatMessageTokenUsage.query().where('uuid', t.uuid).delete();
                }
            });
        } catch (error) {
            console.error(error);
        }
    }
}

const resetTokens = async () => {
    // reset tokens on first day of month
    
    try {
        const wasUpdated = await aiUserBackup.query().orderBy('time', 'desc').first();
        const now = new Date();
        //check if wasUpdate.time (YYYY-MM) (previous month) is same year and month of today - except january
        if(wasUpdated && (wasUpdated.time === (now.getMonth()>0?`${ now.getFullYear()}-${now.getMonth()}`:`${ now.getFullYear()-1}-12`))){
            return;
        }
        if(now.getDate() === 1){
            await transaction(aiUserBackup, aiUser, async (aiUserBackup, aiUser) => {
                const tokens = await aiUser.query().select('mail', raw('token_used::text'), 'token_quota', raw('TO_CHAR(DATE_TRUNC(\'month\', CURRENT_DATE - INTERVAL \'1 month\'), \'YYYY-MM\') AS time'), 'tokens_per_model');
                await aiUserBackup.query().insert(tokens);
                await aiUser.query().patch({ token_used: "0", tokens_per_model: {} });
            });
        }
    } catch (error) {
        console.error(error);
    }

}

const deleteOldChats = async () => {
    // when the create date or share date older than 14 days delete the chat
    const date = new Date();
    date.setDate(date.getDate() - 14);
    const query = aiChatShared.query().where('ai_chat_shared.valid_until', '<', date.toISOString()).leftJoin('ai_chat', 'ai_chat.uuid', 'ai_chat_shared.chat_uuid').select('ai_chat_shared.*', 'ai_chat.user_mail as user');
    const shared = await query;
    if (shared.length > 0) {
        for (const s of shared) {
            try {
                console.log({ s });
                await transaction(aiChatShared, aiChatMessageSession, aiChatMessage, aiChat, async (aiChatShared, aiChatMessageSession, aiChatMessage) => {
                    await aiChatShared.query().where('chat_uuid', s.chat_uuid).delete();
                    await aiChatMessageSession.query().where('chat_uuid', s.chat_uuid).andWhereNot('user', s.user).delete();
                    await aiChatMessage.query().where('chat_uuid', s.chat_uuid).andWhereNot('user', s.user).delete();
                    //await aiChat.query().where('uuid', s.chat_uuid).delete();
                });
            }
            catch (error) {
                server.log.error(error);
            }
        }
    }
    // delete all ai chats and the messages of the user who created the chat when the create date is older than 6 weeks or the last message is older than 6 weeks of the chat
    const date2 = new Date();
    date2.setDate(date2.getDate() - 42);
    const subquery = aiChatMessage.query()
        .groupBy('chat_uuid')
        .as('subquery')
        .select('chat_uuid', raw('max(create_date) as max_date'));
    const chatsQuery = aiChat.query()
        .leftJoin(subquery.as('subquery'), 'ai_chat.uuid', 'subquery.chat_uuid')
        .leftJoin('ai_chat_message', 'subquery.chat_uuid', 'ai_chat_message.chat_uuid')
        .whereRaw('ai_chat_message.create_date = subquery.max_date').andWhere('subquery.max_date', '<', date2.toISOString())
        .select('ai_chat.uuid', 'ai_chat.name', 'ai_chat.create_date', 'ai_chat.type', 'ai_chat.avatar', 'subquery.max_date', 'ai_chat_message.message', 'ai_chat.person_name as person', 'ai_chat.user_mail')
        .orderBy('subquery.max_date', 'desc');
    const chats = await chatsQuery.execute();
    if (chats.length > 0) {
        for (const c of chats) {
            try {
                console.log({ c });
                const result = await transaction(aiChatMessageSession, aiChatMessage, aiChat, aiChatShared, async (aiChatMessageSession, aiChatMessage, aiChat, aiChatShared) => {
                    await aiChatMessageSession.query().where('chat_uuid', c.uuid).delete();
                    await aiChatMessage.query().where('chat_uuid', c.uuid).delete();
                    await aiChatShared.query().where('chat_uuid', c.uuid).delete();
                    await aiChat.query().where('uuid', c.uuid).delete();
                    return true;
                });
                if(result === true){
                    const path = `./uploads/chats/${c.uuid}`;
                    if (existsSync(path)) {
                        rmSync(path, { recursive: true });
                    }
                }
            }
            catch (error) {
                server.log.error(error);
            }
        }
    }
}

const deleteOldChatSessions = async() =>{
    await knex.raw('SELECT delete_old_sessions();');
};


const start = async () => {
    try {
        await server.listen({ port: process.env.BACKEND_PORT, host: '0.0.0.0' });
        if (Globals.executeCronJobs) {
            if (Globals.sessionDeleteInterval) {
                const task = new AsyncTask('sessiondelete', deleteInvalidSessions);
                const job = new SimpleIntervalJob({ hours: Number(Globals.sessionDeleteInterval), }, task);
                scheduler.addSimpleIntervalJob(job);
            }
            if (Globals.tokenCalculationInterval && Globals.tokenCalculation) {
                const task2 = new AsyncTask('tokenCalculation', updateTokens);
                const job2 = new SimpleIntervalJob({ seconds: Number(Globals.tokenCalculationInterval), }, task2);
                scheduler.addSimpleIntervalJob(job2);
            }
            if (Globals.deleteChatsCrontab) {
                await deleteOldChats();
                const task3 = new AsyncTask('deleteOldChats', deleteOldChats);
                const job3 = new CronJob({ cronExpression: Globals.deleteChatsCrontab }, task3);
                scheduler.addSimpleIntervalJob(job3);
            }
            if(Globals.resetTokensCrontab){
                await resetTokens();
                const task4 = new AsyncTask('resetTokens', resetTokens);
                const job4 = new CronJob({ cronExpression: Globals.resetTokensCrontab }, task4);
                scheduler.addSimpleIntervalJob(job4);
            }
            await deleteOldChatSessions();
            const task5 = new AsyncTask('deleteOldChatSessions', deleteOldChatSessions);
            const job5 = new SimpleIntervalJob({ minutes: 1 }, task5);
            scheduler.addSimpleIntervalJob(job5);
        }
    } catch (err) {
        server.log.error(err)
        process.exit(1)
    }
}

start()