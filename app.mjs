// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import { join } from 'path';
import Fastify from 'fastify';
import fastifyMultipart from '@fastify/multipart';
import fastifyStatic from '@fastify/static';
import fastifyCors from '@fastify/cors';
import 'dotenv/config';

import index from './routes/index.mjs'
import Globals from './lib/globalvars.mjs';
const __dirname = new URL('.', import.meta.url).pathname;

const build = (opts={}) => {
    const fastify = Fastify(opts);
    fastify.register(fastifyCors, {
        //origin: "*", // for local development purposes
        origin: Globals.frontendServerAddress, // for production purposes
        credentials: true
    });
    fastify.register(fastifyMultipart, {
        //addToBody: true
        attachFieldsToBody: true,
        limits: {
            fileSize: 200 * 1024 * 1024 // 200 MB
        }
    });
    fastify.register(fastifyStatic, {
        root: join(__dirname),
    });

    fastify.register(index,{ prefix: '/api' });

    fastify.get('/', async (request, reply) => {
        return { status: 'ok' }
    });
    
    return fastify;
}

export default build;